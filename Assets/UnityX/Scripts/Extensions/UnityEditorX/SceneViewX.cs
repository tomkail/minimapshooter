﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
public static class SceneViewX {
	public static Vector2 WorldToGUI (SceneView sceneView, Vector3 position) {
		Debug.Assert(sceneView != null, "Scene view is null!");

		Vector2 positionOnScreen = SceneView.lastActiveSceneView.camera.WorldToScreenPoint (position);
		Rect cameraRect = SceneView.lastActiveSceneView.camera.pixelRect;

		Vector2 positionOnGUI = new Vector2 (positionOnScreen.x, cameraRect.height - positionOnScreen.y);
		if (EditorApplicationX.IsRetina()) {
			positionOnGUI *= 0.5f;
		}
		return positionOnGUI;
	}
}
#endif