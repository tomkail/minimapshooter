﻿using UnityEngine;
using System.Collections;

public static class EditorApplicationX {

	public static bool IsRetina () {
		return Application.platform == RuntimePlatform.OSXEditor && float.Parse(Application.unityVersion.Substring(0,3)) >= 5.4;
	}
}
