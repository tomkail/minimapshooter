﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public static class EditorGUILayoutX {

	public static T ObjectField<T>(T val, bool allowSceneObjects = true) where T : Object {
		return EditorGUILayout.ObjectField(val, typeof(T), allowSceneObjects) as T;
	}

	public static T ObjectField<T>(string label, T val, bool allowSceneObjects = true) where T : Object {
		return EditorGUILayout.ObjectField(label, val, typeof(T), allowSceneObjects) as T;
	}

	public static T ObjectField<T>(GUIContent guiContent, T val, bool allowSceneObjects = true) where T : Object {
		return EditorGUILayout.ObjectField(guiContent, val, typeof(T), allowSceneObjects) as T;
	}

	/// <summary>
	/// A text field that allows a placeholder. Unlike EditorGUIX's version, this placeholder is used as default text when the box is selected.
	/// </summary>
	/// <returns>The field.</returns>
	/// <param name="label">Label.</param>
	/// <param name="text">Text.</param>
	/// <param name="placeholderText">Placeholder text.</param>
	public static string TextField (GUIContent label, string text, string placeholderText) {
		string uniqueControlName = "TextFieldControlName_"+label+"_"+placeholderText;
		GUI.SetNextControlName(uniqueControlName);

		if(GUI.GetNameOfFocusedControl() != uniqueControlName && text == string.Empty) {
			GUIStyle style = new GUIStyle(GUI.skin.textField);
			style.fontStyle = FontStyle.Italic;
			style.normal.textColor = new Color(0.5f, 0.5f, 0.5f);
			// Have to add a space to make this work, for some reason
			EditorGUILayout.TextField(label.text+" ", placeholderText, style);
		} else {
			text = EditorGUILayout.TextField(label, text);
		}
		return text;
    }
}
