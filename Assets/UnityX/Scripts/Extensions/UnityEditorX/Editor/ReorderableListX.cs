﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;

public static class ReorderableListX {
	public static ReorderableList.HeaderCallbackDelegate DefaultDrawHeaderCallback(ReorderableList list) {
		return (Rect rect) => {  
			EditorGUI.LabelField(rect, list.serializedProperty.displayName);
		};
	}

	public static ReorderableList.ElementHeightCallbackDelegate DefaultElementHeightCallback(ReorderableList list) {
		return (int index) => {
			var element = list.serializedProperty.GetArrayElementAtIndex(index);
			return EditorGUI.GetPropertyHeight(element);
		};
	}

	public static ReorderableList.ElementCallbackDelegate DefaultDrawElementCallback(ReorderableList list) {
		return (Rect rect, int index, bool isActive, bool isFocused) => {
			var element = list.serializedProperty.GetArrayElementAtIndex(index);
			rect.x += ReorderableList.Defaults.dragHandleWidth;
			rect.width -= ReorderableList.Defaults.dragHandleWidth;
			EditorGUI.PropertyField(rect, element, true);
   	 	};
	}
}
