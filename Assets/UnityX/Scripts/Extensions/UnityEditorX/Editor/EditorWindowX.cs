﻿using UnityEngine;
using UnityEditor;
using System.Collections;

//namespace UnityX.Editor {
	public static class EditorWindowX {
		public static WindowType[] FindEditorWindows<WindowType>() where WindowType : EditorWindow {
			return Resources.FindObjectsOfTypeAll<WindowType>();
		}

		public static bool EditorWindowInitialized<WindowType>() where WindowType : EditorWindow {
			return !FindEditorWindows<WindowType>().IsNullOrEmpty();
		}
	}
//}