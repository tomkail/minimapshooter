﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public static class EditorGUIX {

	public const float windowHeaderHeight = 14;
	public const float scrollBarSize = 15;

	public static T ObjectField<T>(Rect rect, T val, bool allowSceneObjects = true) where T : Object {
		return EditorGUI.ObjectField(rect, val, typeof(T), allowSceneObjects) as T;
	}

	public static T ObjectField<T>(Rect rect, string label, T val, bool allowSceneObjects = true) where T : Object {
		return EditorGUI.ObjectField(rect, label, val, typeof(T), allowSceneObjects) as T;
	}

	public static T ObjectField<T>(Rect rect, GUIContent guiContent, T val, bool allowSceneObjects = true) where T : Object {
		return EditorGUI.ObjectField(rect, guiContent, val, typeof(T), allowSceneObjects) as T;
	}


	/// <summary>
	/// Textfield with placeholder.
	/// </summary>
	/// <returns>The field.</returns>
	/// <param name="position">Position.</param>
	/// <param name="label">Label.</param>
	/// <param name="text">Text.</param>
	/// <param name="placeholderText">Placeholder text.</param>
	public static string TextField (Rect position, GUIContent label, string text, string placeholderText) {
		string uniqueControlName = "TextFieldControlName_"+label+"_"+placeholderText;
		GUI.SetNextControlName(uniqueControlName);
		text = EditorGUI.TextField(position, label, text);
		if(GUI.GetNameOfFocusedControl() != uniqueControlName && text == string.Empty) {
			GUIStyle style = new GUIStyle(GUI.skin.textField);
			style.fontStyle = FontStyle.Italic;
			style.normal.textColor = new Color(0.5f, 0.5f, 0.5f);
			// Have to add a space to make this work, for some reason
			EditorGUI.TextField(position, label.text+" ", placeholderText, style);
		}
		return text;
	}
}
