﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class AssetDatabaseX {
	#if UNITY_EDITOR

	/// <summary>
	/// Given an absolute path, return a path rooted at the Assets folder.
	/// </summary>
	/// <remarks>
	/// Asset relative paths can only be used in the editor. They will break in builds.
	/// </remarks>
	/// <example>
	/// /Folder/UnityProject/Assets/resources/music returns Assets/resources/music
	/// </example>
	public static string AssetsRelativePath (string absolutePath) {
		if (absolutePath.StartsWith(Application.dataPath)) {
			return "Assets" + absolutePath.Substring(Application.dataPath.Length);
		} else {
			throw new System.ArgumentException("Full path does not contain the current project's Assets folder", "absolutePath");
		}
	}

	/// <summary>
	/// Loads all assets at path. 9
	/// Replaces AssetDatabase.LoadAllAssetsAtPath, which for some reason requires a file path rather than a folder path.
	/// </summary>
	/// <returns>The all assets at path.</returns>
	/// <param name="path">Path.</param>
	public static Object[] LoadAllAssetsAtPath(string path) {
		if(path.EndsWith("/")) {
			path = path.TrimEnd('/');
		}
		Debug.Log (path);
		string[] GUIDs = AssetDatabase.FindAssets("", new string[] {path});
		Object[] objectList = new Object[GUIDs.Length];
		for (int index = 0; index < GUIDs.Length; index++)
		{
			string guid = GUIDs[index];
			string assetPath = AssetDatabase.GUIDToAssetPath(guid);
			Object asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(Object)) as Object;
			objectList[index] = asset;
		}
		
		return objectList;
	}
	
	
	public static T[] LoadAllAssetsAtPath<T> (string path) {
		Object[] assets = AssetDatabaseX.LoadAllAssetsAtPath(path);
		T[] castAssets = assets.Where(asset => asset.GetType() == typeof(T)).Cast<T>().ToArray ();
		return castAssets;
	}
	
	/// <summary>
	/// Loads all assets of type T from anywhere in the project.
	/// </summary>
	/// <returns>The all assets of type.</returns>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T[] LoadAllAssetsOfType<T>(string optionalPath = "") where T : Object {
		string[] GUIDs;
		if(optionalPath != "") {
			if(optionalPath.EndsWith("/")) {
				optionalPath = optionalPath.TrimEnd('/');
			}
			GUIDs = AssetDatabase.FindAssets("t:" + typeof (T).ToString(),new string[] { optionalPath });
		} else {
			GUIDs = AssetDatabase.FindAssets("t:" + typeof (T).ToString());
		}
		T[] objectList = new T[GUIDs.Length];
		
		for (int index = 0; index < GUIDs.Length; index++) {
			string guid = GUIDs[index];
			string assetPath = AssetDatabase.GUIDToAssetPath(guid);
			T asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(T)) as T;
			objectList[index] = asset;
		}
		
		return objectList;
	}

	/// <summary>
	/// Destroys all sub assets. If used on a subasset, will also destroy the subasset and all family members up to the main asset.
	/// </summary>
	/// <param name="parent">Parent.</param>
	public static void DestroyAllSubAssets (Object parent) {
		string path = AssetDatabase.GetAssetPath(parent);
		Object[] assets = AssetDatabase.LoadAllAssetsAtPath(path);
		foreach(Object asset in assets) {
			if (UnityEditor.AssetDatabase.IsMainAsset(asset) || asset is GameObject || asset is Component) continue;
			else Object.DestroyImmediate(asset, true);
		}
	}
	#endif
}
