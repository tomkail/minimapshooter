using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;
using UnityEditorX.SceneManagement;

[CustomPropertyDrawer(typeof(SceneNameAttribute))]
public class SceneNameDrawer : PropertyDrawer {
	
    private SceneNameAttribute sceneNameAttribute {
        get {
            return (SceneNameAttribute)attribute;
        }
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		string[] sceneNames = GetSceneNames();
		for(int i = 0; i < sceneNames.Length; i++) {
			if(!sceneNameAttribute.useFullPath) {
				sceneNames[i] = System.IO.Path.GetFileNameWithoutExtension(sceneNames[i]);
			}
		}

        if (sceneNames.Length == 0)
        {
            EditorGUI.LabelField(position, ObjectNames.NicifyVariableName(property.name), "No Scenes in build.");
            return;
        }
        int[] sceneNumbers = new int[sceneNames.Length];

        SetSceneNumbers(sceneNumbers, sceneNames);

        if (!string.IsNullOrEmpty(property.stringValue))
            sceneNameAttribute.selectedValue = GetIndex(sceneNames, property.stringValue);

        sceneNameAttribute.selectedValue = EditorGUI.IntPopup(position, label.text, sceneNameAttribute.selectedValue, sceneNames, sceneNumbers);

        property.stringValue = sceneNames[sceneNameAttribute.selectedValue];
    }

    private string[] GetSceneNames()
    {
		if(sceneNameAttribute.findMethod == SceneNameAttribute.SceneFindMethod.AllInProject) {
			return EditorSceneManagerX.scenePaths;
		}
        List<EditorBuildSettingsScene> scenes = null;
		if(sceneNameAttribute.findMethod == SceneNameAttribute.SceneFindMethod.AllInBuild) scenes = EditorBuildSettings.scenes.ToList();
		else if(sceneNameAttribute.findMethod == SceneNameAttribute.SceneFindMethod.EnabledInBuild) scenes = EditorBuildSettings.scenes.Where(scene => scene.enabled).ToList();
        HashSet<string> sceneNames = new HashSet<string>();
        scenes.ForEach(scene => {
			sceneNames.Add(scene.path);
        });
        return sceneNames.ToArray();
    }

    private void SetSceneNumbers(int[] sceneNumbers, string[] sceneNames) {
        for (int i = 0; i < sceneNames.Length; i++) {
            sceneNumbers[i] = i;
        }
    }

    private int GetIndex(string[] sceneNames, string sceneName)
    {
        int result = 0;
        for (int i = 0; i < sceneNames.Length; i++)
        {
            if (sceneName == sceneNames[i])
            {
                result = i;
                break;
            }
        }
        return result;
    }
}