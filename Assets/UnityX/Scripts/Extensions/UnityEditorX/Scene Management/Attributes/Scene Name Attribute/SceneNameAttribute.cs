using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Linq;
using System.Collections.Generic;
#endif
public class SceneNameAttribute : PropertyAttribute {
	public int selectedValue = 0;
	public SceneFindMethod findMethod;
	public enum SceneFindMethod {
    	EnabledInBuild,
    	AllInBuild,
    	AllInProject
    }

    public bool useFullPath;
	public SceneNameAttribute(SceneFindMethod findMethod = SceneFindMethod.EnabledInBuild, bool useFullPath = true)
    {
		this.findMethod = findMethod;
		this.useFullPath = useFullPath;
    }
}