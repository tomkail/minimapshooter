﻿using UnityEngine;

//namespace UnityX.Geometry {

	[System.Serializable]
	public struct Point {
		public int x, y;
	
		public static Point zero {
			get {
				return new Point(0,0);
			}
		}
		
		public static Point one {
			get {
				return new Point(1,1);
			}
		}

		public static Point up {
			get {
				return new Point(0,1);
			}
		}

		public static Point down {
			get {
				return new Point(0,-1);
			}
		}

		public static Point left {
			get {
				return new Point(-1,0);
			}
		}

		public static Point right {
			get {
				return new Point(1,0);
			}
		}
		
		public Point(int _x, int _y) {
			x = _x;
			y = _y;
		}
	
		public Point(float _x, float _y) {
			x = Mathf.RoundToInt(_x);
			y = Mathf.RoundToInt(_y);
		}
		
		public Point(Vector2 v) {
			x = Mathf.RoundToInt(v.x);
			y = Mathf.RoundToInt(v.y);
		}
		
		public Point (int[] xy) {
			x = xy[0];
			y = xy[1];
		}

		public static Point Min (Point p1, Point p2) {
			Vector2 vector;
			vector.x = (p1.x < p2.x) ? p1.x : p2.x;
			vector.y = (p1.y < p2.y) ? p1.y : p2.y;
		    return vector;
		}

		public static Point Max (Point p1, Point p2) {
			Vector2 vector;
			vector.x = (p1.x < p2.x) ? p2.x : p1.x;
			vector.y = (p1.y < p2.y) ? p2.y : p1.y;
		    return vector;
		}
		public static int ManhattanDistance(Point p1, Point p2) {
			return Mathf.Abs(p1.x-p2.x) + Mathf.Abs(p1.y-p2.y);
		}
	
		public static Point FromVector2(Vector2 vector) {
			return new Point(Mathf.RoundToInt(vector.x), Mathf.RoundToInt(vector.y));
		}
	
		public static Point FromVector3(Vector3 vector) {
			return new Point(Mathf.RoundToInt(vector.x), Mathf.RoundToInt(vector.y));
		}
	
		public static Vector2 ToVector2(Point point) {
			return new Vector2(point.x, point.y);
		}
	
		public static Vector3 ToVector3(Point point) {
			return new Vector3(point.x, point.y, 0);
		}
	
		public Vector2 ToVector2() {
			return ToVector2(this);
		}
	
		public Vector3 ToVector3() {
			return ToVector3(this);
		}
	
		public override string ToString() {
			return "(" + x + ", " + y+")";
		}
	
		public int area {
			get { return x * y; }
		}
	
		public float magnitude {
			get { return Mathf.Sqrt(x * x + y * y); }
		}
	
		public int sqrMagnitude {
			get { return x * x + y * y; }
		}

		public static Point Add(Point left, Point right){
			return new Point(left.x+right.x, left.y+right.y);
		}
	
		public static Point Add(Point left, float right){
			return new Point(left.x+right, left.y+right);
		}
	
		public static Point Add(float left, Point right){
			return new Point(left+right.x, left+right.y);
		}
	
	
		public static Point Subtract(Point left, Point right){
			return new Point(left.x-right.x, left.y-right.y);
		}
	
		public static Point Subtract(Point left, float right){
			return new Point(left.x-right, left.y-right);
		}
	
		public static Point Subtract(float left, Point right){
			return new Point(left-right.x, left-right.y);
		}
	
	
		public static Point Multiply(Point left, Point right){
			return new Point(left.x*right.x, left.y*right.y);
		}
	
		public static Point Multiply(Point left, float right){
			return new Point(left.x*right, left.y*right);
		}
	
		public static Point Multiply(float left, Point right){
			return new Point(left*right.x, left*right.y);
		}
	
	
		public static Point Divide(Point left, Point right){
			return new Point(left.x/right.x, left.y/right.y);
		}
	
		public static Point Divide(Point left, float right){
			return new Point(left.x/right, left.y/right);
		}
	
		public static Point Divide(float left, Point right){
			return new Point(left/right.x, left/right.y);
		}
	
		public override bool Equals(System.Object obj) {
			// If parameter is null return false.
			if (obj == null) {
				return false;
			}
	
			// If parameter cannot be cast to Point return false.
			Point p = (Point)obj;
			if ((System.Object)p == null) {
				return false;
			}
	
			// Return true if the fields match:
			return (x == p.x) && (y == p.y);
		}
	
		public bool Equals(Point p) {
			// If parameter is null return false:
			if ((object)p == null) {
				return false;
			}
	
			// Return true if the fields match:
			return (x == p.x) && (y == p.y);
		}
	
		public override int GetHashCode() {
			return x * y;
		}
	
		public static bool operator == (Point left, Point right) {
			if (System.Object.ReferenceEquals(left, right))
			{
				return true;
			}
	
			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}
			if(left.x == right.x && left.y == right.y)return true;
			return false;
		}
	
		public static bool operator != (Point left, Point right) {
			return !(left == right);
		}
	
		public static Point operator +(Point left, Point right) {
			return Add(left, right);
		}

		public static Point operator -(Point left) {
			return new Point(-left.x, -left.y);
		}

		public static Point operator -(Point left, Point right) {
			return Subtract(left, right);
		}
	
		public static Point operator -(Vector2 left, Point right) {
			return Subtract(left, right);
		}
	
		public static Point operator -(Point left, Vector2 right) {
			return Subtract(left, right);
		}
	
	
		public static Point operator *(Point left, Point right) {
			return Multiply(left, right);
		}
	
		public static Point operator *(Vector2 left, Point right) {
			return Multiply(left, right);
		}
	
		public static Point operator *(Point left, Vector2 right) {
			return Multiply(left, right);
		}
		
		public static Point operator *(Point left, float right) {
			return Multiply(left, right);
		}
	
	
		public static Point operator /(Point left, Point right) {
			return Divide(left, right);
		}
	
		public static Point operator /(Vector2 left, Point right) {
			return Divide(left, right);
		}
	
		public static Point operator /(Point left, Vector2 right) {
			return Divide(left, right);
		}
		
		public static Point operator /(Point left, float right) {
			return Divide(left, right);
		}
	
		public static implicit operator Point(Vector2 src) {
			return FromVector2(src);
		}
	
		public static implicit operator Point(Vector3 src) {
			return FromVector3(src);
		}
		
		public static implicit operator Vector2(Point src) {
			return src.ToVector2();
		}
	
		public static implicit operator Vector3(Point src) {
			return src.ToVector3();
		}
	}
//}