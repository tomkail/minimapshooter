﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace UnityX.Geometry.Polygon {
		
	[System.Serializable]
	public struct Polygon {
		
		/// <summary>
		/// The vertices.
		/// </summary>
		public Vector2[] vertices;
		
		/// <summary>
		/// Gets the vert count.
		/// </summary>
		/// <value>The vert count.</value>
		public int VertCount {
			get {
				if(vertices == null) 
					return 0;
				else 
					return vertices.Length;
			}
		}
		
		/// <summary>
		/// Gets a value indicating whether this <see cref="UnityX.Geometry.Polygon.Polygon"/> is connected.
		/// </summary>
		/// <value><c>true</c> if connected; otherwise, <c>false</c>.</value>
		public bool connected {
			get {
				return (!vertices.IsNullOrEmpty() && vertices[0] == vertices[VertCount-1]);
			}
		}
		
		/// <summary>
		/// Determines whether this instance is valid.
		/// </summary>
		/// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
		public bool IsValid () {
			if(vertices.IsNullOrEmpty()) 
				return false;
			if (connected)
			{
				return vertices.Length >= 3;
			}
			else
			{
				return vertices.Length >= 2;
			}
		}
		
		public static Polygon triangle {
			get {
				return new RegularPolygon(3).ToPolygon();
			}
		}
		
		public static Polygon square {
			get {
				return new RegularPolygon(4).ToPolygon();
			}
		}
		
		public static Polygon pentagon {
			get {
				return new RegularPolygon(5).ToPolygon();
			}
		}
		
		public static Polygon hexagon {
			get {
				return new RegularPolygon(6).ToPolygon();
			}
		}
		
		public Polygon (params Vector2[] _vertices) {
			vertices = _vertices;
		}
		
		public Polygon (Polygon _polygon) {
			vertices = (Vector2[]) _polygon.vertices.Clone();
		}
		
		
		public Bounds GetBounds () {
			Bounds bounds = new Bounds();
			if(!IsValid()) 
				return bounds;

			Vector2 smallest = vertices[0];
			float smallestMagnitude = vertices[0].sqrMagnitude;
			Vector2 largest = vertices[0];
			float largestMagnitude = vertices[0].sqrMagnitude;
			for(int i = 1; i < vertices.Length; i++){
				float currentMagnitude = vertices[i].sqrMagnitude;
				if(currentMagnitude > largestMagnitude) {
					largestMagnitude = currentMagnitude;
					largest = vertices[i];
				}
				if(currentMagnitude < smallestMagnitude) {
					smallestMagnitude = currentMagnitude;
					smallest = vertices[i];
				}
			}

			bounds.SetMinMax(smallest, largest);
			for(int i = 0; i < vertices.Length; i++) {
				bounds.Encapsulate(vertices[i]);
			}
			return bounds;
		}


		/// <summary>
		/// Returns a scaled polygon.
		/// </summary>
		/// <param name="_polygonDefinition">_polygon definition.</param>
		/// <param name="_scaleModifier">_scale modifier.</param>
		public static Polygon Scale (Polygon _polygonDefinition, Polygon _scaleModifier) {
			Polygon newPolygonDefinition = new Polygon(_polygonDefinition);
			if(newPolygonDefinition.vertices.Length > _scaleModifier.vertices.Length) 
				DebugX.Log(null, "Cannot Scale PolygonDefinition because the input modifier does not have enough vertices. It has "+_scaleModifier.vertices.Length+". It requires at least "+_polygonDefinition.vertices.Length+".");
			for(int i = 0; i < _polygonDefinition.vertices.Length; i++) {
				Vector2.Scale(newPolygonDefinition.vertices[i], _scaleModifier.vertices[i]);
			}
	
			return newPolygonDefinition;
		}
		
		/// <summary>
		/// Returns a scaled polygon.
		/// </summary>
		/// <param name="_polygonDefinition">_polygon definition.</param>
		/// <param name="_scaleModifier">_scale modifier.</param>
		public static Polygon Scale (Polygon _polygonDefinition, Vector2 _scaleModifier) {
			if(_scaleModifier == Vector2.one) return _polygonDefinition;
			
			Polygon newPolygonDefinition = new Polygon(_polygonDefinition);
			for(int i = 0; i < _polygonDefinition.vertices.Length; i++) {
				Vector2.Scale(newPolygonDefinition.vertices[i], _scaleModifier);
			}
			return newPolygonDefinition;
		}
		
		/// <summary>
		/// Returns a scaled polygon.
		/// </summary>
		/// <param name="_polygonDefinition">_polygon definition.</param>
		/// <param name="_scaleModifier">_scale modifier.</param>
		public static Polygon Scale (Polygon _polygonDefinition, float _scaleModifier) {
			if(_scaleModifier == 1) return _polygonDefinition;
			
			Polygon newPolygonDefinition = new Polygon(_polygonDefinition);
			for(int i = 0; i < _polygonDefinition.vertices.Length; i++) {
				newPolygonDefinition.vertices[i] = _polygonDefinition.vertices[i] * _scaleModifier;
			}
			return newPolygonDefinition;
		}
		
		/// <summary>
		/// Returns an expanded polygon, making it wider or thinner.
		/// </summary>
		/// <param name="_polygonDefinition">_polygon definition.</param>
		/// <param name="_scaleModifier">_scale modifier.</param>
		public void Expand (float _expansion) {
			if(_expansion == 0) return;
			
			for(int i = 0; i < vertices.Length; i++) {
				vertices[i] = vertices[i].normalized * (vertices[i].magnitude + _expansion);
			}
		}
		
		
		/// <summary>
		/// Gets the vertex.
		/// </summary>
		/// <returns>The vertex.</returns>
		/// <param name="edgeIndex">Edge index.</param>
		public Vector2 GetVertex (int edgeIndex) {
			return vertices[(int)GetRepeatingVertexIndex(edgeIndex)];
		}
		
		/// <summary>
		/// Gets the vertex.
		/// </summary>
		/// <returns>The vertex.</returns>
		/// <param name="edgeIndex">Edge index.</param>
		public Vector2 GetBestVertex (float edgeDistance) {
			return GetVertex(Mathf.RoundToInt (edgeDistance));
		}
		
		
		/// <summary>
		/// Gets the edge position from the distance around the shape.
		/// Whole numbers are vertices.
		/// </summary>
		/// <returns>The edge position.</returns>
		/// <param name="edgeIndex">Edge index.</param>
		public Vector2 GetEdgePosition (float edgeDistance) {
			edgeDistance = GetRepeatingVertexIndex(edgeDistance);
			if(edgeDistance.IsWhole()) 
				return GetVertex((int)edgeDistance);
			int leftIndex = Mathf.FloorToInt(edgeDistance);
			int rightIndex = leftIndex+1;
			return Vector2.Lerp(GetVertex(leftIndex), GetVertex(rightIndex), edgeDistance-leftIndex);
		}
		
		//Returns the angle of a side of a shape at a distance. 
		//If the shape is a triangle, the angle of the sides are 0, 120 and 240.
		public float GetEdgeDegreesFromCenter(float edgeDistance){
			return Vector2X.Degrees(GetEdgePosition(edgeDistance));
		}
		
		//Returns the angle of a side of a shape at a distance. 
		//If the shape is a triangle, the angle of the sides are 0, 120 and 240.
		public float GetVertexDegreesInternal(int vertIndex){
			Vector2 leftDir = GetVertex(vertIndex) - GetVertex(vertIndex-1);
			Vector2 rightDir = GetVertex(vertIndex) - GetVertex(vertIndex+1);
			return Vector2X.DegreesBetween(leftDir, rightDir);
		}
		
		public float GetVertexDegreesInternal (int i = 0, int j = 1) {
			i = (int)GetRepeatingVertexIndex(i);
			j = (int)GetRepeatingVertexIndex(j);
			return Vector2X.DegreesBetween(vertices[i], vertices[j]);
		}
		
		/// <summary>
		/// Gets the edge at edge distance.
		/// </summary>
		/// <returns>The edge at the edge distance.</returns>
		/// <param name="edgeDistance">Edge distance.</param>
		public Line GetEdge (float edgeDistance) {
			int leftIndex = Mathf.FloorToInt(edgeDistance);
			int rightIndex = leftIndex+1;
			return GetEdge(leftIndex, rightIndex);
		}
		
		/// <summary>
		/// Gets the edge between two vertex indices.
		/// If the two verts are not adjacent, the edge is technically known as a diagonal.
		/// </summary>
		/// <returns>The edge.</returns>
		/// <param name="i">The index.</param>
		/// <param name="j">J.</param>
		public Line GetEdge (int i = 0, int j = 1) {
			return new Line(GetVertex(i), GetVertex(j));
		}
		
		/// <summary>
		/// Gets the edge length between two vert indices.
		/// If the two verts are not adjacent, the edge is technically known as a diagonal.
		/// </summary>
		/// <returns>The edge.</returns>
		/// <param name="i">The index.</param>
		/// <param name="j">J.</param>
		public float GetEdgeLength (int i = 0, int j = 1) {
			return Vector2.Distance(GetVertex(i), GetVertex(j));
		}
		
		
		
		
		
		
		/// <summary>
		/// Gets the edge center.
		/// </summary>
		/// <returns>The edge center.</returns>
		/// <param name="edgeDistance">Edge distance.</param>
		public Vector2 GetEdgeCenter (float edgeDistance) {
			int leftIndex = Mathf.FloorToInt(edgeDistance);
			int rightIndex = leftIndex+1;
			return GetEdgeCenter(leftIndex, rightIndex);
		}
		
		/// <summary>
		/// Gets the edge between two vert indices.
		/// If the two verts are not adjacent, the edge is technically known as a diagonal.
		/// </summary>
		/// <returns>The edge.</returns>
		/// <param name="i">The index.</param>
		/// <param name="j">J.</param>
		public Vector2 GetEdgeCenter (int i = 0, int j = 1) {
			return Vector2.Lerp(GetVertex(i), GetVertex(j), 0.5f);
		}
		
		/// <summary>
		/// Gets the edge center.
		/// </summary>
		/// <returns>The edge center.</returns>
		/// <param name="edgeDistance">Edge distance.</param>
		public Vector2[] GetEdgeCenters () {
			Vector2[] centers = new Vector2[VertCount];
			for(int i = 0; i < centers.Length; i++) {
				centers[i] = GetEdgeCenter(i, i+1);
			}
			return centers;
		}
		
		


		

		public Vector2 GetInnermostVertex () {
			return Closest(Vector2.zero, new List<Vector2>(vertices));
		}
		
		public Vector2 GetInnermostEdge () {
			return Closest(Vector2.zero, new List<Vector2>(GetEdgeCenters()));
		}
		
		public Vector2 GetInnermostPoint () {
			return Closest(Vector2.zero, new List<Vector2>(vertices.Concat<Vector2>(GetEdgeCenters())));
		}
		
		
		public Vector2 GetOutermostVertex () {
			return Furthest(Vector2.zero, new List<Vector2>(vertices));
		}
		
		public Vector2 GetOutermostEdge () {
			return Furthest(Vector2.zero, new List<Vector2>(GetEdgeCenters()));
		}
		
		public Vector2 GetOutermostPoint () {
			return Furthest(Vector2.zero, new List<Vector2>(vertices.Concat<Vector2>(GetEdgeCenters())));
		}

		
		/// <summary>
		/// Gets the complete graph, a list of all the lines that can be formed by connecting the verts. 
		/// </summary>
		/// <returns>The complete graph.</returns>
		public Line[] GetCompleteGraph () {
			List<Line> _lines = new List<Line>();
			for(int i = 0; i < vertices.Length; i++) {
				for(int j = 0; j < vertices.Length; j++) {
					if(i == j) continue;
					bool found = false;
					Line tmpLine = new Line(vertices[i], vertices[j]);
					for(int k = 0; k < _lines.Count; k++) {
						if(Line.Equals(tmpLine, _lines[k])) {
							found = true;
							break;
						}
					}
					if(!found) {
						_lines.Add(new Line(vertices[i], vertices[j]));
					}
				}
			}
			return _lines.ToArray();
		}
	
		public static Polygon Lerp (Polygon from, Polygon to, float lerp) {
			if(from.vertices.Length == to.vertices.Length) return LerpEqualVertCount (from, to, lerp);
			else return LerpDifferentVertCount (from, to, lerp);
		}
	
	
		private static Polygon LerpEqualVertCount (Polygon from, Polygon to, float lerp) {
			Vector2[] newPolygonVerts = new Vector2[from.VertCount];
			for(int i = 0; i < newPolygonVerts.Length; i++) {
				newPolygonVerts[i] = Vector2.Lerp(from.vertices[i], to.vertices[i], lerp);
			}
			return new Polygon(newPolygonVerts);
		}
	
		private static Polygon LerpDifferentVertCount (Polygon from, Polygon to, float lerp) {
			// Vert count of the PolygonDefinition with the least verts
			int minVerts = Mathf.Min(from.vertices.Length, to.vertices.Length);
			// Vert count of the PolygonDefinition with the most verts
			int maxVerts = Mathf.Max(from.vertices.Length, to.vertices.Length);
			// Interpolated vert count between the min and max vert count, rounded up to the nearest whole number.
			Polygon largestPolygon = from.vertices.Length > to.vertices.Length ? from : to;
			
			//Polygon verts
			Vector2[] newPolygonVerts = new Vector2[maxVerts];
			
			for(int i = 0; i < minVerts; i++) {
				newPolygonVerts[i] = Vector2.Lerp(from.vertices[i], to.vertices[i], lerp);
			}
			
			for(int i = minVerts; i < maxVerts; i++) {
//				float tempLerp = MathX.Fraction((i-minVerts)+lerp, maxVerts-minVerts);
//				Debug.Log(lerp+" "+tempLerp+" "+i+ " "+minVerts+" "+maxVerts+" ");
				if(from.vertices.Length > to.vertices.Length) {
					newPolygonVerts[i] = Vector2.Lerp(largestPolygon.vertices[i], newPolygonVerts[i-1], lerp);
				} else {
					newPolygonVerts[i] = Vector2.Lerp(newPolygonVerts[i-1], largestPolygon.vertices[i], lerp);
				}
			}
			return new Polygon(newPolygonVerts);
		}
		
		
//		private static Polygon LerpDifferentVertCount (Polygon from, Polygon to, float lerp) {
//			// Vert count of the PolygonDefinition with the least verts
//			int minVerts = Mathf.Min(from.vertices.Length, to.vertices.Length);
//			// Vert count of the PolygonDefinition with the most verts
//			int maxVerts = Mathf.Max(from.vertices.Length, to.vertices.Length);
//			// Interpolated vert count between the min and max vert count, rounded up to the nearest whole number.
//			int lerpedMaxVerts = Mathf.CeilToInt(Mathf.Lerp(minVerts, maxVerts, lerp));
//			// Difference between the maximum and minimum vert count
//			int vertDifference = maxVerts - minVerts;
//			// Difference between the lerped number of verts and the vert count
//			int lerpedVertDifference = lerpedMaxVerts - minVerts;
//			
//			//Polygon verts
//			Vector2[] newPolygonVerts = new Vector2[lerpedMaxVerts];
//			
//			for(int i = 0; i < minVerts; i++) {
//				newPolygonVerts[i] = Vector2.Lerp(from.vertices[i], to.vertices[i], lerp*(lerpedVertDifference+1));
//				//newPolygonVerts[i] = Vector2.Lerp(from.vertices[i], to.vertices[i], lerp);
//				//newPolygonVerts[i] = to.vertices[i];
//			}
//			
//			for(int i = minVerts; i < lerpedMaxVerts; i++) {
//				//				float tempLerp = (lerp * (vertDifference)) - (i - minVerts+1);
//				float tempLerp = MathX.Fraction((i-minVerts)+lerp, lerpedMaxVerts-minVerts);
//				Debug.Log(lerp+" "+tempLerp+" "+i+ " "+minVerts+" "+lerpedMaxVerts+" ");
//				if(i == lerpedMaxVerts) {
//					Debug.Log("Init: "+newPolygonVerts[to.vertices.Length-i]+" Dest: "+to.vertices[i]);
//				}
//				//if(i > from.vertices.Length) {
//				//newPolygonVerts[i] = newPolygonVerts[i-1];
//				
//				newPolygonVerts[i] = Vector2.Lerp(Vector2.zero, to.vertices[i], tempLerp);
//				//				newPolygonVerts[i] = Vector2.Lerp(newPolygonVerts[i-1], to.vertices[i], tempLerp);
//				//}
//			}
//			return new Polygon(newPolygonVerts);
//		}
		
		
		/// <summary>
		/// Array operator. 
		/// </summary>
		public Vector2 this[int key] {
			get {
				return vertices[key];
			} set {
				vertices[key] = value;
			}
		}
		
		
		/// <summary>
		/// Gets the index of the repeating vertex.
		/// </summary>
		/// <returns>The repeating vertex index.</returns>
		/// <param name="vertexIndex">Vertex index.</param>
		public float GetRepeatingVertexIndex (float vertexIndex) {
			return Mathf.Repeat(vertexIndex, VertCount);
		}



		public Vector2 FindClosestPointInPolygon(Vector2 point){
			if(ContainsPoint(point)) {
				return point;
			} else {
				return FindClosestPointOnPolygon(point);
			}
		}

		public Vector2 FindClosestPointOnPolygon(Vector2 point) {
			Vector2[] closestPoints = new Vector2[vertices.Length];
			for(int l = 0; l < vertices.Length; l++)
				if(l < vertices.Length-1)
					closestPoints[l] = GetClosestPointOnLineSegment(vertices[l], vertices[l+1], point);
				else
					closestPoints[l] = GetClosestPointOnLineSegment(vertices[l], vertices[0], point);

			return Closest(point, closestPoints);
		}

		public Vector2 GetClosestPointOnLineSegment(Vector2 A, Vector2 B, Vector2 P) {
			Vector2 AP = P - A;       //Vector from A to P   
			Vector2 AB = B - A;       //Vector from A to B  

			float magnitudeAB = AB.sqrMagnitude;     //Magnitude of AB vector (it's length squared)     
			float ABAPproduct = Vector2.Dot(AP, AB);    //The DOT product of a_to_p and a_to_b     
			float distance = ABAPproduct / magnitudeAB; //The normalized "distance" from a to your closest point  

			if (distance < 0) {
				return A;
			} else if (distance > 1) {
				return B;
			} else {
				return A + AB * distance;
			}
		}

		public bool ContainsPoint(Vector2 testPoint) {
			bool result = false;
			int j = vertices.Length-1;
			for (int i = 0; i < vertices.Length; i++) {
				if (vertices[i].y < testPoint.y && vertices[j].y >= testPoint.y || vertices[j].y < testPoint.y && vertices[i].y >= testPoint.y) {
					if (vertices[i].x + (testPoint.y - vertices[i].y) / (vertices[j].y - vertices[i].y) * (vertices[j].x - vertices[i].x) < testPoint.x) {
						result = !result;
					}
				}
				j = i;
			}
			return result;
		}

		/// <summary>
		/// Gets a random point inside a polygon. Not optimized - the best way to do it would be to triangulate the polygon, then pick a triangle based on a weighted selection of area, and pick a random point inside it.
		/// </summary>
		/// <returns>The random point in polygon.</returns>
		public Vector2 GetRandomPointInPolygon () {
			Rect rect = RectX.CreateEncapsulating(vertices);
			Vector2? randomPoint = null;
			while(randomPoint == null) {
				Vector2 testPoint = new Vector2 (Random.Range(rect.min.x, rect.max.x), Random.Range(rect.min.y, rect.max.y));
				if(ContainsPoint(testPoint)) {
					randomPoint = testPoint;
				}
			}
			return (Vector2)randomPoint;
		}

		public void CopyFrom(Polygon src) {
			vertices = new Vector2[src.vertices.Length];
			for (int i = 0; i < vertices.Length; ++i)
				vertices[i] = src.vertices[i];		
		}
		
		public bool CompareTo(Polygon src)
		{
			if (vertices.Length != src.vertices.Length) return false;
			for (int i = 0; i < vertices.Length; ++i)
				if (vertices[i] != src.vertices[i]) return false;
			return true;
		}


		
		
		
//		public override bool Equals(System.Object obj) {
//			if (obj == null) 
//				return false;
//			
//			Polygon p = obj as Polygon;
//			if ((System.Object)p == null) {
//				return false;
//			}
//			
//			// Return true if the fields match:
//			return (x == p.x) && (y == p.y);
//		}
//		
//		public bool Equals(Polygon p) {
//			// If parameter is null return false:
//			if ((object)p == null) {
//				return false;
//			}
//			
//			// Return true if the fields match:
//			return (x == p.x) && (y == p.y);
//		}
//		
//		public static bool operator == (Polygon left, Polygon right) {
//			if (System.Object.ReferenceEquals(left, right))
//			{
//				return true;
//			}
//			
//			// If one is null, but not both, return false.
//			if (((object)left == null) || ((object)right == null))
//			{
//				return false;
//			}
//			if(left.x == right.x && left.y == right.y)return true;
//			return false;
//		}
//		
//		public static bool operator != (Polygon left, Polygon right) {
//			return !(left == right);
//		}

		public override string ToString () {
			string s = "Polygon: VertCount="+VertCount+" Connected="+connected+" Valid="+connected;
			for (int i = 0; i < vertices.Length; ++i)
				s += "\n"+i+": "+vertices[i];
			return s;
		}

		/// <summary>
		/// Returns the index of the closest vector to v in the values list
		/// </summary>
		/// <returns>The index.</returns>
		/// <param name="v">V.</param>
		/// <param name="values">Values.</param>
		private static int ClosestIndex(Vector2 v, IList<Vector2> values){
			int index = 0;
			float closest = Vector2X.SqrDistance(v, values[index]);
			for(int i = 1; i < values.Count; i++){
				float distance = Vector2X.SqrDistance(v, values[i]);
				if (distance < closest) {
					closest = distance;
					index = i;
				}
			}
			return index;
		}

		/// <summary>
		/// Returns the closest vector to v in the values list
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="values">Values.</param>
		public static Vector2 Closest(Vector2 v, IList<Vector2> values){
			return values[ClosestIndex(v, values)];
		}

		/// <summary>
		/// Returns the index of the furthest vector to v in the values list
		/// </summary>
		/// <returns>The index.</returns>
		/// <param name="v">V.</param>
		/// <param name="values">Values.</param>
		public static int FurthestIndex(Vector2 v, IList<Vector2> values){
			if(values.Count == 0) {
				Debug.LogError("Values is empty!");
				return -1;
			}
			int index = 0;
			float furthest = Vector2X.SqrDistance(v, values[index]);
			for(int i = 1; i < values.Count; i++){
				float distance = Vector2X.SqrDistance(v, values[i]);
				if (distance > furthest) {
					furthest = distance;
					index = i;
				}
			}
			return index;
		}

		/// <summary>
		/// Returns the furthest vector to v in the values list
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="values">Values.</param>
		public static Vector2 Furthest(Vector2 v, IList<Vector2> values){
			return values[FurthestIndex(v, values)];
		}


	}
}