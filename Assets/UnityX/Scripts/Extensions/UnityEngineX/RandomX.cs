﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class RandomX {
	
	/// <summary>
	/// Returns random true/false
	/// </summary>
	public static bool boolean {
		get { return (UnityEngine.Random.value > 0.5f); }
	}
	
	/// <summary>
	/// Returns a random Vector2 of a circle edge 
	/// </summary>
	public static Vector2 onUnitCircle {
		get {
			float angle = RandomX.eulerAngle;
			return MathX.DegreesToVector2(angle);
		}
	}

	/// <summary>
	/// Returns a random float between 0 and 360
	/// </summary>
	public static float eulerAngle {
		get {
			return UnityEngine.Random.value*360;
		}
	}
	
	/// <summary>
	/// Returns a boolean from a chance value between 0 and 1, where 0.25 is unlikely and 0.75 is likely.
	/// </summary>
	/// <param name="chance">Chance.</param>
	public static bool Chance(float chance) {
		return chance > UnityEngine.Random.value;
	}

	/// <summary>
	/// Returns a random index, using the value of each array item as a weight
	/// </summary>
	/// <returns>The random index.</returns>
	/// <param name="weights">Weights.</param>
	public static int WeightedIndex(IList<float> weights){
		return RandomX.WeightedIndex(weights, weights.Sum());
	}
	
	/// <summary>
	/// Returns a random index, using the value of each array item as a weight
	/// </summary>
	/// <returns>The random index.</returns>
	/// <param name="weights">Weights.</param>
	/// <param name="total">Total.</param>
	public static int WeightedIndex(IList<float> weights, float total){
		if(weights.IsNullOrEmpty()) {
			Debug.LogError("Weights array is null or empty");
			return -1;
		}
		if(total == 0) return weights.RandomIndex();
		float currentValue = 0;
		float randomValue = UnityEngine.Random.Range(0f, total);
		for(int i = 0; i < weights.Count; i++){
			currentValue += weights[i];
			if(currentValue >= randomValue) return i;
		}
		Debug.LogError("Could not find a value. Total was: "+total+" and num values was "+weights.Count);
		return -1;
	}
}
