using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Debug extension class
/// </summary>
public static class DebugX {
	#region
	public static bool debug = true;
	
	public static bool Assert (System.Object o) {
		return Assert(o != null, "Error: Assert failed because object is null");
	}
	
	public static bool Assert(bool condition) {
		return Assert (condition, "Error: Assert Failed!");
	}
	
	public static bool Assert(bool condition, string message) {
		if (!condition)
			LogError (message);
		return condition;
	}
	
	public static bool Assert (System.Object a, System.Object o) {
		return Assert(a, o != null, "Error: Assert failed because object is null");
	}
	
	public static bool Assert(System.Object a, bool condition) {
		return Assert (a, condition, "Error: Assert Failed!");
	}
	
	public static bool Assert(System.Object a, bool condition, string message) {
		if (!condition)
			LogError (a, message);
		return condition;
	}
	
	/// <summary>Returns a log for the target string, along with the current time and the log source.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	/// <returns>The string.</returns>
	public static string LogString (System.Object a) {
		return "("+Time.time+")"+" : "+a.ToString();
	}
	
	/// <summary>Returns a log for the target string, along with the current time and the log source.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	/// <returns>The string.</returns>
	public static string LogString (System.Object obj, System.Object a) {
		string log = "("+Time.time+")";
		if(obj is MonoBehaviour) {
			log += " "+((MonoBehaviour)obj).transform.HierarchyPath();
		}
		log += " ("+obj.GetType().Name+")";
		return a.ToString()+"\n"+log;
	}
	
	// <summary>Returns a log for the target string, along with the current time and the log source, in a particular color.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	/// <returns>The string.</returns>
	public static string LogString (System.Object obj, System.Object a, Color color) {
		return LogString(obj, a).AsRichText(color);
	}
	
	/// <summary>Logs the target string.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	public static void Log (System.Object a) {
		if(!debug) return;
		Debug.Log(LogString(a));
	}
	
	/// <summary>Logs the target string in a particular color.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="color">The log color.</param>
	public static void Log (System.Object obj, Color color) {
		if(!debug) return;
		Debug.Log(LogString(obj,null,color));
	}
	
	/// <summary>Logs the target string, along with the current time and the log source.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	public static void Log (System.Object obj, System.Object a) {
		if(!debug) return;
		Debug.Log(LogString(obj,a));
	}
	
	/// <summary>Logs the target string, along with the current time and the log source, in a particular color.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	/// <param name="color">The log color.</param>
	public static void Log (System.Object obj, System.Object a, Color color) {
		if(!debug) return;
		Debug.Log(LogString(obj,a,color));
	}
	
	/// <summary>Logs a warning with the target string, along with the current time and the log source.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	public static void LogWarning (System.Object a) {
		if(!debug) return;
		Debug.LogWarning(LogString(a));
	}
	
	/// <summary>Logs a warning with the target string, along with the current time and the log source.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	public static void LogWarning (System.Object obj, System.Object a) {
		if(!debug) return;
		Debug.LogWarning(LogString(obj,a));
	}
	
	/// <summary>Logs a warning with the target string, along with the current time and the log source, in a particular color.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	/// <param name="color">The log color.</param>
	public static void LogWarning (System.Object obj, System.Object a, Color color) {
		if(!debug) return;
		Debug.LogWarning(LogString(obj,a,color));
	}
	
	/// <summary>Logs an error with the target string, along with the current time and the log source.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	/// <param name="color">The log color.</param>
	public static void LogError (System.Object a) {
		if(!debug) return;
		Debug.LogError(LogString(a));
	}
	
	/// <summary>Logs an error with the target string, along with the current time and the log source.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	/// <param name="color">The log color.</param>
	public static void LogError (System.Object obj, System.Object a) {
		if(!debug) return;
		Debug.LogError(LogString(obj,a));
	}
	
	/// <summary>Logs an error with the target string, along with the current time and the log source, in a particular color.</summary>
	/// <param name="o">The log source.</param>
	/// <param name="item">The log item.</param>
	/// <param name="color">The log color.</param>
	public static void LogError (System.Object obj, System.Object a, Color color) {
		if(!debug) return;
		Debug.LogError(LogString(obj,a,color));
	}
	
	public static void LogIfNull (System.Object o, System.Object a) {
		if(a == null)
			DebugX.LogError(o, "Object is null!");
	}
	
	/// <summary>LogMany</summary>
	/// <param name="list">The objects to log.</param>
	public static void LogMany (params object[] list) {
		if(!debug) return;
		Debug.Log(DebugX.ListAsString(list));
	}
	
	/// <summary>LogWarningMany</summary>
	/// <param name="list">The objects to log warning.</param>
	public static void LogWarningMany (params object[] list) {
		if(!debug) return;
		Debug.LogWarning(DebugX.ListAsString(list));
	}
	
	/// <summary>LogErrorMany</summary>
	/// <param name="list">The objects to log error.</param>
	public static void LogErrorMany (params object[] list) {
		if(!debug) return;
		Debug.LogError(DebugX.ListAsString(list));
	}
	
	/// <summary>Gets a log string from an object array.</summary>
	/// <param name="list">The list of objects to log.</param>
	/// <returns>string</returns>
	public static string ListAsString<T> (IList<T> list) {
		string output = "Displaying list of "+typeof(T).Name+" with "+list.Count +" values:";
		for(int i = 0; i < list.Count; i++) {
			output += "\n";
			if(list[i] == null) {
				output += "NULL";
			} else {
				output += list[i].ToString();
			}
		}
		return output;
	}
	
	/// <summary>Clearly logs the contents of a list.</summary>
	/// <typeparam name="T">The generic list type.</typeparam>
	/// <param name="list">The list to log.</param>
	public static void LogList<T> (IList<T> list) {
		if(list.IsNullOrEmpty()) {
			Debug.Log("List is null or empty");
			return;
		}
		#if !UNITY_WINRT
		string output = ListAsString(list);
		Debug.Log(output);
		#endif
	}

	/// <summary>Clearly logs the contents of a list.</summary>
	/// <typeparam name="T">The generic list type.</typeparam>
	/// <param name="list">The list to log.</param>
	public static void LogList<T> (string log, IList<T> list) {
		#if !UNITY_WINRT
		if(list.IsNullOrEmpty()) {
			Debug.Log(log+"\n"+"List is null or empty");
		} else {
			Debug.Log(log+"\n"+ListAsString(list));
		}
		#endif
	}
	
	
	public static void LogDictionary<T,Q> (IDictionary<T,Q> dictionary) {
		if(dictionary.IsNullOrEmpty()) {
			Debug.Log("Dictionary is null or empty");
			return;
		}
		#if !UNITY_WINRT
		string output = "Dictionary ("+dictionary.Count+") : ";
		int i = 0;
		foreach(KeyValuePair<T,Q> kvp in dictionary) {
			if(i > 0)output += "\n";
			output += "Key:"+kvp.Key.ToString()+" Value:"+kvp.Value.ToString();
			i++;
		}
		Debug.Log(output);
		#endif
	}
	#endregion
	
	// Following functions taken from UnityWiki, created by Hayden Scott-Baron (Dock) - http://starfruitgames.com
	
	/// <summary>Draws a cube in the scene window.</summary>
	/// <param name="pos">Position of the cube.</param>
	/// <param name="scale">Scale of the cube.</param>
	/// <param name="col">Color of the cube.</param>
	public static void DrawCube (Vector3 pos, Vector3 scale, Color col) {
		Vector3 halfScale = scale * 0.5f; 
		
		Vector3[] points = new Vector3 [] {
			pos + new Vector3(halfScale.x, 		halfScale.y, 	halfScale.z),
			pos + new Vector3(-halfScale.x, 	halfScale.y, 	halfScale.z),
			pos + new Vector3(-halfScale.x, 	-halfScale.y, 	halfScale.z),
			pos + new Vector3(halfScale.x, 		-halfScale.y, 	halfScale.z),			
			pos + new Vector3(halfScale.x, 		halfScale.y, 	-halfScale.z),
			pos + new Vector3(-halfScale.x, 	halfScale.y, 	-halfScale.z),
			pos + new Vector3(-halfScale.x, 	-halfScale.y, 	-halfScale.z),
			pos + new Vector3(halfScale.x, 		-halfScale.y, 	-halfScale.z),
		};
		
		Debug.DrawLine (points[0], points[1], col); 
		Debug.DrawLine (points[1], points[2], col); 
		Debug.DrawLine (points[2], points[3], col); 
		Debug.DrawLine (points[3], points[0], col); 
	}
	
	/// <summary>Draws a rect in the scene window.</summary>
	/// <param name="rect">Rect to draw.</param>
	/// <param name="col">Color of the rect.</param>
	public static void DrawRect (Rect rect, Color col) {
		Vector3 pos = new Vector3( rect.x + rect.width/2, rect.y + rect.height/2, 0.0f );
		Vector3 scale = new Vector3 (rect.width, rect.height, 0.0f );
		DebugX.DrawRect (pos, scale, col); 
	}
	
	/// <summary>Draws a rect in the scene window.</summary>
	/// <param name="pos">Position of the rect.</param>
	/// <param name="scale">Scale of the rect.</param>
	/// <param name="col">Color of the rect.</param>
	public static void DrawRect (Vector3 pos, Vector3 scale, Color col) {		
		Vector3 halfScale = scale * 0.5f; 
		Vector3[] points = new Vector3 [] {
			pos + new Vector3(halfScale.x, 		halfScale.y, 	halfScale.z),
			pos + new Vector3(-halfScale.x, 	halfScale.y, 	halfScale.z),
			pos + new Vector3(-halfScale.x, 	-halfScale.y, 	halfScale.z),
			pos + new Vector3(halfScale.x, 		-halfScale.y, 	halfScale.z),	
		};
		Debug.DrawLine (points[0], points[1], col); 
		Debug.DrawLine (points[1], points[2], col); 
		Debug.DrawLine (points[2], points[3], col); 
		Debug.DrawLine (points[3], points[0], col); 
	}
	
	/// <summary>Draws a point in the scene window.</summary>
	/// <param name="pos">Position of the rect.</param>
	/// <param name="scale">Scale of the rect.</param>
	/// <param name="col">Color of the rect.</param>
	public static void DrawPoint (Vector3 pos, float scale, Color col) {
		Vector3[] points = new Vector3[] 
		{
			pos + (Vector3.up * scale), 
			pos - (Vector3.up * scale), 
			pos + (Vector3.right * scale), 
			pos - (Vector3.right * scale), 
			pos + (Vector3.forward * scale), 
			pos - (Vector3.forward * scale)
		}; 		
		
		Debug.DrawLine (points[0], points[1], col); 
		Debug.DrawLine (points[2], points[3], col); 
		Debug.DrawLine (points[4], points[5], col); 
		
		Debug.DrawLine (points[0], points[2], col); 
		Debug.DrawLine (points[0], points[3], col); 
		Debug.DrawLine (points[0], points[4], col); 
		Debug.DrawLine (points[0], points[5], col); 
		
		Debug.DrawLine (points[1], points[2], col); 
		Debug.DrawLine (points[1], points[3], col); 
		Debug.DrawLine (points[1], points[4], col); 
		Debug.DrawLine (points[1], points[5], col); 
		
		Debug.DrawLine (points[4], points[2], col); 
		Debug.DrawLine (points[4], points[3], col); 
		Debug.DrawLine (points[5], points[2], col); 
		Debug.DrawLine (points[5], points[3], col); 
		
	}
	
	
	
	/*public static void DrawGrid  (Vector3 pos, Point size, Vector2 cellSize)
	{		
	
	}*/
}