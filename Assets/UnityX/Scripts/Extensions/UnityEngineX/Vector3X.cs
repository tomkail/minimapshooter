﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Vector3X {

	/// <summary>
	/// Returns a half.
	/// </summary>
	/// <value>The half.</value>
	public static Vector3 half {
		get {
			return new Vector3(0.5f, 0.5f, 0.5f);
		}
	}

	/// <summary>
	/// Returns direction from a to b.
	/// </summary>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	public static Vector3 FromTo(Vector3 a, Vector3 b){
		return b - a;
	}

	/// <summary>
	/// Changes the direction of the vector without affecting magnitude. Magnitude of direction vector is irrelevant.
	/// </summary>
	/// <returns>The rotated vector.</returns>
	/// <param name="vector">Vector.</param>
	/// <param name="newDirection">New direction.</param>
	public static Vector3 InDirection(Vector3 vector, Vector3 newDirection){
		return newDirection.normalized * vector.magnitude;
	}
	
	/// <summary>
	/// Returns normalized direction from a to b.
	/// </summary>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	public static Vector3 NormalizedDirection(Vector3 a, Vector3 b){
		return FromTo(a, b).normalized;
	}

	/// <summary>
	/// Reflects a vector off the plane defined by a normal, also using an optional coefficient of restituion.
	/// </summary>
	/// <param name="inDirection">In direction.</param>
	/// <param name="inNormal">In normal.</param>
	/// <param name="coeficientOfRestitution">Coeficient of restitution.</param>
	public static Vector3 Reflect(Vector3 inDirection, Vector3 inNormal, float coefficientOfRestitution = 1.0f) {
		return (2 * Vector3.Project(inDirection, inNormal.normalized) - inDirection) * coefficientOfRestitution;
	}

	public static float SqrDistance (Vector3 a, Vector3 b) {
		return (a-b).sqrMagnitude;
	}
	
	/// <summary>
	/// Returns the distance between two vectors in a specific direction, using projection.
	/// For example, if the direction is forward and a is (0,0,0) and b is (0,2,1), the function will return 1, as the upwards component of b is ignored by the forward direction.
	/// </summary>
	/// <returns>The in direction.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	/// <param name="direction">Direction.</param>
	public static float DistanceInDirection (Vector3 a, Vector3 b, Vector3 direction) {
		Vector3 normalizedDirection = direction.sqrMagnitude == 1 ? direction : direction.normalized;
		Vector3 projectedA = Vector3.Project(a, normalizedDirection);
		Vector3 projectedB = Vector3.Project(b, normalizedDirection);
		return Vector3.Distance(projectedA, projectedB);
	}
	
	/// <summary>
	/// Returns a Vector3 with all components made absolute.
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector3 Abs( this Vector3 v ){
		return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
	}

	/// <summary>
	/// Rotates the specified Vector3 as if it were a eulerAngle. 
	/// Shortcut to Quaternion.Rotate.
	/// </summary>
	/// <param name="rotation">Rotation.</param>
	/// <param name="eulerAngles">Euler angles.</param>
	/// <param name="space">Space.</param>
	public static Vector3 Rotate (this Vector3 rotation, Vector3 eulerAngles, Space space = Space.Self) {
		return (Quaternion.Euler(rotation).Rotate(eulerAngles, space)).eulerAngles;
	}
	
	public static Vector3 RotateX(this Vector3 v,  float angle) {
		float sin = Mathf.Sin( angle );
		float cos = Mathf.Cos( angle );
		float ty = v.y;
		float tz = v.z;
		v.y = (cos * ty) - (sin * tz);
		v.z = (cos * tz) + (sin * ty);

		return v;
	}

	public static Vector3 RotateY(this Vector3 v, float angle) {
		float sin = Mathf.Sin( angle );
		float cos = Mathf.Cos( angle );
		float tx = v.x;
		float tz = v.z;
		v.x = (cos * tx) + (sin * tz);
		v.z = (cos * tz) - (sin * tx);

		return v;
	}

	public static Vector3 RotateZ (this Vector3 v, float angle) {
		float sin = Mathf.Sin( angle );
		float cos = Mathf.Cos( angle );
		float tx = v.x;
		float ty = v.y;
		v.x = (cos * tx) - (sin * ty);
		v.y = (cos * ty) + (sin * tx);

		return v;
	}

	public static float GetPitch (this Vector3 v){
		float len = Mathf.Sqrt((v.x * v.x) + (v.z * v.z));    // Length on xz plane.
		return(-Mathf.Atan2(v.y, len));
	}

	public static float GetYaw (this Vector3 v)  {
		return( Mathf.Atan2( v.x, v.z ) );
	}

	/// <summary>
	/// Returns a sign indicating the direction of forward relative to the target direction, rotating around axis up.
	/// </summary>
	/// <returns>The dir.</returns>
	/// <param name="fwd">Fwd.</param>
	/// <param name="targetDir">Target dir.</param>
	/// <param name="up">Up. The axis the rotation check is measured against</param>
	public static float AngleDirection(Vector3 forward, Vector3 targetDir, Vector3 up) {
		Vector3 side = Vector3.Cross(forward, up);
		float dir = Vector3.Dot(targetDir, side);

		if (dir > 0.0f) {
			return 1.0f;
		} else if (dir < 0.0f) {
			return -1.0f;
		} else {
			return 0.0f;
		}
	}


	
	public static Vector3 ClampMagnitudeInDirection (Vector3 velocity, Vector3 direction, float clampValue, bool outwardsOnly = false) {
		float speedAlongTangent = Vector3.Dot(velocity, direction);
		if(Mathf.Abs(speedAlongTangent) > clampValue) {
			float clampedSpeed = Mathf.Clamp(speedAlongTangent, -clampValue, +clampValue);
			float speedDiff = clampedSpeed - speedAlongTangent;
			velocity += speedDiff * direction;
		}
		return velocity;
	}
	
	
	
	// To Vector2
	/// <summary>
	/// Creates a Vector2 from a Vector3, using the X and Y components (in that order).
	/// </summary> 
	public static Vector2 XY (this Vector3 v) {
		return new UnityEngine.Vector2(v.x,v.y);
	}

	/// <summary>
	/// Creates a Vector2 from a Vector3, using the X and Z components (in that order).
	/// </summary> 
	public static Vector2 XZ (this Vector3 v) {
		return new UnityEngine.Vector2(v.x, v.z);
	}



	/// <summary>
	/// Sets the value of the X component.
	/// </summary> 
	public static Vector3 WithX (this Vector3 v, float newX) {
		return new Vector3(newX, v.y, v.z);
	}
 	
 	/// <summary>
	/// Sets the value of the Y component
	/// </summary> 
	public static Vector3 WithY (this Vector3 v, float newY) {
		return new Vector3(v.x, newY, v.z);
	}
 	
 	/// <summary>
	/// Sets the value of the Z component
	/// </summary> 
	public static Vector3 WithZ (this Vector3 v, float newZ) {
		return new Vector3(v.x, v.y, newZ);
	}	
	
	

	/// <summary>
	/// Adds a value to the X component
	/// </summary> 
	public static Vector3 AddX (this Vector3 v, float addX) {
		return new Vector3(v.x + addX, v.y, v.z);
	}
 	
 	/// <summary>
	/// Adds a value to the Y component
	/// </summary> 
	public static Vector3 AddY (this Vector3 v, float addY) {
		return new Vector3(v.x, v.y + addY, v.z);
	}
	
	/// <summary>
	/// Adds a value to the Z component
	/// </summary> 
	public static Vector3 AddZ (this Vector3 v, float addZ) {
		return new Vector3(v.x, v.y, v.z + addZ);
	}

	/// <summary>
	/// Creates a vector3 from a vector2 where the x and y components are mapped to x and z
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector3 XZ(Vector2 v) {
		return new UnityEngine.Vector3(v.x, 0, v.y);
	}
}