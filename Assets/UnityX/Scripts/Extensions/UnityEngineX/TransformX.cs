﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class TransformX {
	///
	///Transform properties
	///
	
	/// <summary>
	/// Determines if the transform has default local values. If true, calling ResetTransform will change nothing.
	/// </summary>
	/// <returns><c>true</c> if is default the specified trans; otherwise, <c>false</c>.</returns>
	/// <param name="trans">Trans.</param>
	public static bool IsDefault(this Transform trans) {
		return trans.localPosition == Vector3.zero && trans.localRotation == Quaternion.identity && trans.localScale == Vector3.one;
	}

	/// <summary>
	/// Resets the transform locally.
	/// </summary>
	/// <param name="trans">Trans.</param>
	public static void ResetTransform(this Transform trans) {
		trans.localPosition = Vector3.zero;
		trans.localRotation = Quaternion.identity;
		trans.localScale = Vector3.one;
	}

	public static void SetPositionX(this Transform t, float newX) {
		t.position = new Vector3(newX, t.position.y, t.position.z);
	}
 
	public static void SetPositionY(this Transform t, float newY) {
		t.position = new Vector3(t.position.x, newY, t.position.z);
	}
 
	public static void SetPositionZ(this Transform t, float newZ) {
		t.position = new Vector3(t.position.x, t.position.y, newZ);
	}
	
	public static void SetLocalPositionX(this Transform t, float newX) {
		t.localPosition = new Vector3(newX, t.localPosition.y, t.localPosition.z);
	}
	
	public static void SetLocalPositionY(this Transform t, float newY) {
		t.localPosition = new Vector3(t.localPosition.x, newY, t.localPosition.z);
	}
	
	public static void SetLocalPositionZ(this Transform t, float newZ) {
		t.localPosition = new Vector3(t.localPosition.x, t.localPosition.y, newZ);
	}


	public static void SetEulerAnglesX(this Transform t, float newX) {
		t.eulerAngles = new Vector3(newX, t.eulerAngles.y, t.eulerAngles.z);
	}
	
	public static void SetEulerAnglesY(this Transform t, float newY) {
		t.eulerAngles = new Vector3(t.eulerAngles.x, newY, t.eulerAngles.z);
	}
	
	public static void SetEulerAnglesZ(this Transform t, float newZ) {
		t.eulerAngles = new Vector3(t.eulerAngles.x, t.eulerAngles.y, newZ);
	}

	public static void SetLocalEulerAnglesX(this Transform t, float newX) {
		t.localEulerAngles = new Vector3(newX, t.localEulerAngles.y, t.localEulerAngles.z);
	}
	
	public static void SetLocalEulerAnglesY(this Transform t, float newY) {
		t.localEulerAngles = new Vector3(t.localEulerAngles.x, newY, t.localEulerAngles.z);
	}
	
	public static void SetLocalEulerAnglesZ(this Transform t, float newZ) {
		t.localEulerAngles = new Vector3(t.localEulerAngles.x, t.localEulerAngles.y, newZ);
	}
	
	
	public static void SetLocalScaleX(this Transform t, float newX) {
		t.localScale = new Vector3(newX, t.localScale.y, t.localScale.z);
	}
	
	public static void SetLocalScaleY(this Transform t, float newY) {
		t.localScale = new Vector3(t.localScale.x, newY, t.localScale.z);
	}
	
	public static void SetLocalScaleZ(this Transform t, float newZ) {
		t.localScale = new Vector3(t.localScale.x, t.localScale.y, newZ);
	}

	///
	///Paths
	///

	/// <summary>
	/// Recursively travels to the top-level parent, or the parent at level maxLevels and outputs a string of the hierarchy path of the transform
	/// </summary>
	public static string HierarchyPath (this Transform t, int maxLevels = 0) {
		string path = "";
		//Transform p = t.GetParent(maxLevels);
		//return p.name;
		Transform[] parents = t.GetParents(maxLevels);
		for(int i = parents.Length-1; i >= 0; i--) {
			path += parents[i].name;
			path += "/";
		}
		path += t.name;
		return path;
	}

	/// <summary>
	/// Destroys all children.
	/// </summary>
	/// <param name="transform">The parent transform.</param>
	public static void DestroyAllChildren (this Transform transform) {
		for (int i = transform.childCount-1; i >= 0; i--) {
			Object.Destroy(transform.GetChild(i).gameObject);
		}
	}
	
	public static void DestroyAllChildrenImmediate (this Transform transform) {
		for (int i = transform.childCount-1; i >= 0; i--) {
			Object.DestroyImmediate(transform.GetChild(i).gameObject);
		}
	}
	
	
	
	/// <summary>
	/// Gets the children of the transform. 
	/// Remember that you can also loop through children using Transform's enumerator: foreach (Transform child in transform)
	/// </summary>
	/// <returns>The children.</returns>
	/// <param name="current">Current.</param>
	public static Transform[] GetChildren(this Transform current) {
		Transform[] children = new Transform[current.childCount];
		for (int i = 0; i < children.Length; i++) {
			children[i] = current.GetChild(i);
		}
		return children;
	}
	
	/// <summary>
	/// Gets the siblings of a transform.
	/// </summary>
	/// <returns>The siblings.</returns>
	/// <param name="current">Current.</param>
	public static Transform[] GetSiblings(this Transform current) {
		Transform[] siblingsIncludingSelf = current.parent.GetChildren();
		Transform[] siblings = new Transform[siblingsIncludingSelf.Length - 1];
		int siblingsIndex = 0;
		for (int i = 0; i < siblingsIncludingSelf.Length; i++) {
			if(siblingsIncludingSelf[i] != current) {
				siblings[siblingsIndex] = siblingsIncludingSelf[i];
				siblingsIndex++;
			}
		}
		return siblings;
	}
	
	
	///
	///Finding
	///

	/// <summary>
	/// Recursively travels to the top-level parent, or the parent at level maxLevels
	/// </summary>
	public static Transform GetParent (this Transform t, int maxLevels = 0) {
		maxLevels--;
		if(maxLevels == 0) {
			return t.parent;
		} else if(t.parent != null) {
			return GetParent(t.parent, maxLevels);
		} else {
			return t;
		}
	}
	
	/// <summary>
	/// Recursively travels to the top-level parent, or the parent at level maxLevels
	/// </summary>
	public static Transform[] GetParents (this Transform t, int maxLevels = 0, IList<Transform> parents = null) {
		if(parents == null) parents = new List<Transform>();
		maxLevels--;
		if(t.parent != null) {
			parents.Add(t.parent);
			if(maxLevels == 0) {
				return parents.ToArray();
			} 
			return GetParents(t.parent, maxLevels, parents);
		} else {
			return parents.ToArray();
		}
	}

	/// <summary>
	/// Recursively travels to the top-level parent, or the parent at level maxLevels
	/// </summary>
	public static int GetNumParents (this Transform t) {
		return t.GetParents().Length;
	}
	
	
	/// <summary>
	/// Finds the first transform in the descendants of the transform with a specified name.
	/// Searches using a breadth-first search, so it doesn't go too deeply too quickly.
	/// </summary>
	/// <returns>The in children.</returns>
	/// <param name="current">The start transform.</param>
	/// <param name="name">The name of the child to search for.</param>
	public static Transform FindInChildren(this Transform current, string name) {
		return FindInChildren<Transform>(current, name);
	}
		
	/// <summary>
	/// Breadth first search algorithm to find a child of a particular type, optionally with a particular name 
	/// in the full ancestry below a certain node.
	/// (We use breadth first so that we find the "shallowest" child possible, so we don't recursively go very
	/// deep very fast. It's a more complicated algorithm, but will be faster in most use cases.)
	/// </summary>
	/// <returns>A child Transform, if one exists with a given name.</returns>
	/// <param name="name">The exact name to search for.</param>
	public static T FindInChildren<T>(this Transform current, string name = null) where T : Component {
		return FindInChildren<T>(current, t => t.name == name);
	}

	/// <summary>
	/// Breadth first search algorithm to find a child of a particular type, optionally according to a particular
	/// predicate in the full ancestry below a certain node.
	/// (We use breadth first so that we find the "shallowest" child possible, so we don't recursively go very
	/// deep very fast. It's a more complicated algorithm, but will be faster in most use cases.)
	/// </summary>
	/// <returns>A child Transform, if one exists that passes the test of the predicate function.</returns>
	/// <param name="predicate">An optional predicate that returns true or false depending on whether
	/// we want to consider the given object.</param>
	public static T FindInChildren<T>(this Transform current, System.Predicate<T> predicate = null) where T : Component
	{
		// Keep queue around so that we allocate as little memory as possible for
		// multiple find calls.
		if( _objectQueue == null )
			_objectQueue = new Queue<Transform>();

		foreach(Transform child in current) {
			if( child.gameObject.activeInHierarchy )
				_objectQueue.Enqueue(child);
		}

		T result = FindInChildrenFromMainQueue<T>(predicate);

		// Clear up after ourselves
		_objectQueue.Clear();

		return result;
	}

	/// <summary>
	/// Private/internal function: using _objectQueue (constructed by FindInChildren for example),
	/// search for the objects within the queue, as well as within their children.
	/// </summary>
	/// <returns>A child component, if one exists with a given name.</returns>
	/// <param name="predicate">A predicate to test on each object of the given type.</param>
	/// <typeparam name="T">The specific component type to search for.</typeparam>
	static T FindInChildrenFromMainQueue<T>(System.Predicate<T> predicate) where T : Component
	{
		while(_objectQueue.Count > 0) {

			var child = _objectQueue.Dequeue();

			var comp = child.GetComponent<T>();
			if( comp ) {
				if(predicate == null || predicate(comp)) {
					return comp;
				}
			}

			foreach(Transform subChild in child) {
				_objectQueue.Enqueue(subChild);
			}
		}

		return null;
	}

	// Keep a global pool around so we don't have to keep allocating a new one.
	// Used for the above breadth first search system.
	static Queue<Transform> _objectQueue;

	/// <summary>
	/// Finds all transforms in the descendants of the transform with a specified name.
	/// </summary>
	/// <returns>The all in children.</returns>
	/// <param name="current">Current.</param>
	/// <param name="name">Name.</param>
	public static Transform[] FindAllInChildren(this Transform current, string name) {
		return current.FindAllInChildrenList(name).ToArray();
	}

	public static List<Transform> FindAllInChildrenList(this Transform current, string name, List<Transform> transforms = null) {
		if(transforms == null) transforms = new List<Transform>();
		//current.name.Contains(name)
		if (current.name == name)
			transforms.Add(current);
		for (int i = 0; i < current.childCount; ++i) {
			current.GetChild(i).FindAllInChildrenList(name, transforms);
		}
		return transforms;
	}

	/// <summary>
	/// Find the nearest component in a list to this Transform in 3d space, returning the distance found.
	/// </summary>
	public static T Nearest<T>(this Transform current, IEnumerable<T> objectList, out float distance) where T : Component
	{
		var currPos = current.position;

		float nearestDist = float.MaxValue;
		T nearestObj = null;
		foreach(var obj in objectList) {
			float dist = Vector3.Distance(currPos, obj.transform.position);
			if( nearestObj == null || dist < nearestDist) {
				nearestObj = obj;
				nearestDist = dist;
			}
		}

		distance = nearestDist;

		return nearestObj;
	}

	/// <summary>
	/// Find the nearest component in a list to this Transform in 3d space.
	/// </summary>
	public static T Nearest<T>(this Transform current, IEnumerable<T> objectList) where T : Component
	{
		float distance;
		return Nearest(current, objectList, out distance);
	}
}
