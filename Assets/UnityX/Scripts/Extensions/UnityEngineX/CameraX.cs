using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Extension methods for the UnityEngine.Camera class, and helper methods for working with cameras in general.
/// </summary>
public static class CameraX {

	#region Extensions for UnityEngine.Camera

	public static float CalculateOrthographicSize(Camera camera, Rect boundingBox) {
		float targetAspect = boundingBox.width/boundingBox.height;
		float scaleHeight = camera.aspect / targetAspect;
		if (scaleHeight < 1)
			return Mathf.Abs(boundingBox.width) / camera.aspect / 2f;
		else
			return Mathf.Abs(boundingBox.height) / 2f;
	}

	/// <summary>
	/// Focuses on bounds
	/// </summary>
	/// <param name="camera">Cmera.</param>
	/// <param name="go">Go.</param>
	public static void FocusOnBounds(this Camera camera, Bounds bounds) {
		// Get the radius of a sphere circumscribing the bounds
		float radius = bounds.size.magnitude / 2f;
		// Use the smaller FOV as it limits what would get cut off by the frustum        
		float fov = Mathf.Min(camera.fieldOfView, camera.GetHorizontalFieldOfView());
		float dist = radius / (Mathf.Sin(fov * 0.5f * Mathf.Deg2Rad));
		
		camera.transform.SetLocalPositionZ(dist);
		if (camera.orthographic)
			camera.orthographicSize = radius;
		
		// Frame the object hierarchy
		camera.transform.LookAt(bounds.center);
	}

	/// <summary>
	/// Gets the horizontal field of view of a camera.
	/// </summary>
	/// <returns>The horizontal field of view.</returns>
	/// <param name="camera">Camera.</param>
	public static float GetHorizontalFieldOfView (this Camera camera) {
		return GetHorizontalFieldOfView(camera.fieldOfView, camera.aspect);
	}

	/// <summary>
	/// Gets the frustrum height at a distance.
	/// </summary>
	/// <returns>The frustrum height at a distance.</returns>
	/// <param name="camera">Camera.</param>
	/// <param name="distance">Distance.</param>
	public static float GetFrustrumHeightAtDistance (this Camera camera, float distance) {
		return GetFrustrumHeightAtDistance(distance, camera.fieldOfView);
	}

	/// <summary>
	/// Gets the frustrum width at a distance.
	/// </summary>
	/// <returns>The frustrum width at a distance.</returns>
	/// <param name="camera">Camera.</param>
	/// <param name="distance">Distance.</param>
	public static float GetFrustrumWidthAtDistance (this Camera camera, float distance) {
		return GetFrustrumWidthAtDistance(distance, camera.fieldOfView, camera.aspect);
	}

	/// <summary>
	/// Gets the distance from the camera where frustrum height equals the input value.
	/// </summary>
	/// <returns>The distance at frustrum height.</returns>
	/// <param name="camera">Camera.</param>
	/// <param name="frustumHeight">Frustum height.</param>
	public static float GetDistanceAtFrustrumHeight (this Camera camera, float frustumHeight) {
		return GetDistanceAtFrustrumHeight(frustumHeight, camera.fieldOfView);
	}

	/// <summary>
	/// Gets the distance from the camera where frustrum width equals the input value.
	/// </summary>
	/// <returns>The distance at frustrum width.</returns>
	/// <param name="camera">Camera.</param>
	/// <param name="frustumWidth">Frustum width.</param>
	public static float GetDistanceAtFrustrumWidth (this Camera camera, float frustumWidth) {
		return GetDistanceAtFrustrumWidth(frustumWidth, camera.fieldOfView, camera.aspect);
	}

	/// <summary>
	/// Calculates the FOV angle at a specified width and distance.
	/// </summary>
	/// <returns>The FOV angle at width and distance.</returns>
	/// <param name="camera">Camera.</param>
	/// <param name="frustumWidth">Frustum width.</param>
	/// <param name="distance">Distance.</param>
	public static float GetFOVAngleAtWidthAndDistance (this Camera camera, float frustumWidth, float distance) {
		return GetFOVAngleAtWidthAndDistance(frustumWidth, distance, camera.aspect);
	}

	/// <summary>
	/// Get the frustrum height from a given width using the camera aspect ratio.
	/// </summary>
	/// <returns>The frustum width to frustum height.</returns>
	/// <param name="camera">Camera.</param>
	/// <param name="frustumWidth">Frustum width.</param>
	public static float ConvertFrustumWidthToFrustumHeight (this Camera camera, float frustumWidth) {
		return ConvertFrustumWidthToFrustumHeight(frustumWidth, camera.aspect);
	}

	/// <summary>
	/// Get the frustrum width from a given height using the camera aspect ratio.
	/// </summary>
	/// <returns>The frustum height to frustum width.</returns>
	/// <param name="camera">Camera.</param>
	/// <param name="frustumHeight">Frustum height.</param>
	public static float ConvertFrustumHeightToFrustumWidth (this Camera camera, float frustumHeight) {
		return ConvertFrustumHeightToFrustumWidth(frustumHeight, camera.aspect);
	}
	
	
	#endregion
	
	
	
	
	#region Camera Helper Functions
	
	/// <summary>
	/// Gets the horizontal field of view of a camera.
	/// </summary>
	/// <returns>The horizontal field of view.</returns>
	/// <param name="camera">Camera.</param>
	public static float GetHorizontalFieldOfView (float verticalFieldOfView, float aspectRatio) {
		return 2f * Mathf.Atan(Mathf.Tan(verticalFieldOfView * Mathf.Deg2Rad * 0.5f) * aspectRatio) * Mathf.Rad2Deg;
	}


	/// <summary>
	/// Gets the frustrum height at a distance.
	/// </summary>
	/// <returns>The frustrum height at a distance.</returns>
	/// <param name="camera">Camera.</param>
	/// <param name="distance">Distance.</param>
	public static float GetFrustrumHeightAtDistance (float distance, float fieldOfView) {
		return 2f * distance * Mathf.Tan(fieldOfView * 0.5f * Mathf.Deg2Rad);
	}

	/// <summary>
	/// Gets the frustrum width at a distance.
	/// </summary>
	/// <returns>The frustrum width at a distance.</returns>
	/// <param name="camera">Camera.</param>
	/// <param name="distance">Distance.</param>
	public static float GetFrustrumWidthAtDistance (float distance, float fieldOfView, float aspectRatio) {
		return ConvertFrustumHeightToFrustumWidth(GetFrustrumHeightAtDistance(distance, fieldOfView), aspectRatio);
	}

	
	public static float GetDistanceAtFrustrumHeight (float frustumHeight, float fieldOfView) {
		return frustumHeight * 0.5f / Mathf.Tan(fieldOfView * 0.5f * Mathf.Deg2Rad);
	}
	
	public static float GetDistanceAtFrustrumWidth (float frustumWidth, float fieldOfView, float aspectRatio) {
		return GetDistanceAtFrustrumHeight(ConvertFrustumWidthToFrustumHeight(frustumWidth, aspectRatio), fieldOfView);
	}
	
	
	public static float GetFOVAngleAtHeightAndDistance (float frustumHeight, float distance) {
		return 2f * Mathf.Atan(frustumHeight * 0.5f / distance) * Mathf.Rad2Deg;
	}
	
	public static float GetFOVAngleAtWidthAndDistance (float frustumWidth, float distance, float aspectRatio) {
		return GetFOVAngleAtHeightAndDistance(ConvertFrustumWidthToFrustumHeight(frustumWidth, aspectRatio), distance);
	}
	
	
	public static float ConvertFrustumWidthToFrustumHeight (float frustumWidth, float aspectRatio) {
		return frustumWidth / aspectRatio;
	}
	
	public static float ConvertFrustumHeightToFrustumWidth (float frustumHeight, float aspectRatio) {
		return frustumHeight * aspectRatio;
	}
	
	#endregion
}
