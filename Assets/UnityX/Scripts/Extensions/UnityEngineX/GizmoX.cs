using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GizmosX {
	
	
	public static void DrawWirePolygon (Vector3 position, Quaternion rotation, IList<Vector2> points) {
		for(int i = 0; i < points.Count; i++) {
			Gizmos.DrawLine(position + rotation * points.GetRepeating(i), position + rotation * points.GetRepeating(i+1));
		}
	}
	
	public static void DrawWirePolygon (IList<Vector2> points) {
		for(int i = 0; i < points.Count; i++) {
			Gizmos.DrawLine(points.GetRepeating(i), points.GetRepeating(i+1));
		}
	}
	
	public static void DrawWirePolygon (IList<Vector3> points) {
		for(int i = 0; i < points.Count; i++) {
			Gizmos.DrawLine(points.GetRepeating(i), points.GetRepeating(i+1));
		}
	}
	
	public static void DrawWireCircle (Vector3 position, Quaternion rotation, float radius, int numLines = 32) {
		for(int i = 0; i < numLines; i++) {
			Vector3 offsetVectorA = MathX.DegreesToVector2(MathX.DegreesFromRange(i, numLines)) * radius;
			Vector3 offsetVectorB = MathX.DegreesToVector2(MathX.DegreesFromRange(i+1, numLines)) * radius;
			Vector3 a = position + rotation * offsetVectorA;
			Vector3 b = position + rotation * offsetVectorB;
			Gizmos.DrawLine(a, b);
		}
	}
	
	public static void DrawWireCube (Bounds _bounds) {
		Gizmos.DrawWireCube(_bounds.center, _bounds.size);
	}

	public static void DrawWireCube (Bounds _bounds, Quaternion rotation) {
		DrawWireCube(_bounds.center, rotation, _bounds.size);
	}
	
	
	public static void DrawCube (Bounds _bounds) {
		Gizmos.DrawCube(_bounds.center, _bounds.size);
	}

	public static void DrawCube (Bounds _bounds, Quaternion rotation) {
		DrawCube(_bounds.center, rotation, _bounds.size);
	}

	public static void DrawWireCube (Vector3 position, Quaternion rotation, Vector3 scale) {
		Matrix4x4 temp = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(position, rotation, scale);
		Gizmos.DrawWireCube(new Vector3(0,0,0), Vector3.one);
		Gizmos.matrix = temp;
	}

	public static void DrawCube (Vector3 position, Quaternion rotation, Vector3 scale) {
		Matrix4x4 temp = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(position, rotation, scale);
		Gizmos.DrawCube(new Vector3(0,0,0), Vector3.one);
		Gizmos.matrix = temp;
	}
	
	public static void DrawWireCube (Rect _rect, float _distance) {
		Gizmos.DrawWireCube(new Vector3(_rect.x + _rect.width * 0.5f, _rect.y + _rect.height * 0.5f, _distance), new Vector3(_rect.width, _rect.height, 0));
	}
	
	public static void DrawCube (Rect _rect, float _distance) {
		Gizmos.DrawCube(new Vector3(_rect.x + _rect.width * 0.5f, _rect.y + _rect.height * 0.5f, _distance), new Vector3(_rect.width, _rect.height, 0));
	}
	

	public static void DrawWireRect (Vector3 topLeft, Vector3 topRight, Vector3 bottomLeft, Vector3 bottomRight) {
		DrawWirePolygon(new Vector3[]{topLeft, topRight, bottomRight, bottomLeft, topLeft});
	}

	public static void DrawWireRect (Rect _rect) {
		DrawWireRect(_rect.TopLeft(), _rect.TopRight(), _rect.BottomLeft(), _rect.BottomRight());
	}

	public static void DrawWireRect (Rect _rect, Quaternion rotation) {
		DrawWireRect(_rect.center, rotation, _rect.size);
	}
	
	public static void DrawWireRect (Vector3 origin, Quaternion rotation, Vector2 scale) {
		scale *= 0.5f;
		Vector3 topLeft = origin + rotation * new Vector3(-scale.x, scale.y, 0);
		Vector3 topRight = origin + rotation * new Vector3(scale.x, scale.y, 0);
		Vector3 bottomLeft = origin + rotation * new Vector3(-scale.x, -scale.y, 0);
		Vector3 bottomRight = origin + rotation * new Vector3(scale.x, -scale.y, 0);
		DrawWireRect(topLeft, topRight, bottomLeft, bottomRight);
	}
	
	public static void DrawArrow (Vector3 position, Quaternion rotation, float arrowSize) {
		Vector3 start = position + (rotation * Vector3.back * arrowSize);
		Vector3 end = position + (rotation * Vector3.forward * arrowSize);
		Gizmos.DrawLine(start+(rotation * Vector3.left * arrowSize), end);
		Gizmos.DrawLine(start+(rotation * Vector3.right * arrowSize), end);
	}


	public static void DrawFrustum (Vector3 position, Quaternion rotation, float fieldOfView, float aspectRatio, float nearClipPlane, float farClipPlane) {
		if(!QuaternionX.IsValid(rotation)) return;
		Matrix4x4 temp = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(position, rotation, Vector3.one);
		Gizmos.DrawFrustum(Vector3.zero, fieldOfView, farClipPlane, nearClipPlane, aspectRatio);
		Gizmos.matrix = temp;
	}
	
	public static void DrawOrthographicFrustum (Vector3 position, Quaternion rotation, float orthographicSize, float aspectRatio, float nearClipPlane, float farClipPlane) {
		Matrix4x4 temp = Gizmos.matrix;
		Gizmos.matrix = Matrix4x4.TRS(position, rotation, Vector3.one);
		float spread = farClipPlane - nearClipPlane;
		float center = (farClipPlane + nearClipPlane)*0.5f;
		Gizmos.DrawWireCube(new Vector3(0,0,center), new Vector3(orthographicSize*2*aspectRatio, orthographicSize*2, spread));
		Gizmos.matrix = temp;
	}
}
