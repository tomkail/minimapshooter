﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class QuaternionX {

	public static Quaternion Difference (this Quaternion rotationA, Quaternion rotationB) {
		return Quaternion.Inverse(rotationB) * rotationA;
	}
	
	public static Quaternion Rotate(this Quaternion q1, Quaternion q2, Space relativeTo = Space.Self) {
		if(relativeTo == Space.World) {
			return q2 * q1;
		} else {
			return q1 * q2;
		}
	}
	
	public static Quaternion Rotate(this Quaternion q1, Vector3 eulerAngles, Space relativeTo = Space.Self) {
		return q1.Rotate(Quaternion.Euler(eulerAngles), relativeTo);
	}

	public static bool IsValid(this Quaternion q) {
		return !(q.x == 0 && q.y == 0 && q.z == 0 && q.w == 0);
	}
	
	public static bool IsNaN(this Quaternion q) {
		return float.IsNaN(q.x) || float.IsNaN(q.y) || float.IsNaN(q.z) || float.IsNaN(q.w);
	}
	
	public static Vector4 ToVector4(this Quaternion rot) {
		return new Vector4(rot.x, rot.y, rot.z, rot.w);
	}
}