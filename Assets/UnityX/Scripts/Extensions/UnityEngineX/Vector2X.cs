﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Vector2X {
	#region Static Members
	/// <summary>
	/// Returns the half.
	/// </summary>
	/// <value>The half.</value>
	public static Vector2 half {
		get {
			return new Vector2(0.5f, 0.5f);
		}
	}
	#endregion


	#region Logging functions
	/// <summary>
	/// Produces a string from a Vector, with components rounded to a specified number of decimal places.
	/// </summary>
	/// <returns>The string.</returns>
	/// <param name="v">The vector.</param>
	/// <param name="numDecimalPlaces">Number decimal places.</param>
	public static string ToString(this Vector2 v, int numDecimalPlaces = 3){
		return ("("+v.x.RoundTo(numDecimalPlaces).ToString()+", "+v.y.RoundTo(numDecimalPlaces).ToString()+")");
	}
	#endregion

	#region Manipulation functions
	/// <summary>
	/// Divides the first vector by the second component-wise.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector2 Divide(Vector2 left, Vector2 right){
		return new Vector2(left.x / right.x, left.y / right.y);
	}
	#endregion

	/// <summary>
	/// Returns direction from a to b.
	/// </summary>
	/// <param name="a">The first component.</param>
	/// <param name="b">The second component.</param>
	public static Vector2 FromTo(Vector2 a, Vector2 b){
		return b - a;
	}

	/// <summary>
	/// Changes the direction of the vector without affecting magnitude. Magnitude of direction vector is irrelevant.
	/// </summary>
	/// <returns>The rotated vector.</returns>
	/// <param name="vector">Vector.</param>
	/// <param name="newDirection">New direction.</param>
	public static Vector3 InDirection(Vector3 vector, Vector3 newDirection){
		return newDirection.normalized * vector.magnitude;
	}

	/// <summary>
	/// Returns normalized direction from a to b.
	/// </summary>
	/// <param name="a">The first component.</param>
	/// <param name="b">The second component.</param>
	public static Vector2 NormalizedDirection(Vector2 a, Vector2 b){
		return FromTo(a, b).normalized;
	}


	/// <summary>
	/// Takes the reciprocal of each component in the vector.
	/// </summary>
	/// <param name="value">The vector to take the reciprocal of.</param>
	/// <returns>A vector that is the reciprocal of the input vector.</returns>
	public static Vector2 Reciprocal(this Vector2 v) {
		return new Vector2(1f / v.x, 1f / v.y);
	}
	
	/// <summary>
	/// Projects a vector onto another vector.
	/// </summary>
	/// <param name="vector">vector.</param>
	/// <param name="direction">direction.</param>
	public static Vector2 Project(Vector2 vector, Vector2 direction) {
		Vector2 directionNormalized = direction.normalized;	
		float projectedDist = Vector2.Dot(vector, directionNormalized);
		return directionNormalized * projectedDist;
	}

	public static float SqrDistance (Vector2 a, Vector2 b) {
		return (a-b).sqrMagnitude;
	}

	public static Vector2 Rotate (Vector2 v, float angle) {
		angle *= Mathf.Deg2Rad;
		float sin = Mathf.Sin( angle );
		float cos = Mathf.Cos( angle );
		float tx = v.x;
		float ty = v.y;
		v.x = (cos * tx) - (sin * ty);
		v.y = (cos * ty) + (sin * tx);
		return v;
	}

	// Gets angle between a and b. Use Mathf.Rad2Deg to get degrees. 
	// 90 Degrees is added to ensure that up is 0 degrees
	public static float RadiansBetween(Vector2 a, Vector2 b) {
		//return Mathf.Atan2(-(b.y - a.y), b.x - a.x) + (Mathf.Deg2Rad * 90);
		return Radians(FromTo(b, a));
	}

	// Gets angle between 0,0 and a. Use Mathf.Rad2Deg to get degrees
	public static float Radians(Vector2 a) {
		//return RadiansBetween(Vector2.zero, a);

		return Mathf.Atan2(-a.y, a.x) + (Mathf.Deg2Rad * 90);
	}
	
	// Gets angle between a and b.
	// Note this isn't the angle difference between the angles of a and b.
	public static float DegreesBetween(Vector2 a, Vector2 b) {
		return RadiansBetween(a,b) * Mathf.Rad2Deg;
	}
	
	// Gets angle between 0,0 and a.
	public static float Degrees(Vector2 a) {
		return Radians(a) * Mathf.Rad2Deg;
	}
	

	public static Vector2 ClampMagnitudeInDirection (Vector2 vector, Vector2 direction, float clampValue, bool outwardsOnly = false) {
		
		direction = direction.normalized;
		float actualComponentInDirection = Vector2.Dot (vector, direction);
		float desiredComponentInDirection = Mathf.Clamp (actualComponentInDirection, -clampValue, clampValue);
		float requiredOffsetInDirection = desiredComponentInDirection - actualComponentInDirection;
		
		return vector + requiredOffsetInDirection * direction;
		
		// Hey Ben! This was what I was using before. Seems to have the same result as above.
		//		Vector2 projectedVector = Vector2X.Project(vector, direction);
		//		Vector2 velocityDiff = projectedVector - vector;
		//		Vector2 clampedProjectedVelocity = Vector2.ClampMagnitude(projectedVector, clampValue);
		//		Vector2 deltaProjectedVector = clampedProjectedVelocity - projectedVector;
		//		vector += deltaProjectedVector;
		//		return vector;
	}

	/// <summary>
	/// Returns the index of the closest vector to v in the values list
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static int ClosestIndex(Vector2 v, IList<Vector2> values){
		int index = 0;
		float closest = Vector2X.SqrDistance(v, values[index]);
		for(int i = 1; i < values.Count; i++){
			float distance = Vector2X.SqrDistance(v, values[i]);
			if (distance < closest) {
				closest = distance;
				index = i;
			}
		}
		return index;
	}
	
	/// <summary>
	/// Returns the distance between the closest vector to v in the values list and v
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static float ClosestDistance(Vector2 v, IList<Vector2> values){
		return Vector2.Distance(v, Closest(v, values));
	}
	
	/// <summary>
	/// Returns the closest vector to v in the values list
	/// </summary>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static Vector2 Closest(Vector2 v, IList<Vector2> values){
		return values[ClosestIndex(v, values)];
	}
	/// <summary>
	/// Sets the value of the X component.
	/// </summary> 
	public static Vector2 WithX (this Vector2 v, float newX) {
		return new Vector2(newX, v.y);
	}
	
	/// <summary>
	/// Sets the value of the Y component
	/// </summary> 
	public static Vector2 WithY (this Vector2 v, float newY) {
		return new Vector2(v.x, newY);
	}
	
	/// <summary>
	/// Adds a value to the X component
	/// </summary> 
	public static Vector2 AddX (this Vector2 v, float addX) {
		return new Vector2(v.x + addX, v.y);
	}
	
	/// <summary>
	/// Adds a value to the Y component
	/// </summary> 
	public static Vector2 AddY (this Vector2 v, float addY) {
		return new Vector2(v.x, v.y + addY);
	}

	public static Vector3 ToVector3XZY (this Vector2 v) {
		return new Vector3(v.x, 0, v.y);
	}
}