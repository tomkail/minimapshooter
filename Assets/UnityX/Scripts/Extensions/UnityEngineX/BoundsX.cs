﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class BoundsX {
	
	/// <summary>
	/// Creates new bounds that encapsulates a list of vectors.
	/// </summary>
	/// <param name="vectors">Vectors.</param>
	public static Bounds CreateEncapsulating (IList<Vector3> vectors) {
		Bounds bounds = new Bounds(vectors[0], Vector3.zero);
		for(int i = 1; i < vectors.Count; i++)
			bounds.Encapsulate(vectors[i]);
		return bounds;
	}
}
