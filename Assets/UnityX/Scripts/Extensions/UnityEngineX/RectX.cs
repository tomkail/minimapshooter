﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// TODO - change new Rect() to rect.Set()!
public static class RectX {

	/// <summary>
	/// Creates new rect that encapsulates a list of vectors.
	/// </summary>
	/// <param name="vectors">Vectors.</param>
	public static Rect CreateEncapsulating (params Vector2[] vectors) {
		Rect rect = new Rect(vectors[0].x, vectors[0].y, 0, 0);
		for(int i = 1; i < vectors.Length; i++)
			rect = rect.Encapsulate(vectors[i]);
		return rect;
	}
	
	
	public static Rect CreateEncapsulating (params Rect[] rects) {
		Rect rect = new Rect(rects[0]);
		for(int i = 1; i < rects.Length; i++)
			rect = rect.Encapsulate(rects[i]);
		return rect;
	}
	
	public static Rect CreateFromCenter (Vector2 centerPosition, Vector2 size) {
		return CreateFromCenter(centerPosition.x, centerPosition.y, size.x, size.y);
	}
	
	public static Rect CreateFromCenter (float centerX, float centerY, float sizeX, float sizeY) {
		return new Rect(centerX - sizeX * 0.5f, centerY - sizeY * 0.5f, sizeX, sizeY);
	}

	public static Rect Create (Vector2 position, Vector2 size, Vector2 pivot) {
		return new Rect(position.x - size.x * pivot.x, position.y - size.y * pivot.y, size.x, size.y);
	}

	public static Rect Lerp (Rect rect1, Rect rect2, float lerp) {
		Vector4 newRect = Vector4.Lerp(new Vector4(rect1.x, rect1.y, rect1.width, rect1.height), new Vector4(rect2.x, rect2.y, rect2.width, rect2.height), lerp);
		return new Rect(newRect.x, newRect.y, newRect.z, newRect.w);
	}

	public static Rect Add(this Rect left, Rect right){
		return new Rect(left.x+right.x, left.y+right.y, left.width+right.width, left.height+right.height);
	}
	
	public static Rect Subtract(this Rect left, Rect right){
		return new Rect(left.x-right.x, left.y-right.y, left.width-right.width, left.height-right.height);
	}

	public static Rect CopyWithX(this Rect r, float x){
		return new Rect(x, r.y, r.width, r.height);
	}

	public static Rect CopyWithY(this Rect r, float y){
		return new Rect(r.x, y, r.width, r.height);
	}

	public static Rect CopyWithWidth(this Rect r, float width){
		return new Rect(r.x, r.y, width, r.height);
	}

	public static Rect CopyWithHeight(this Rect r, float height){
		return new Rect(r.x, r.y, r.width, height);
	}

	public static Rect CopyWithPosition(this Rect r, Vector2 position){
		return new Rect(position.x, position.y, r.width, r.height);
	}

	public static Rect CopyWithSize(this Rect r, Vector2 size){
		return new Rect(r.x, r.y, size.x, size.y);
	}


	/// <summary>
	/// Returns a version of the rect with position clamped between minPosition and maxPosition
	/// </summary>
	/// <returns>The position.</returns>
	/// <param name="r">The red component.</param>
	/// <param name="minSize">Minimum position.</param>
	/// <param name="maxSize">Max position.</param>
	public static Rect ClampPosition(this Rect r, Vector2 minPosition, Vector2 maxPosition) {
		r.x = Mathf.Clamp(r.x, minPosition.x, maxPosition.x);
		r.y = Mathf.Clamp(r.y, minPosition.y, maxPosition.y);
		return r;
	}

	/// <summary>
	/// Returns a version of the rect with position clamped between minSize and maxSize
	/// </summary>
	/// <returns>The position.</returns>
	/// <param name="r">The red component.</param>
	/// <param name="minSize">Minimum size.</param>
	/// <param name="maxSize">Max size.</param>
	public static Rect ClampSize(this Rect r, Vector2 minSize, Vector2 maxSize) {
		r.width = Mathf.Clamp(r.width, minSize.x, maxSize.x);
		r.height = Mathf.Clamp(r.height, minSize.y, maxSize.y);
		return r;
	}

	
	/// <summary>
	/// Expands (or contracts) the rect by a specified amount.
	/// </summary>
	/// <param name="r">The red component.</param>
	/// <param name="expansion">Expansion.</param>
	public static Rect Expand(this Rect r, Vector2 expansion){
		return Expand (r, expansion, Vector2X.half);
	}
	
	/// <summary>
	/// Expands (or contracts) the rect by a specified amount, using a pivot to control the expansion center point.
	/// </summary>
	/// <param name="r">The red component.</param>
	/// <param name="expansion">Expansion.</param>
	/// <param name="pivot">Pivot.</param>
	public static Rect Expand(this Rect r, Vector2 expansion, Vector2 pivot){
		return new Rect(r.x - (expansion.x * 0.5f * (pivot.x + 0.5f)), r.y - (expansion.y * 0.5f * (pivot.y + 0.5f)), r.width + (expansion.x * (pivot.x + 0.5f)), r.height + (expansion.y * (pivot.y + 0.5f)));
	}
	
	public static Rect Encapsulate(this Rect r, Rect rect) {
		r = r.Encapsulate(rect.min);
		r = r.Encapsulate(rect.max);
		return r;
	}
	
	public static Rect Encapsulate(this Rect r, Vector2 point){
		Vector2 min = Vector2.Min (r.min, point);
		Vector2 max = Vector2.Max (r.max, point);
		return Rect.MinMaxRect (min.x, min.y, max.x, max.y);
	}
	
	public static Vector2[] Corners (this Rect r) {
		return new Vector2[4] {r.TopLeft(), r.TopRight(), r.BottomRight(), r.BottomLeft()};
	}
	
	public static Vector2 TopLeft(this Rect r){
		return r.min;
	}
	
	public static Vector2 TopRight(this Rect r){
		return new Vector2(r.x+r.width, r.y);
	}
	
	public static Vector2 BottomLeft(this Rect r){
		return new Vector2(r.x, r.y+r.height);
	}
	
	public static Vector2 BottomRight(this Rect r){
		return r.max;
	}

	/// <summary>
	/// Clamps a point inside the rect.
	/// </summary>
	/// <param name="r">The red component.</param>
	/// <param name="point">Point.</param>
	public static Vector2 ClampPoint(Rect rect, Vector2 point) {
		point.x = Mathf.Clamp(point.x, rect.min.x, rect.max.x);
		point.y = Mathf.Clamp(point.y, rect.min.y, rect.max.y);
		return point;
	}

	/// <summary>
	/// Returns the point as a normalized position inside the rect, where 0,0 is the top left and 1,1 is the top right
	/// </summary>
	/// <returns>The normalized position inside rect.</returns>
	/// <param name="r">The red component.</param>
	/// <param name="point">Point.</param>
	public static Vector2 GetNormalizedPositionInsideRect (this Rect r, Vector2 point) {
		return new Vector2((point.x - r.x) / r.width, (point.y - r.y) / r.height);
	}

	public static Rect ClampInside(Rect r, Rect container) {
		Rect ret = new Rect();
		ret.x = MathX.Clamp0Infinity(Mathf.Max(r.x, container.x));
		ret.y = MathX.Clamp0Infinity(Mathf.Max(r.y, container.y));
		ret.width = MathX.Clamp0Infinity(Mathf.Min(r.x + r.width, container.x + container.width) - ret.x);
		ret.height = MathX.Clamp0Infinity(Mathf.Min(r.y + r.height, container.y + container.height) - ret.y);
		
		float offsetX = MathX.Clamp0Infinity(r.x - Mathf.Max(ret.width, container.width));
		float offsetY = MathX.Clamp0Infinity(r.y - Mathf.Max(ret.height, container.height));
		ret.x -= offsetX;
		ret.y -= offsetY;
		return ret;
	}
	
	public static Rect ClampInsideKeepSize(Rect r, Rect container) {
		Rect ret = new Rect();
		ret.x = MathX.Clamp0Infinity(Mathf.Max(r.x, container.x));
		ret.y = MathX.Clamp0Infinity(Mathf.Max(r.y, container.y));
		ret.width = Mathf.Min(r.width, container.width);
		ret.height = Mathf.Min(r.height, container.height);
		
		float offsetX = MathX.Clamp0Infinity((r.x + r.width) - (container.x + container.width));
		float offsetY = MathX.Clamp0Infinity((r.y + r.height) - (container.y + container.height));
		ret.x -= Mathf.Min(offsetX, ret.x);
		ret.y -= Mathf.Min(offsetY, ret.y);
		
		return ret;
	}

	public static bool ContainsAny (this Rect r, params Vector2[] positions) {
		foreach(Vector2 position in positions) {
			if(r.Contains(position)) {
				return true;
			}
		}
		return false;
	}
	/// <summary>
	/// Gets the vertices.
	/// Returns moving clockwise around from the center, starting with the top left.
	/// </summary>
	/// <returns>The vertices.</returns>
	/// <param name="rect">Rect.</param>
	public static Vector2[] GetVertices(this Rect rect) {
		Vector2[] vertices = new Vector2[4];
		Vector2 max = rect.max;
		vertices[0] = rect.min;
		vertices[1] = new Vector2(max.x, vertices[0].y);
		vertices[2] = max;
		vertices[3] = new Vector2(vertices[0].x, max.y);
		return vertices;
	}
}
