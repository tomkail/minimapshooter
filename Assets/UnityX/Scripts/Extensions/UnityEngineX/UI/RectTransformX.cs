﻿using UnityEngine;

public static class RectTransformX {
	/// <summary>
	/// Find the size of the parent required to fit the child at a new size, given current anchoring.
	/// When anchors are together, parent is the same as it currently is.
	/// When anchors are at the parent corners, parent needs to grow 1:1 with child.
	/// </summary>
	/// <returns>The required size of the parent on the given axis.</returns>
	/// <param name="size">The desired size of the target RectTransform.</param>
	/// <param name="axis">The axis for the size calculation.</param>
	public static float SizeOfParentToFitSize(this RectTransform thisRect, float size, RectTransform.Axis axis) {
		
		int axisIndex = (int)axis;
		float currentSize = thisRect.rect.size[axisIndex];

		float anchorSeparation = thisRect.anchorMax[axisIndex] - thisRect.anchorMin[axisIndex];

		RectTransform parent = thisRect.parent.transform as RectTransform;
		float parentSize = parent.rect.size[axisIndex];

		float toParent = parentSize - currentSize;
		float newParent = size + anchorSeparation * toParent;
		
		return newParent;
	}
	
	
	/// <summary>
	/// Returns the anchor of a RectTransform as a rect.
	/// </summary>
	/// <param name="rectTransform">Rect transform.</param>
	public static Rect Anchor(this RectTransform rectTransform) {
		return new Rect(rectTransform.anchorMin.x, rectTransform.anchorMin.y, rectTransform.anchorMax.x, rectTransform.anchorMax.y);
	}
	
	/// <summary>
	/// Gets the anchor of the specified rectTransform at a Vector. Eg (0,0) returns anchorMin, (1,1) returns anchorMax.
 	/// </summary>
	/// <param name="rectTransform">Rect transform.</param>
	/// <param name="rectPosition">Rect position.</param>
	public static Vector2 AnchorPosition(this RectTransform rectTransform, Vector2 normalizedRectCoordinates) {
		return Rect.NormalizedToPoint(Anchor(rectTransform), normalizedRectCoordinates);
	}
	
	/// <summary>
	/// The center of a RectTransform's anchors. 
	/// Shorthand for rectTransform.AnchorPosition(new Vector2(0.5f, 0.5f)).
	/// </summary>
	/// <returns>The center.</returns>
	/// <param name="rectTransform">Rect transform.</param>
	public static Vector2 AnchorCenter(this RectTransform rectTransform) {
		return rectTransform.AnchorPosition(new Vector2(0.5f, 0.5f));
	}
	
	public static Rect GetScreenRect(this RectTransform rectTransform, Canvas canvas) {
		Vector3[] corners = new Vector3[4];
		
		rectTransform.GetWorldCorners(corners);
		
		float xMin = float.PositiveInfinity;
		float xMax = float.NegativeInfinity;
		float yMin = float.PositiveInfinity;
		float yMax = float.NegativeInfinity;
		
		for (int i = 0; i < 4; i++)
		{
			// For Canvas mode Screen Space - Overlay there is no Camera; best solution I've found
			// is to use RectTransformUtility.WorldToScreenPoint) with a null camera.
			Camera cam = null;
			if(canvas.renderMode == RenderMode.ScreenSpaceCamera || canvas.renderMode == RenderMode.WorldSpace)
				cam = canvas.worldCamera;
			Vector3 screenCoord = RectTransformUtility.WorldToScreenPoint(cam, corners[i]);
			
			if (screenCoord.x < xMin)
				xMin = screenCoord.x;
			if (screenCoord.x > xMax)
				xMax = screenCoord.x;
			if (screenCoord.y < yMin)
				yMin = screenCoord.y;
			if (screenCoord.y > yMax)
				yMax = screenCoord.y;
		}
		
		Rect result = new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
		
		return result;
	}
	
	
	public static void SetDefaultScale(this RectTransform trans) {
		trans.localScale = new Vector3(1, 1, 1);
	}
	public static void SetPivotAndAnchors(this RectTransform trans, Vector2 aVec) {
		trans.pivot = aVec;
		trans.anchorMin = aVec;
		trans.anchorMax = aVec;
	}
	
	public static Vector2 GetSize(this RectTransform trans) {
		return trans.rect.size;
	}
	public static float GetWidth(this RectTransform trans) {
		return trans.rect.width;
	}
	public static float GetHeight(this RectTransform trans) {
		return trans.rect.height;
	}
	
	public static void SetPositionOfPivot(this RectTransform trans, Vector2 newPos) {
		trans.localPosition = new Vector3(newPos.x, newPos.y, trans.localPosition.z);
	}
	
	public static void SetLeftBottomPosition(this RectTransform trans, Vector2 newPos) {
		trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
	}
	public static void SetLeftTopPosition(this RectTransform trans, Vector2 newPos) {
		trans.localPosition = new Vector3(newPos.x + (trans.pivot.x * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
	}
	public static void SetRightBottomPosition(this RectTransform trans, Vector2 newPos) {
		trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y + (trans.pivot.y * trans.rect.height), trans.localPosition.z);
	}
	public static void SetRightTopPosition(this RectTransform trans, Vector2 newPos) {
		trans.localPosition = new Vector3(newPos.x - ((1f - trans.pivot.x) * trans.rect.width), newPos.y - ((1f - trans.pivot.y) * trans.rect.height), trans.localPosition.z);
	}
	
	public static void SetSize(this RectTransform trans, Vector2 newSize) {
		Vector2 oldSize = trans.rect.size;
		Vector2 deltaSize = newSize - oldSize;
		trans.offsetMin = trans.offsetMin - new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
		trans.offsetMax = trans.offsetMax + new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
	}
	
	public static void SetWidth(this RectTransform trans, float newSize) {
		SetSize(trans, new Vector2(newSize, trans.rect.size.y));
	}
	public static void SetHeight(this RectTransform trans, float newSize) {
		SetSize(trans, new Vector2(trans.rect.size.x, newSize));
	}
	
	public static void SetRect(this RectTransform rectTransform, Rect rect) {
		rectTransform.position = rect.position + Vector2.Scale(rect.size, rectTransform.pivot);
		rectTransform.SetSize(rect.size);
	}
}