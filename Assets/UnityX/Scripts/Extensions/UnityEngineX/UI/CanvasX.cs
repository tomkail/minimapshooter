﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public static class CanvasX {

	public static RectTransform GetRectTransform (this Canvas canvas) {
		return canvas.GetComponent<RectTransform>();
	}

	private static void GetCameraFromCanvas (Canvas canvas, ref Camera camera) {
		if(canvas.renderMode == RenderMode.ScreenSpaceOverlay) {
			camera = null;
		} else if(canvas.renderMode == RenderMode.ScreenSpaceCamera) {
			camera = canvas.worldCamera;
		} else if(canvas.renderMode == RenderMode.WorldSpace && camera == null) {
			if(canvas.worldCamera != null)
				camera = canvas.worldCamera;
			else
				Debug.LogError("Canvas is in world space, but camera is null and no event camera exists on canvas.");
		}
	}

	// "Canvas Space" is space local to a child of a canvas. 0,0 is in the center.
	// Converts screen position to the canvas position, allowing for a specified camera to be chosen for world space canvases.
	public static Vector2 ScreenToCanvasPoint(this Canvas canvas, Vector2 screenPoint, Camera camera = null) {
		GetCameraFromCanvas(canvas, ref camera);
		RectTransform canvasRectTransform = canvas.GetComponent<RectTransform>();

		Vector2 canvasSpace = Vector3.zero;
		if(RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, screenPoint, camera, out canvasSpace))
			return canvasSpace;
		else return Vector2.zero;
	}

	public static Vector3 WorldToCanvasPoint(this Canvas canvas, Vector3 position, Camera camera = null) {
		GetCameraFromCanvas(canvas, ref camera);
		Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(camera, position);
//		Debug.Log(position);
		return ScreenToCanvasPoint(canvas, screenPoint, camera);
		/*
		RectTransform canvasRectTransform = canvas.GetComponent<RectTransform>();

		//Vector position (percentage from 0 to 1) considering camera size.
		//For example (0,0) is lower left, middle is (0.5,0.5)
		Vector2 temp = camera.WorldToViewportPoint(position);

		//Calculate position considering our percentage, using our canvas size
		//So if canvas size is (1100,500), and percentage is (0.5,0.5), current value will be (550,250)
		temp.x *= canvasRectTransform.sizeDelta.x;
		temp.y *= canvasRectTransform.sizeDelta.y;

		//The result is ready, but, this result is correct if canvas recttransform pivot is 0,0 - left lower corner.
		//But in reality its middle (0.5,0.5) by default, so we remove the amount considering cavnas rectransform pivot.
		//We could multiply with constant 0.5, but we will actually read the value, so if custom rect transform is passed(with custom pivot) , 
		//returned value will still be correct.

		temp.x -= canvasRectTransform.sizeDelta.x * canvasRectTransform.pivot.x;
		temp.y -= canvasRectTransform.sizeDelta.y * canvasRectTransform.pivot.y;
		return temp;
		*/
	}


	/// <summary>
	/// Converts a point in world space to a point in canvas space by converting from world space to screen space using a specified camera. 
	/// </summary>
	/// <returns>The point to local point in rectangle.</returns>
	/// <param name="canvas">Canvas.</param>
	/// <param name="camera">Camera.</param>
	/// <param name="worldPosition">World position.</param>
	public static Vector3? WorldPointToLocalPointInRectangle (this Canvas canvas, Camera camera, Vector3 worldPosition) {
		Vector3 screenPoint = camera.WorldToScreenPoint(worldPosition);

		// Behind the camera, definitely can't be within the rectangle
		if (screenPoint.z < 0)
			return null;
		
		Vector3? output = canvas.ScreenPointToLocalPointInRectangle(screenPoint);
		if(output == null)
			return null;
		else
			return (Vector3)output;
	}

	public static Vector3? ScreenPointToLocalPointInRectangle (this Canvas canvas, Vector2 screenPoint) {
		Camera camera = canvas.renderMode == RenderMode.ScreenSpaceCamera ? canvas.worldCamera : null;
		Vector2 localPosition;
		if(RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.GetComponent<RectTransform>(), screenPoint, camera, out localPosition))
			return localPosition;
		else return null;
	}
	
	
	public static Vector3? ScreenPointToCanvasSpace(this Canvas canvas, Vector2 screenPoint) {
		Camera camera = canvas.renderMode == RenderMode.ScreenSpaceCamera ? canvas.worldCamera : null;
		Vector3 canvasSpace = Vector3.zero;
		if(RectTransformUtility.ScreenPointToWorldPointInRectangle(canvas.GetComponent<RectTransform>(), screenPoint, camera, out canvasSpace))
			return canvasSpace;
		else return null;
	}
}
