﻿using UnityEngine;
using System.Collections;

public static class Vector4X {

	/// <summary>
	/// Sets the value of the W component.
	/// </summary> 
	public static Vector4 SetW (this Vector4 v, float newW) {
		return new Vector4(v.x, v.y, v.z, newW);
	}
	public static Quaternion ToQuaternion(this Vector4 vec) {
		return new Quaternion(vec.x, vec.y, vec.z, vec.w);
	}
}
