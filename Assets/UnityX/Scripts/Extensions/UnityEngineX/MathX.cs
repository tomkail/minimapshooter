using UnityEngine;
using System;
using System.Collections.Generic;

public static class MathX {


	/// <summary>
	/// Repeat the specified value around a min and max.
	/// Note that unlike Unity's Repeat function, this function returns max when val == max instead of min.
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static int RepeatInclusive (int val, int min, int max) {
		int range = max - min;
		if (val > max) 
			val -= range * Mathf.CeilToInt((val-max) / range);
		else if (val < min) 
			val += range * Mathf.CeilToInt((-val+min) / range);
		return val;
	}
	
	/// <summary>
	/// Repeat the specified value around a min and max.
	/// Note that unlike Unity's Repeat function, this function returns max when val == max instead of min.
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static float RepeatInclusive (float val, float min, float max) {
		float range = max - min;
		if (val > max) 
			val -= range * Mathf.CeilToInt((val-max) / range);
		else if (val < min) 
			val += range * Mathf.CeilToInt((-val+min) / range);
		return val;
	}
	
	
	/// <summary>
	/// Returns the reciprocal of a number.
	/// </summary>
	public static float Reciprocal (this float f) {
		return 1f/f;
	}
	
	/// <summary>
	/// Returns true is the float is a whole number
	/// </summary>
	public static bool IsWhole (this float f) {
		return f.IsMultipleOf(1);
	}

	/// <summary>
	/// Returns true is the float is positive (more than or equal to zero)
	/// </summary>
	public static bool IsPositive (this float f) {
		return (f >= 0);
	}
	
	/// <summary>
	/// Determines if the float is even.
	/// </summary>
	/// <returns><c>true</c> if the specified f is even; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	public static bool IsEven (this int f) {
		return f % 2 == 0;
	}
	
	/// <summary>
	/// Determines if the float is odd.
	/// </summary>
	/// <returns><c>true</c> if the specified float is odd; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	public static bool IsOdd (this int f) {
		return f % 2 == 1;
	}
	
	
	/// <summary>
	/// Determines if the float is a multiple of val.
	/// </summary>
	/// <returns><c>true</c> if is multiple of the specified f val; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	/// <param name="val">Value.</param>
	public static bool IsMultipleOf (this float f, float val) {
		return f % val == 0;
	}
	
	

	
	/// <summary>
	/// Returns a value indicating the sign of a number.
	/// Where allowZero is false, returns 0 when f is 0.
	/// </summary>
	public static int Sign (this float f, bool allowZero = false) {
		if( allowZero && f == 0.0f ) return 0;
		return f.IsPositive() ? 1 : -1;
	}

    //COMPARISON 

    //Similar to Unity's epsilon comparison, but allows for any precision.
    public static bool NearlyEqual(float a, float b, float maxDifference = 0.001f) {
		if (a == b)  { 
			return true;
		} else {
			return MathX.Difference(a, b) < maxDifference;
	    }
	}

	/// <summary>
	/// Determines if f is more than a, and less than to b.
	/// </summary>
	/// <returns><c>true</c> if f is more than a, and less than to b.; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	/// <param name="a">A.</param>
	/// <param name="b">B.</param>
	public static bool IsBetween(this float f, float a, float b) {
		return f > a && f < b;
	}
	
	/// <summary>
	/// Determines if f is equal or more than a, and less than or equal to b.
	/// </summary>
	/// <returns><c>true</c> if f is equal or more than a, and less than or equal to b.; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	/// <param name="a">A.</param>
	/// <param name="b">B.</param>
	public static bool IsBetweenInclusive(this float f, float a, float b) {
		return f >= a && f <= b;
	}
	
	/// <summary>
	/// Absolute difference between a and b. Similar to Vector3.Distance
	/// </summary>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	public static float Difference(float a, float b) {
		return Mathf.Abs(a - b);
	}

	/// <summary>
	/// Absolute difference between a and b.
	/// </summary>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	public static int Difference(int a, int b) {
		return Mathf.Abs(a - b);
	}


	/// <summary>
	/// Finds the average value from a list
	/// </summary>
	/// <param name="values">The list from which to find the average</param>
	/// <returns>A float equal to the average value of the list</returns>
	public static float Average(this IList<float> values){
		return values.Sum()/values.Count;
	}

	/// <summary>
	/// Returns the total of the specified values.
	/// </summary>
	/// <param name="values">Values.</param>
	public static float Sum(this IList<float> values){
		float total = 0;
		int count = values.Count;
		for(int i = 0; i < count; i++)
			total += values[i];
		return total;
	}

	//Absolute value as extension method
	public static float Abs(this float f) {
		return Mathf.Abs(f);
	}

	// ANGLES
	
	/// <summary>
	/// Calculates degrees as a fraction of value and max. For example, DegreesFromRange(1, 4) would return 90.
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="max">Max.</param>
	public static float DegreesFromRange (float val, float max) {
		return (val / max) * 360f;
	}

	/// <summary>
	/// Wraps a number around 360
	/// </summary>
	/// <returns>The degrees.</returns>
	/// <param name="degrees">Degrees.</param>
	public static float WrapDegrees (float degrees) {
		return MathX.RepeatInclusive(degrees, -180, 180);
	}
	
	/// <summary>
	/// Returns the difference between two degrees, wrapping values where appropriate.
	/// DEPRECIATED - Use Mathf.DeltaAngle instead (note that delta angle returns the same as this, but negative: b-a rather than a-b?).
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="max">Max.</param>
	public static float DeltaDegrees (float a, float b) {
		Debug.Log("SWAP TO DELTAANGLE, FIX BUGS AND DELETE");
		float delta = a-b;
		return WrapDegrees(delta);
	}
	
	/// <summary>
	/// Returns the difference between two degrees, wrapping values where appropriate.
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="max">Max.</param>
	public static float AbsDeltaDegrees (float a, float b) {
		return Mathf.DeltaAngle(a, b).Abs ();
	}

	/// <summary>
	/// Wraps an angle around 360 and clamps it between min and max
	/// </summary>
	/// <returns>The degrees.</returns>
	/// <param name="degrees">Degrees.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static float ClampDegrees (float degrees, float min, float max) {
		return Mathf.Clamp (MathX.WrapDegrees(degrees), min, max);
	}
	
	/// <summary>
	/// Angle in degrees to normalized direction as Vector2
	/// </summary>
	/// <returns>The vector2.</returns>
	/// <param name="degrees">Degrees.</param>
	public static Vector2 DegreesToVector2 (float degrees) {
		return RadiansToVector2(degrees * Mathf.Deg2Rad);
	}
	
	/// <summary>
	/// Angle in degrees to normalized direction as Vector2
	/// </summary>
	/// <returns>The vector2.</returns>
	/// <param name="radians">Radians.</param>
	public static Vector2 RadiansToVector2 (float radians) {
		float sin = Mathf.Sin(radians);
		float cos = Mathf.Cos(radians);
		return new Vector2(sin, cos);
	}


	//Oscellates a value between 0 and 1, where a value of 0 yields 0, 0.5 yields 1, and 1 returns to 0.
	public static float Oscellate (float value) {
		return 1f-(Mathf.Cos(Mathf.PI * 2 * value) + 1f) * 0.5f;
	}

	//Oscellates a value between min and max, where a value of 0 yields min, 0.5 yields max, and 1 returns to min.
	public static float Oscellate (float value, float min, float max) {
		return Mathf.Lerp(min, max, Oscellate(value));
	}

	//CLAMP
	
	public static float Clamp0Infinity(float value) {
		return Mathf.Clamp(value, 0, Mathf.Infinity);
	}

	public static float Clamp1Infinity(float value) {
		Debug.Log("FIX ANY ERRORS AND REMOVE ME");
		return Mathf.Clamp(value, 1, Mathf.Infinity);
	}
	
	public static int Clamp0Infinity(int value) {
		return Mathf.Clamp(value, 0, int.MaxValue);
	}
	
	public static int Clamp1Infinity(int value) {
		Debug.Log("FIX ANY ERRORS AND REMOVE ME");
		return Mathf.Clamp(value, 1, int.MaxValue);
	}
	
	/// <summary>
	/// Extension method to round a float to an int.
	/// </summary>
	/// <returns>The to int.</returns>
	/// <param name="val">Value.</param>
	public static int RoundToInt(this float val) {
		return Mathf.RoundToInt(val);
	}

	/// <summary>
	/// Rounds a value to a specified power of ten. 
	/// Decimal places can be positive or negative
	/// USED AS SO: (123.456f).RoundTo(-2); = 100;
	/// USED AS SO: (123.456f).RoundTo(2); = 123.46;
	/// </summary>
	/// <returns>The to.</returns>
	/// <param name="newNum">New number.</param>
	/// <param name="decimalPlaces">Decimal places.</param>
	public static float RoundTo(this float newNum, int decimalPlaces) {
		return Mathf.Round(newNum*Mathf.Pow(10,decimalPlaces)) / Mathf.Pow(10,decimalPlaces);
	}

	/// <summary>
	/// Rounds a value to a specified number of significant digits
	/// USED AS SO: (123.456f).RoundTo(2); = 120;
	/// </summary>
	/// <returns>The to sig.</returns>
	/// <param name="d">D.</param>
	/// <param name="digits">Digits.</param>
	public static float RoundToSig(this float d, int digits) {
		if(d == 0) return 0;
		float scale = Mathf.Pow(10, Mathf.Floor(Mathf.Log10(Mathf.Abs(d))) + 1);
		return scale * (d / scale).RoundTo(digits);
	}

	/// <summary>
	/// Rounds to an interval of nearestValue.
	/// </summary>
	/// <returns>The rounded value.</returns>
	/// <param name="newNum">Value.</param>
	/// <param name="nearestValue">Nearest value.</param>
	public static float RoundToNearest(this float newNum, float nearestValue){
		return Mathf.Round(newNum/nearestValue)*nearestValue;
	}

	/// <summary>
	/// Rounds to an interval of nearestValue.
	/// </summary>
	/// <returns>The rounded value.</returns>
	/// <param name="newNum">Value.</param>
	/// <param name="nearestValue">Nearest value.</param>
	public static int RoundToNearestInt(this float newNum, float nearestValue){
		return Mathf.RoundToInt(Mathf.Round(newNum/nearestValue)*nearestValue);
	}


	//GEOMETRY

	public static float SphereVolumeFromRadius(float radius){
		return (4/3)*Mathf.PI*(Mathf.Pow(radius, 3));
	}
	
	//INTERPOLATE FUNCTIONS.

	//Also known as Linear Dodge
	public static float LerpAdditive(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, float1 + float2, lerp);
	}

	//Multiply
	public static float LerpMultiply(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, float1 * float2, lerp);
	}

	//Screen
	public static float LerpScreen(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, 1 - ((1 - float1) * (1 - float2)), lerp);
	}

	//This doesn't quite mirror what photoshop do - I think they do both techniques (multiply and screen - thats what this is) at the same time, while we do one or the other.
	public static float LerpOverlay(float float1, float float2, float lerp = 1f){
		if(float2 < 0.5f){
			return Mathf.Lerp(float1, float1 * (float2 * 2f), lerp);
		} else {
			return Mathf.Lerp(float1, 1f - ((1f - float1) * (1f - float2 * 2f)), lerp);
		}
	}

	//Lighten
	public static float LerpLighten(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, Mathf.Max(float1, float2), lerp);
	}
	
	//Darken
	public static float LerpDarken(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, Mathf.Min(float1, float2), lerp);
	}

	//Difference
	public static float LerpDifference(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, Mathf.Max(float1, float2) - Mathf.Min(float1, float2), lerp);
	}
}
