﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public static class ReflectionX {
	
	public static Type GetTypeFromObject(object obj, string propertyPath) {
		string[] parts = propertyPath.Split('.');
		Type currentType = obj.GetType();
		for (int i = 0; i < parts.Length; i++) {
			currentType = currentType.GetField(parts[i], BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance).FieldType;
//			Debug.Log(parts[i]+" "+currentType+" "+(currentType is IList)+" "+(typeof(IList).IsAssignableFrom(currentType)));

//			if (typeof(IList).IsAssignableFrom(currentType)) {
//				FieldInfo fieldInfo = obj.GetType().GetField(parts[i]);
//				object x = fieldInfo.GetValue(obj);
//				int indexStart = parts[i+2].IndexOf("[")+1;
//				int collectionElementIndex = Int32.Parse(parts[i+2].Substring(indexStart, parts[i+2].Length-indexStart-1));
//				IList list = x as IList;
//				if(MathX.IsBetweenInclusive(collectionElementIndex, 0, list.Count-1)) {
//					obj = (x as IList)[collectionElementIndex];
////					type = obj.GetType();
//				} else {
//					DebugX.LogWarning ("Index: "+collectionElementIndex+", List Count: "+list.Count+", Current Path Part: "+parts[i]+", Full Path: "+propertyPath);
//					return typeof(object);
//				}
//				continue;
//			}
		}
		return currentType;
	}

	public static T GetValueFromObject<T>(object obj, string propertyPath) {
//		Type type = obj.GetType();
		FieldInfo fieldInfo = null;
		string[] parts = propertyPath.Split('.');
		int partIndex = -1;
		foreach (string part in parts) {
			partIndex++;
			if(obj is T) return (T)obj;
			fieldInfo = obj.GetType().GetField(part);
			if(fieldInfo == null)continue;
			
			object x = fieldInfo.GetValue(obj);
			
			if (x is IList) {
				int indexStart = parts[partIndex+2].IndexOf("[")+1;
				int collectionElementIndex = Int32.Parse(parts[partIndex+2].Substring(indexStart, parts[partIndex+2].Length-indexStart-1));
				IList list = x as IList;
				if(MathX.IsBetweenInclusive(collectionElementIndex, 0, list.Count-1)) {
					obj = (x as IList)[collectionElementIndex];
//					type = obj.GetType();
				} else {
					DebugX.LogWarning ("Index: "+collectionElementIndex+", List Count: "+list.Count+", Current Path Part: "+part+", Full Path: "+propertyPath);
					return default(T);
				}
				continue;
			} else {
//				type = fieldInfo.GetType();
			}
			
			obj = fieldInfo.GetValue(obj);
		}
			
		if(!(obj is T)) return default(T);
		return (T)obj;
	}
	
	// TODO - This doesn't work on structs that aren't in arrays. 
	// There's some code below that does. fieldInfo.SetValue might be the answer, although not having it in the first setter doesn't make a difference. Must be related to the setter at the bottom?
	// Weirdly
	public static void SetValueFromObject<T>(object obj, string propertyPath, T val) {
//		Type type = obj.GetType();
		FieldInfo fieldInfo = null;
		string[] parts = propertyPath.Split('.');
		object value = obj;
		int partIndex = -1;
		foreach (string part in parts) {
			partIndex++;
			
			if(value is T) {
//				value = val;
				fieldInfo.SetValue(obj, val);
				return;
			}
			fieldInfo = value.GetType().GetField(part);
			if(fieldInfo == null)continue;
			object x = fieldInfo.GetValue(value);
			
			if (x is IList) {
//				int indexStart = parts[partIndex+2].IndexOf("[")+1;
//				string collectionPropertyName = parts[partIndex+2].Substring(0, indexStart-1);
//				int collectionElementIndex = Int32.Parse(parts[partIndex+2].Substring(indexStart, parts[partIndex+2].Length-indexStart-1));
//				type = value.GetType();
				continue;
			}
			else {
//				type = fieldInfo.GetType();
			}
			
			value = fieldInfo.GetValue(value);
		}
		
		value = val;
	}
	
	// This is an example that works on structs, but not arrays and nested objects very well. Find the secret!
//	public static void SetBaseProperty<T>(this UnityEditor.SerializedProperty prop, T val) {
//		// Separate the steps it takes to get to this property
//		string[] separatedPaths = prop.propertyPath.Split('.');
//		// Go down to the root of this serialized property
//		System.Object reflectionTarget = prop.serializedObject.targetObject as object;
//		
//		// Walk down the path to get the target object
//		string finalPath = "";
//		foreach (string path in separatedPaths) {
//			FieldInfo _fieldInfo = reflectionTarget.GetType().GetField(path);
//			try {
//				reflectionTarget = _fieldInfo.GetValue(reflectionTarget);
//				finalPath = path;
//			} catch {}
//		}
//		
//		FieldInfo fieldInfo = reflectionTarget.GetType().GetField(finalPath);
//		
//		if(separatedPaths.Last().EndsWith("]")) {
//			// Should look like [0]
//			string s = separatedPaths.Last();
//			int index = int.Parse(s.Split('[', ']')[1]);
//			//			Debug.Log (index);
//			IList<T> containerList = (IList<T>)reflectionTarget;
//			//			DebugX.LogList (containerList);
//			containerList[index] = val;
//			
//			fieldInfo.SetValue(reflectionTarget, val);
//		} else {
//			fieldInfo.SetValue(reflectionTarget, val);
//		}
//	}
	
//	private static void SetValueFromObject<T>(object obj, string propertyPath, T val) {
//		Type type = obj.GetType();
//		FieldInfo fieldInfo = null;
//		string[] parts = propertyPath.Split('.');
//		object value = obj;
//		int partIndex = -1;
//		foreach (string part in parts) {
//			partIndex++;
//			
//			if(value.IsTypeOf<T>()) {
//				value = val;
//				return;
//			}
//			fieldInfo = value.GetType().GetField(part);
//			if(fieldInfo == null)continue;
//			object x = fieldInfo.GetValue(value);
//			
//			if (x is IList)
//			{
//				int indexStart = parts[partIndex+2].IndexOf("[")+1;
//				string collectionPropertyName = parts[partIndex+2].Substring(0, indexStart-1);
//				int collectionElementIndex = Int32.Parse(parts[partIndex+2].Substring(indexStart, parts[partIndex+2].Length-indexStart-1));
//				type = value.GetType();
//				continue;
//			}
//			else {
//				type = fieldInfo.GetType();
//			}
//			
//			value = fieldInfo.GetValue(value);
//		}
//		
//		value = val;
//	}
}
