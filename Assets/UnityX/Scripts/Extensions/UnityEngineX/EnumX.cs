﻿using UnityEngine;
using System;

/// <summary>
/// Provides additional functionality to Enums.
/// </summary>
public static class EnumX {
	
	/// <summary>
	/// Gets the length of the enum
	/// </summary>
	/// <param name="src">Source.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static int Length<T>(this T src) where T : struct {
		return EnumX.Length<T>();
	}
	
	/// <summary>
	/// Gets the length of the enum
	/// </summary>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static int Length<T>() where T : struct {
		#if !UNITY_WINRT
		if (!typeof(T).IsEnum) Debug.LogError("Argument {0} is not an Enum "+typeof(T).FullName);
		#endif
		return Enum.GetNames(typeof(T)).Length;
	}

	/// <summary>
	/// Gets the index of the specified enum value.
	/// </summary>
	/// <returns>The index of the value in the enum.</returns>
	/// <param name="src">Source.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static int IndexOf<T>(this T src) where T : struct {
		#if !UNITY_WINRT
		if (!typeof(T).IsEnum) Debug.LogError("Argument {0} is not an Enum "+typeof(T).FullName);
		#endif
		
		T[] Arr = (T[])Enum.GetValues(src.GetType());
		return Array.IndexOf<T>(Arr, src);
	}
	
	/// <summary>
	/// Gets a random enum.
	/// </summary>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Random<T>() where T : struct {
		#if !UNITY_WINRT
		if (!typeof(T).IsEnum) Debug.LogError("Argument {0} is not an Enum "+typeof(T).FullName);
		#endif
		return (T)Enum.ToObject(typeof(T), UnityEngine.Random.Range(0, EnumX.Length<T>()));
	}
	
	/// <summary>
	/// Gets the next member in the enum. Repeats if the current member is the last in the sequence.
	/// </summary>
	/// <param name="src">Source.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Next<T>(this T src) where T : struct {
		#if !UNITY_WINRT
		if (!typeof(T).IsEnum) Debug.LogError("Argument {0} is not an Enum "+typeof(T).FullName);
		#endif
		T[] Arr = (T[])Enum.GetValues(src.GetType());
		int j = Array.IndexOf<T>(Arr, src) + 1;
		return (j == Arr.Length) ? Arr[0] : Arr[j];            
	}
	
	/// <summary>
	/// Gets the previous member in the enum. Repeats if the current member is the first in the sequence.
	/// </summary>
	/// <param name="src">Source.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Previous<T>(this T src) where T : struct {
		#if !UNITY_WINRT
		if (!typeof(T).IsEnum) Debug.LogError("Argument {0} is not an Enum "+typeof(T).FullName);
		#endif
		T[] Arr = (T[])Enum.GetValues(src.GetType());
		int j = Array.IndexOf<T>(Arr, src) - 1;
		return (j == -1) ? Arr[Arr.Length-1] : Arr[j];            
	}
	
	public static T TryParse<T>(string _string) where T : struct {
		#if !UNITY_WINRT
		if (!typeof(T).IsEnum) Debug.LogError("Argument {0} is not an Enum "+typeof(T).FullName);
		#endif
		try {
			T t = (T)Enum.Parse(typeof(T), _string, true);
			return t;
		} catch {
			return default(T);
		}
	}

	/// <summary>
	/// Returns an array of all the values of the enum.
	/// </summary>
	/// <returns>The array.</returns>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T[] ToArray<T>() where T : struct {
		T[] array = new T[Enum.GetValues(typeof(T)).Length];
		for(int i = 0; i < array.Length; i++) {
			array[i] = (T)Enum.ToObject(typeof(T), i);
		}
		return array;
	}

	/// <summary>
	/// Returns an array of all the names of the enum values.
	/// </summary>
	/// <returns>The array.</returns>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static string[] ToStringArray<T>() where T : struct {
		string[] array = new string[Enum.GetValues(typeof(T)).Length];
		for(int i = 0; i < array.Length; i++) {
			array[i] = ((T)Enum.ToObject(typeof(T), i)).ToString();
		}
		return array;
	}
	
	/// <summary>
	/// Determines if the value is first in the specified enum.
	/// </summary>
	/// <returns><c>true</c> if is first the specified src; otherwise, <c>false</c>.</returns>
	/// <param name="src">Source.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool IsFirst<T>(this T src) where T : struct {
		return IndexOf(src) == 0;
	}

	/// <summary>
	/// Determines if the value is last in the specified enum.
	/// </summary>
	/// <returns><c>true</c> if is last the specified src; otherwise, <c>false</c>.</returns>
	/// <param name="src">Source.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool IsLast<T>(this T src) where T : struct {
		return IndexOf(src) == Length<T>()-1;
	}

	public static bool IsValid<T>(this T src) where T : struct {
		return Enum.IsDefined(typeof(T), src);
	}
}