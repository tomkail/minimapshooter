﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ObjectX {
	
	/// <summary>
	/// Instantiate the specified source as T, getting T from the source. Less type-safe, as it's always possible that the gameobject doesn't contain T.
	/// </summary>
	/// <param name="source">Source.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Instantiate<T>(GameObject source) where T : UnityEngine.Component {
		T componentSource = source.GetComponent<T>();
		if(componentSource == null) Debug.LogError("Couldn't find component "+typeof(T)+" on source gameObject " +source);
		return ObjectX.Instantiate<T>(componentSource);
	}
	
	/// <summary>
	/// Instantiate the specified source as T.
	/// </summary>
	/// <param name="source">Source.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Instantiate<T>(T source) where T : UnityEngine.Object {
		if(source == null) Debug.LogError("Could not Instantiate because source is null!");
		return Object.Instantiate(source) as T;
	}
	
	/// <summary>
	/// Instantiate the specified source as T with specified position and rotation.
	/// </summary>
	/// <param name="source">Source.</param>
	/// <param name="position">Position.</param>
	/// <param name="rotation">Rotation.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Instantiate<T>(T source, Vector3 position, Quaternion rotation) where T : UnityEngine.Object {
		T clone = ObjectX.Instantiate<T>(source);
		Transform t = GetTransformFromObject(clone);
		if(t == null) {
			Debug.LogError("Attempted to Instantiate Object of Type ("+typeof (T).ToString()+") without a Transform component with a specific position and rotation.");
			return clone;
		}
		
		t.position = position;
		t.rotation = rotation;
		
		return clone;
	}
	
	/// <summary>
	/// Instantiates the source as T as a child of parent.
	/// </summary>
	/// <returns>The as child.</returns>
	/// <param name="source">Source.</param>
	/// <param name="parent">Parent. Leave out (or pass null) to use source's parent.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T InstantiateAsChild<T>(T source, Transform parent = null, bool worldPositionStays = true) where T : UnityEngine.Object {
		if( parent == null ) parent = GetTransformFromObject(source).parent;
		return ObjectX.InstantiateAsChild<T>(source, parent, Vector3.zero, Quaternion.identity, worldPositionStays);
	}
	
	/// <summary>
	/// Instantiates the source as T as a child of parent with local position and rotation properties.
	/// </summary>
	/// <returns>The as child.</returns>
	/// <param name="source">Source.</param>
	/// <param name="parent">Parent. Pass null to use source's parent.</param>
	/// <param name="localPosition">Local position.</param>
	/// <param name="localRotation">Local rotation.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T InstantiateAsChild<T>(T source, Transform parent, Vector3 localPosition, Quaternion localRotation, bool worldPositionStays = true) where T : UnityEngine.Object {
		T clone = ObjectX.Instantiate<T>(source);
		Transform t = GetTransformFromObject(clone);
		if(t == null) {
			Debug.LogError("Attempted to Instantiate Object of Type ("+typeof (T).ToString()+") without a Transform component with a specific position and rotation.");
			return clone;
		}

		if( parent == null ) parent = t.parent;
		
		t.SetParent(parent, worldPositionStays);
		t.localPosition = localPosition;
		t.localRotation = localRotation;
		
		return clone;
	}
	

	/// <summary>
	/// Destroys an object using the correct function for the play mode state.
	/// Uses Destroy in play mode, DestroyImmediate in editor.
	/// </summary>
	/// <param name="o">O.</param>
	public static void DestroyAutomatic(Object o) {
		if(Application.isPlaying)
			UnityEngine.Object.Destroy (o);
		else
			UnityEngine.Object.DestroyImmediate (o);
	}

	/// <summary>
	/// Gets the transform from object.
	/// Necessary because GameObject and Component both inherit Object, but independantly implement Transform.
	///	This means that another class inheriting from Object might not have a Transform.
	/// </summary>
	/// <returns>The transform from object.</returns>
	/// <param name="source">Source.</param>
	private static Transform GetTransformFromObject(Object source) {
		if (source is GameObject) {
			return (source as GameObject).transform;
		} else if (source is Component) {
			return (source as Component).transform;
		} else {
			return null;
		}
	}
}