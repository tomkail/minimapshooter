﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Provides additional functionality to IEnumerables
/// </summary>
public static class IEnumerableX {
	
    public static T Random<T>(this IEnumerable<T> source) {
		if(source.Count() == 0) return default(T);
		return source.ElementAt(source.RandomIndex());
    }
    
	public static int RandomIndex<T>(this IEnumerable<T> source) {
		return UnityEngine.Random.Range(0, source.Count());
	}

	/// <summary>
	/// 
	/// </summary>
	/// <param name="list"></param>
	/// <typeparam name="T"></typeparam>
	public static bool IsEmpty<T>(this IEnumerable<T> list) {
		return list.Count() == 0;
	}
	
	/// <summary>
	/// Determines if the specified list is null or empty.
	/// </summary>
	/// <returns><c>true</c> if the specified list is null or empty; otherwise, <c>false</c>.</returns>
	/// <param name="list">List.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool IsNullOrEmpty<T>(this IEnumerable<T> list) {
		return (list == null || list.IsEmpty());
	}


    /// <summary>
	/// Determines if the specified array index is valid.
    /// </summary>
	/// <returns><c>true</c> if the specified array index is valid; otherwise, <c>false</c>.</returns>
    /// <param name="array">Array.</param>
    /// <param name="index">Index.</param>
    /// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool ContainsIndex<T>(this IEnumerable<T> list, int index) {
		if(list.IsNullOrEmpty()) return false;
		return MathX.IsBetweenInclusive(index, 0, list.Count()-1);
    }
}