﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(RuntimeSceneSetLoader))]
public class RuntimeSceneSetLoaderEditor : BaseEditor<RuntimeSceneSetLoader> {

	public override void OnInspectorGUI () {
		base.Repaint();
		base.OnInspectorGUI ();
		if(data.currentLevelSetLoadTask != null) {
			DrawSceneSetLoadTask(data.currentLevelSetLoadTask, "Loading");
			if(data.pendingLevelSetLoadTask != null) {
				DrawSceneSetLoadTask(data.pendingLevelSetLoadTask, "Pending");
			}
		} else {
			EditorGUILayout.HelpBox("No scene sets currently being loaded", MessageType.Info);
		}
	}

	void DrawSceneSetLoadTask (RuntimeSceneSetLoadTask task, string prefix) {
		EditorGUILayout.BeginVertical(GUI.skin.box);
		EditorGUILayout.LabelField(prefix +" "+ task.sceneSet.name);
		if(task.cancelled) {
			GUILayout.Label("Cancelled");
			if (GUILayout.Button("Uncancel")) {
				task.Uncancel();
			}
		} else {
			if(task.unloading) {
				GUILayout.Label("Unloading");
			} else if(task.activated) {
				GUILayout.Label("Activated");
			} else if(task.loadingDone) {
				GUILayout.Label("Ready");
				if (GUILayout.Button("Activate")) {
					task.Activate();
				}
			} else if(task.loading) {
				GUILayout.Label("Loading");
			} else if(!task.loading) {
				GUILayout.Label("Waiting");
				if (GUILayout.Button("Load")) {
					task.Load();
				}
			}
			if (GUILayout.Button("Cancel")) {
				task.Cancel();
			}
		}

		EditorGUILayout.EndVertical();
	}
}
