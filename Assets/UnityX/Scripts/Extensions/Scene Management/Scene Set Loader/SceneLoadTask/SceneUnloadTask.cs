using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SceneUnloadTask : SceneTask {

	public SceneUnloadTask(string sceneName) : base (sceneName) {}

	public void Unload () {
		RuntimeSceneSetLoader.Instance.StartCoroutine(UnloadCR());
	}

	public IEnumerator UnloadCR () {
		op = SceneManager.UnloadSceneAsync(sceneName);
        while (!op.isDone)
			yield return null;
		complete = true;
		op = null;
    }
}