using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SceneLoadTask : SceneTask {
	
	public bool loadingDone = false;
	public bool activated = false;
	public bool cancel = false;

//	public event OnSceneTaskEvent OnStartLoad;
//	public event OnSceneTaskEvent OnCompleteLoad;
//	public event OnSceneTaskEvent OnCompleteCancel;

	private static float activationLoadStopMagicNumber = 0.9f;

	public SceneLoadTask(string sceneName) : base (sceneName) {}

	public void Load () {
		RuntimeSceneSetLoader.Instance.StartCoroutine(LoadCR());
	}

	public IEnumerator LoadCR () {
		op = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
		op.allowSceneActivation = false;
//		if(OnStartLoad != null)
//			OnStartLoad();
		// when "allowSceneActivation" is false Unity will stop at "0.9f"
        // until you set allowSceneActivation back to true
		while(op.progress < activationLoadStopMagicNumber)
			yield return null;
		loadingDone = true;
		while (!op.isDone)
			yield return null;
		Debug.Assert(SceneManager.GetSceneByName(sceneName).isLoaded);
		if(cancel) {
			SceneUnloadTask unloadTask = new SceneUnloadTask(sceneName);
			unloadTask.Unload();
			while(!unloadTask.complete)
				yield return null;
		}
		complete = true;
		op = null;
    }

    public void Activate () {
    	activated = true;
		UpdateAllowSceneActivation();
    }

	public void Cancel() {
		cancel = true;
		UpdateAllowSceneActivation();
    }

	public void Uncancel() {
		if(!complete) {
			cancel = false;
			UpdateAllowSceneActivation();
		} else {
			Debug.Log("Attempted to uncancel a cancelled scene load for scene '"+sceneName+"', but scene load has already completed");
		}
    }

    private void UpdateAllowSceneActivation () {
		if(complete) {
			Debug.LogWarning("Attempted to update allowSceneActivation, but activation is already complete.");
			return;
		}
		op.allowSceneActivation = activated || cancel;
    }
}