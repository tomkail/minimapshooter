using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public abstract class SceneTask {
	public string sceneName {get; private set;}
	public bool complete {get; protected set;}
	protected AsyncOperation op;

	public SceneTask(string sceneName) {
		this.sceneName = sceneName;
	}

	public override string ToString () {
		return string.Format ("[SceneUnloadTask] SceneName {0}", sceneName);
	}
}
