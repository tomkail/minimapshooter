using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//[System.Serializable]
public class RuntimeSceneSetLoadTask {
	public RuntimeSceneSet sceneSet {get; private set;}
	public LoadSceneMode sceneLoadMode {get; private set;}

	public List<SceneLoadTask> loadTasks = new List<SceneLoadTask>();
	public List<SceneUnloadTask> unloadTasks = new List<SceneUnloadTask>();

	public bool loading {get; private set;}
	public bool unloading {get; private set;}
	public bool cancelled {get; private set;}

	public bool activated {
		get {
			bool done = true;
			foreach(SceneLoadTask task in loadTasks) {
				if(!task.activated) {
					done = false;
					break;
				}
			}
			return done;
		}
	}

	public bool loadingDone {
		get {
			bool done = true;
			foreach(SceneLoadTask task in loadTasks) {
				if(!task.loadingDone) {
					done = false;
					break;
				}
			}
			return done;
		}
	}

	public bool complete {
		get {
			bool done = true;
			foreach(SceneLoadTask task in loadTasks) {
				if(!task.complete) {
					done = false;
					break;
				}
			}
			return done;
		}
	}

	public bool unloadingDone {
		get {
			bool done = true;
			foreach(SceneUnloadTask task in unloadTasks) {
				if(!task.complete) {
					done = false;
					break;
				}
			}
			return done;
		}
	}

	public RuntimeSceneSetLoadTask (RuntimeSceneSet sceneSetup, LoadSceneMode sceneLoadMode = LoadSceneMode.Single) {
		this.sceneSet = sceneSetup;
		this.sceneLoadMode = sceneLoadMode;
		Debug.Assert(sceneSetup != null, "Scene setup is null");
	}

	private void AssignTasks () {
		var pathsToLoad = sceneSet.AllScenePaths();

		loadTasks = new List<SceneLoadTask>(); 
		{
			foreach(string scenePath in pathsToLoad) {
				Scene scene = SceneManager.GetSceneByPath(scenePath);
				if(scene.isLoaded)
					continue;
				string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePath);
				loadTasks.Add(new SceneLoadTask(sceneName));
			}
		}

		unloadTasks = new List<SceneUnloadTask>(); 
		if(sceneLoadMode == LoadSceneMode.Single) {
			string[] currentPaths = SceneManagerX.GetCurrentScenePaths();

			List<string> pathsToUnload = currentPaths.Except(pathsToLoad).ToList();
			pathsToUnload.Reverse();
			foreach(string scenePath in pathsToUnload) {
				Scene scene = SceneManager.GetSceneByPath(scenePath);
				if(!scene.isLoaded)
					continue;
				string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePath);
				unloadTasks.Add(new SceneUnloadTask(sceneName));
			}
		}
	}

	public void Load () {
		AssignTasks();
		loading = true;
		foreach (var task in loadTasks)
			task.Load();
	}

	public void Activate () {
		foreach(SceneLoadTask task in loadTasks) {
			task.Activate();
		}
	}

	public void Unload () {
		unloading = true;
		foreach (var task in unloadTasks)
			task.Unload();
	}

	public void Cancel() {
		cancelled = true;
		foreach(SceneLoadTask task in loadTasks) {
			task.Cancel();
		}
    }

	public void Uncancel() {
		cancelled = false;
		foreach(SceneLoadTask task in loadTasks) {
			task.Uncancel();
		}
    }

	public string[] GetSceneNamesToLoad () {
		return loadTasks.Select(x => x.sceneName).ToArray();
	}
//
//	public bool IsEqual (SceneSetLoadTask other) {
//		return GetSceneNamesToLoad().SequenceEqual(other.GetSceneNamesToLoad()) && unloadTasks.SequenceEqual(other.unloadTasks);
//    }
}
