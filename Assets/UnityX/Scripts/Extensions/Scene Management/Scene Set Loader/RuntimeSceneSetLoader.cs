﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Scene loader class. Loads a RuntimeSceneSet, removing the previous set of scenes.
/// </summary>
public class RuntimeSceneSetLoader : MonoSingleton<RuntimeSceneSetLoader> {
	public bool loading {
		get {
			return currentLevelSetLoadTask != null;
		}
	}

	private RuntimeSceneSetLoadTask _currentLevelSetLoadTask;
	public RuntimeSceneSetLoadTask currentLevelSetLoadTask {
		get {
			return _currentLevelSetLoadTask;
		} private set {
			if(_currentLevelSetLoadTask == value)
				return;
			_currentLevelSetLoadTask = value;
		}
	}

	public RuntimeSceneSetLoadTask pendingLevelSetLoadTask {get; private set;}

	public delegate void OnSceneSetLoadEvent(RuntimeSceneSet sceneSet);
	public delegate void OnSceneSetUnloadEvent();
	public event OnSceneSetLoadEvent OnWillLoad;
	public event OnSceneSetLoadEvent OnDidLoad;
	public event OnSceneSetUnloadEvent OnWillUnload;
	public event OnSceneSetUnloadEvent OnDidUnload;

	public void LoadSceneSetup(RuntimeSceneSet sceneSetup, LoadSceneMode sceneLoadMode = LoadSceneMode.Single) {
		Debug.Log("Try to load scene setup "+sceneSetup.name);
		RuntimeSceneSetLoadTask newLevelSetLoadTask = new RuntimeSceneSetLoadTask(sceneSetup, sceneLoadMode);
		LoadSceneSetup(newLevelSetLoadTask);
	}

	private void LoadSceneSetup(RuntimeSceneSetLoadTask newLevelSetLoadTask) {
		if(currentLevelSetLoadTask == null) {
			currentLevelSetLoadTask = newLevelSetLoadTask;
			StartCoroutine(LoadSceneSetupCR());
		} else {
			if(pendingLevelSetLoadTask == null) {
				if (currentLevelSetLoadTask.sceneSet != newLevelSetLoadTask.sceneSet) {
					pendingLevelSetLoadTask = newLevelSetLoadTask;
				} else {
					return;
				}
			} else {
				if(currentLevelSetLoadTask.sceneSet == newLevelSetLoadTask.sceneSet) {
					pendingLevelSetLoadTask = null;
					currentLevelSetLoadTask.Uncancel();
					return;
				} else if(pendingLevelSetLoadTask.sceneSet != newLevelSetLoadTask.sceneSet) {
					pendingLevelSetLoadTask = newLevelSetLoadTask;
				} else {
					return;
				}
			}
			if(currentLevelSetLoadTask != null)
				currentLevelSetLoadTask.Cancel();
		}
	}

	private IEnumerator LoadSceneSetupCR() {
		if(OnWillLoad != null) OnWillLoad(currentLevelSetLoadTask.sceneSet);
		currentLevelSetLoadTask.Load();
		while(!currentLevelSetLoadTask.loadingDone) {
			yield return null;
		}
		currentLevelSetLoadTask.Activate();
		while(!currentLevelSetLoadTask.complete) {
			yield return null;
		}
		if(!currentLevelSetLoadTask.cancelled) {
			if (OnWillUnload != null) OnWillUnload();
			currentLevelSetLoadTask.Unload();
			while(!currentLevelSetLoadTask.unloadingDone) {
				yield return null;
			}
			if (OnDidUnload != null) OnDidUnload();
		}
		CheckCompleteLoad();
    }

    // This used to be a coroutine - I'm not sure if changing it back to function that starts a new coroutine has any downsides.
	private void CheckCompleteLoad() {
		if(pendingLevelSetLoadTask != null) {
			currentLevelSetLoadTask = pendingLevelSetLoadTask;
			Debug.Log("Starting Queued "+currentLevelSetLoadTask.sceneSet.name);
			pendingLevelSetLoadTask = null;
			StartCoroutine(LoadSceneSetupCR());
		} else {
			// If we're not immediately loading another scene, call ready on our new scene.
			CompleteLoad();
		}
    }

    private void CompleteLoad () {
		if (currentLevelSetLoadTask.cancelled) {
			currentLevelSetLoadTask = null;
			return;
		} else {
			RuntimeSceneSet sceneSetup = currentLevelSetLoadTask.sceneSet;
			currentLevelSetLoadTask = null;
			if (OnDidLoad != null) OnDidLoad(sceneSetup);
		}
    }
}