﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class WorldSpaceUIElement : UIBehaviour {
	
	[SerializeField]
	private Camera _camera;
	public new Camera camera {
		get {
			if(_camera == null) return Camera.main;
			else return _camera;
		} set {
			if(_camera == value)
				return;
			_camera = value;
			Refresh();
		}
	}

	[SerializeField]
	private Transform _target;
	public Transform target {
		get {
			return _target;
		} set {
			if(_target == value)
				return;
			_target = value;
			Refresh();
		}
	}

	public Vector3 worldPosition;
	public Quaternion worldRotation;

	private Vector3 targetPositionInternal {
		get {
			return target == null ? worldPosition : target.position;
		}
	}

	private Quaternion targetRotationInternal {
		get {
			return target == null ? worldRotation : target.rotation;
		}
	}
	
	public bool updatePosition = true;
	public bool updateRotation = true;
	public bool updateScale = false;
	public bool updateOcclusion = false;
	public float scaleMultiplier = 1;
	public float minScale = 0.2f;
	public float maxScale = 1f;
	
	public bool clampToScreen;
	
	[HideInInspector]
	public bool onScreen;

	[HideInInspector]
	public bool occluded;
	public int occlusionMask = Physics.DefaultRaycastLayers;

	public float distanceFromCamera;
	
	private RectTransform rectTransform;
	private Canvas parentCanvas;
	private RectTransform parentRect;
	
	public void SetParentCanvas (Canvas parentCanvas) {
		this.parentCanvas = parentCanvas;
		if(this.parentCanvas != null)
			parentRect = this.parentCanvas.GetComponent<RectTransform>();
	}
	
	public Vector3 GetLocalScale () {
		float scale = 1;
		if(parentCanvas.renderMode == RenderMode.WorldSpace && camera.orthographic) {
//			return parentCanvas.transform.InverseTransformVector(Vector3.one);
			scale = camera.orthographicSize;
		} else {
			if(camera.orthographic) {
				distanceFromCamera = Vector3X.DistanceInDirection(targetPositionInternal, camera.transform.position, camera.transform.forward);
			} else {
				distanceFromCamera = Vector3.Distance(targetPositionInternal, camera.transform.position);
			}
			float frustrumHeight = camera.GetFrustrumHeightAtDistance(distanceFromCamera);
			scale = (1f/frustrumHeight);
		}
		scale *= scaleMultiplier;
		float clampedScale = Mathf.Clamp(scale, minScale, maxScale);
		return Vector3.one * clampedScale;
	}
	
	protected override void Awake () {
		rectTransform = this.GetRectTransform();
		if(camera == null)
			camera = Camera.main;
		SetParentCanvas(this.GetParentCanvas());
	}

	protected override void OnEnable () {
		Refresh();
	}
	
	protected override void OnTransformParentChanged () {
		SetParentCanvas(this.GetParentCanvas());
		base.OnTransformParentChanged ();
	}

	// LateUpdate because we want it to come even after camera updates
	private void LateUpdate () {
		Refresh();
	}

	private void Refresh () {
		if(camera == null || parentCanvas == null || parentRect == null) 
			return;
		if(updateScale)
			ScaleFromDistance();
		if(updatePosition)
			SetPositionFromWorldPosition();
		if(updateRotation)
			SetAngleFromRotation();
		if(updateOcclusion)
			CheckOcclusion();
	}
	
	public void ScaleFromDistance () {
		rectTransform.localScale = GetLocalScale();
	}
	
	public void SetPositionFromWorldPosition () {
		Vector3? targetPositionNullable = parentCanvas.WorldPointToLocalPointInRectangle(camera, targetPositionInternal);
		if(targetPositionNullable == null) {
			onScreen = false;
			return;
		}

		var targetPosition = (Vector3) targetPositionNullable;

		if(parentCanvas.renderMode == RenderMode.WorldSpace) {
			targetPosition = transform.parent.InverseTransformPoint(targetPositionInternal);
			targetPosition.z = 0;
		}
		
		rectTransform.localPosition = targetPosition;
		
		if(clampToScreen) {
			Rect smallRect = rectTransform.GetScreenRect(parentCanvas);
			Rect largeRect = parentCanvas.GetComponent<RectTransform>().GetScreenRect(parentCanvas);
			Rect clampedRect = RectX.ClampInsideKeepSize(smallRect, largeRect);
			Vector3? canvasSpace = parentCanvas.ScreenPointToCanvasSpace(clampedRect.position + smallRect.size * 0.5f);
			if(canvasSpace != null)
				rectTransform.position = (Vector3)canvasSpace;
		}
		
		onScreen = parentCanvas.GetComponent<RectTransform>().rect.Contains((Vector2)targetPositionNullable);
	}

	public void SetAngleFromRotation () {
		Debug.Log("HELP!");
//		float angle = Vector2X.Degrees (Vector3.ProjectOnPlane(targetRotationInternal * Vector3.forward, SpaceGameWorld.Instance.floor.GetWorldNormalAtWorldPosition(targetPositionInternal)).XZ());
//		rectTransform.localRotation = Quaternion.AngleAxis (angle, -Vector3.forward);
	}

	void CheckOcclusion() {
		RaycastHit hit;
		Vector3 offset = targetPositionInternal-camera.transform.position;
		float maxDistance = offset.magnitude;
		occluded = maxDistance > 0.0f && Physics.Raycast(camera.transform.position, offset / maxDistance, out hit, maxDistance, occlusionMask);
	}

	// Test clamp with this
	private void _OnDrawGizmos () {
//		if(!Application.isPlaying) return;
		Vector3? targetPosition = parentCanvas.WorldPointToLocalPointInRectangle(camera, targetPositionInternal);
		if(targetPosition == null) return;
		
		Rect smallRect = rectTransform.GetScreenRect(parentCanvas);
		Rect largeRect = parentCanvas.GetComponent<RectTransform>().GetScreenRect(parentCanvas);
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireCube(smallRect.center, smallRect.size);
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube(largeRect.center, largeRect.size);
		Gizmos.color = Color.red;
		
		Rect clampedRect = RectX.ClampInsideKeepSize(smallRect, largeRect);
		Gizmos.DrawWireCube(clampedRect.center, clampedRect.size);	
	}
}
