﻿/// Credit CiaccoDavide
/// Sourced from - http://ciaccodavi.de/unity/uipolygon

using System.Collections.Generic;

namespace UnityEngine.UI.Extensions {
    [AddComponentMenu("UI/Extensions/Primitives/Polygon")]
    public class UIPolygon : MaskableGraphic {
        [SerializeField]
        Texture _texture;
        public bool fill = true;
		public float strokeSize = 5;
        public enum StrokePosition {
        	Outside,
        	Inside,
        	Center
        }
        public StrokePosition strokePosition = StrokePosition.Outside;
        [SerializeField]
        private Vector2[] _points = new Vector2[3];
		public Vector2[] points {
			get {
				return _points;
			}
			set {
				_points = value;
				base.SetVerticesDirty();
			}
		}

        public override Texture mainTexture
        {
            get
            {
                return _texture == null ? s_WhiteTexture : _texture;
            }
        }
        public Texture texture
        {
            get
            {
                return _texture;
            }
            set
            {
                if (_texture == value) return;
                _texture = value;
                SetVerticesDirty();
                SetMaterialDirty();
            }
        }

        public void DrawPolygon(Vector2[] _points) {
			points = _points;
        }

		protected UIVertex[] SetVbo(Vector2[] vertices, Vector2[] uvs, Color color) {
            UIVertex[] vbo = new UIVertex[4];
            for (int i = 0; i < vertices.Length; i++) {
                var vert = UIVertex.simpleVert;
                vert.color = color;
                vert.position = vertices[i];
                vert.uv0 = uvs[i];
                vbo[i] = vert;
            }
            return vbo;
        }

        protected override void OnPopulateMesh(VertexHelper vh) {
            vh.Clear();
            Vector2 uv0 = new Vector2(0, 0);
            Vector2 uv1 = new Vector2(0, 1);
            Vector2 uv2 = new Vector2(1, 1);
            Vector2 uv3 = new Vector2(1, 0);
            Vector2 pos0;
            Vector2 pos1;
            Vector2 pos2;
            Vector2 pos3;

			Vector2 total = Vector2.zero;
			for(int i = 0; i < points.Length; i++){
				total += points[i];
			}
			Vector2 center = total/points.Length;


			float outsideFactor = 0;
			float insideFactor = 0;
			if(!fill) {
				if(strokePosition == StrokePosition.Center) {
					outsideFactor = 0.5f;
					insideFactor = 0.5f;
				} else if(strokePosition == StrokePosition.Inside) {
					insideFactor = 1f;
				} else if(strokePosition == StrokePosition.Outside) {
					outsideFactor = 1f;
				}
			}

			if (fill) {
	            for (int i = 0; i < points.Length; i++) {
					pos0 = pos3 = points[i];
					pos1 = pos2 = points.GetRepeating(i+1);

					pos2 = center;
					pos3 = center;

					vh.AddUIVertexQuad(SetVbo(new[] { pos0, pos1, pos2, pos3 }, new[] { uv0, uv1, uv2, uv3 }, color));
				}
            } else {
				for (int i = 0; i < points.Length; i++) {
					pos0 = pos3 = points[i];
					pos1 = pos2 = points.GetRepeating(i+1);

					Vector2 leftVector0 = (pos0 - points.GetRepeating(i-1)).normalized;
					Vector2 rightVector0 = (pos0 - pos1).normalized;

					Vector2 leftVector1 = (pos1 - pos0).normalized;
					Vector2 rightVector1 = (pos1 - points.GetRepeating(i+2)).normalized;

					Vector2 bisect0 = ((leftVector0+rightVector0) * 0.5f).normalized;
					Vector2 bisect1 = ((leftVector1+rightVector1) * 0.5f).normalized;
					pos0 -= bisect0 * strokeSize * insideFactor;
					pos1 -= bisect1 * strokeSize * insideFactor;
					pos2 += bisect1 * strokeSize * outsideFactor;
					pos3 += bisect0 * strokeSize * outsideFactor;

					// Prevent twisting. This probably usb;t the best way to do it though.
					if(Vector2.Distance(center, pos1) > Vector2.Distance(center, pos2)) {
						Vector2 t = pos1;
						pos1 = pos2;
						pos2 = t;
					}
					if(Vector2.Distance(center, pos0) > Vector2.Distance(center, pos3)) {
						Vector2 t = pos0;
						pos0 = pos3;
						pos3 = t;
					}

					vh.AddUIVertexQuad(SetVbo(new[] { pos0, pos1, pos2, pos3 }, new[] { uv0, uv1, uv2, uv3 }, color));
                }
			}
		}
    }
}