﻿using System.Linq;
using UnityEngine;
using UnityEditor.AnimatedValues;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using UnityEditorInternal;

namespace UnityEditor.UI
{
    /// <summary>
    /// Editor class used to edit UI Sprites.
    /// </summary>

    [CustomEditor(typeof(UIPolygon), true)]
    [CanEditMultipleObjects]
    public class PolygonEditor : GraphicEditor {
		SerializedProperty texture;
		SerializedProperty fill;
		SerializedProperty strokeSize;
		SerializedProperty strokePosition;
		SerializedProperty points;
		private ReorderableList pointsList;


        AnimBool showStroke;

        protected override void OnEnable() {
            base.OnEnable();

			texture = serializedObject.FindProperty("_texture");
			fill = serializedObject.FindProperty("fill");
			strokeSize = serializedObject.FindProperty("strokeSize");
			strokePosition = serializedObject.FindProperty("strokePosition");
			points = serializedObject.FindProperty("_points");
			pointsList = new ReorderableList(serializedObject, points, true, true, true, true);
			pointsList.drawHeaderCallback = (Rect rect) => {  
    			EditorGUI.LabelField(rect, "Points");
			};
			pointsList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
				var element = pointsList.serializedProperty.GetArrayElementAtIndex(index);
    			rect.y += 2;
				EditorGUI.PropertyField(rect, element, GUIContent.none);
    		};

			showStroke = new AnimBool(!fill.boolValue);
			showStroke.valueChanged.AddListener(Repaint);
        }
//
        protected override void OnDisable() {
			showStroke.valueChanged.RemoveListener(Repaint);
        }
//
        public override void OnInspectorGUI()
        {
        	base.OnInspectorGUI();
            serializedObject.Update();

			EditorGUILayout.PropertyField(texture, new GUIContent("Texture"));
			EditorGUILayout.PropertyField(fill, new GUIContent("Fill"));
			showStroke.target = !fill.boolValue;
            if (EditorGUILayout.BeginFadeGroup(showStroke.faded)) {
				EditorGUILayout.PropertyField(strokeSize, new GUIContent("Stroke Size"));
				EditorGUILayout.PropertyField(strokePosition, new GUIContent("Stroke Position"));
				EditorGUILayout.EndFadeGroup();
            }
			pointsList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();
        }
	}
}