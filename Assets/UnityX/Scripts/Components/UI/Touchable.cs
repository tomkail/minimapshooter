﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// To allow an invisible button without having an image or text
/// http://answers.unity3d.com/questions/801928/46-ui-making-a-button-transparent.html#answer-851816
/// </summary>
public class Touchable : Graphic
{
	public override bool Raycast(Vector2 sp, Camera eventCamera)
	{
		//return base.Raycast(sp, eventCamera);
		return true;
	}

	protected override void OnPopulateMesh(VertexHelper vh) {
		vh.Clear();
	}
}