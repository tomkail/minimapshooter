﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public interface ISAnimatedProperty { 
	void StartAnimation();
	void Animate(float lerpValue);
}

public abstract class SAnimatedProperty<T> : ISAnimatedProperty 
{
	public T value {
		get {
			return _value;
		}
		set {
			var currentAnim = SLayoutAnimation.AnimationUnderDefinition();
			if( currentAnim != null ) {
				_animStart = _value;
				currentAnim.AddProperty(this);
			}

			_value = value;

			if( onChangeCallback != null )
				onChangeCallback();
		}
	}

	public bool dirty { get; set; }

	public System.Action onChangeCallback;

	public void StartAnimation()
	{
		_animEnd = _value;
		_value = _animStart;
	}

	public void Animate(float lerpValue)
	{
		value = Lerp(_animStart, _animEnd, lerpValue);
	}

	protected abstract T Lerp(T v0, T v1, float t);

	T _value;

	T _animStart;
	T _animEnd;
}

public class SAnimatedFloatProperty : SAnimatedProperty<float> {
	protected override float Lerp(float v0, float v1, float t) {
		return Mathf.Lerp(v0, v1, t);
	}
}

public class SAnimatedColorProperty : SAnimatedProperty<Color> {
	protected override Color Lerp(Color v0, Color v1, float t) {
		return Color.Lerp(v0, v1, t);
	}
}