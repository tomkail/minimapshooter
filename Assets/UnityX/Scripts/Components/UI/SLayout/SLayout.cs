﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[ExecuteInEditMode]
public class SLayout : MonoBehaviour, ILayoutSelfController, ILayoutGroup {

	public Rect debugRect;

	public event System.Action<SLayout> layoutEvent;

	public void Animate(float duration, System.Action animAction, System.Action completeAction = null)
	{
		Animate(duration, 0.0f, animAction, completeAction);
	}

	public void Animate(float duration, float delay, System.Action animAction, System.Action completeAction = null)
	{
		var newAnim = new SLayoutAnimation(duration, delay, animAction, completeAction);

		if( _animations == null )
			_animations = new List<SLayoutAnimation>();

		_animations.Add(newAnim);
	}

	public Rect parentRect {
		get {
			return parentProperties.rect;
		}
	}

	public SLayoutProperties parentProperties {
		get {
			var parentLayout = this.parent;
			if( parentLayout )
				return parentLayout.properties;
			else
				return CalculateExistingProperties(transform.parent.gameObject);
		}
	}

	public SLayout parent {
		get {
			return transform.parent.GetComponent<SLayout>();
		}
	}

	public SLayoutHierarchyProperties layout {
		get {
			var fullLayout = new SLayoutHierarchyProperties();
			fullLayout.properties = properties;
			fullLayout.targetComponent = this;

			var childLayoutComponents = GetComponentsInChildren<SLayout>();
			foreach(SLayout childLayoutComp in childLayoutComponents) {
				
				if( childLayoutComp == this ) continue;
					
				var childLayout = childLayoutComp.layout;

				if( fullLayout.childrenLayout == null )
					fullLayout.childrenLayout = new List<SLayoutHierarchyProperties>();

				fullLayout.childrenLayout.Add(childLayout);
			}

			return fullLayout;
		}
		set {
			SLayoutHierarchyProperties fullLayout = value;
			Debug.Assert(fullLayout.targetComponent == this, "Layout isn't intended for this component");

			properties = fullLayout.properties;

			if( fullLayout.childrenLayout != null ) {
				foreach(var childLayout in fullLayout.childrenLayout) {
					childLayout.targetComponent.layout = childLayout;
				}
			}
		}
	}

	public SLayoutProperties properties {
		get {
			var p = new SLayoutProperties();
			p.rect = rect;
			p.rotation = rotation;
			p.scale = scale;
			p.groupAlpha = groupAlpha;
			p.textColor = textColor;
			p.imageColor = imageColor;
			return p;
		}
		set {
			x = value.rect.x;
			y = value.rect.y;
			width = value.rect.width;
			height = value.rect.height;
			rotation = value.rotation;
			scale = value.scale;
			groupAlpha = value.groupAlpha;
			textColor = value.textColor;
			imageColor = value.imageColor;
		}
	}
		
	public float x {
		get {
			return _x.value;
		}
		set {
			_x.value = value; 
		}
	}

	public float y {
		get {
			return _y.value;
		}
		set {
			_y.value = value;
		}
	}

	public float width {
		get {
			return _width.value;
		}
		set {
			_width.value = value;
		}
	}

	public float height {
		get {
			return _height.value;
		}
		set {
			_height.value = value;
		}
	}

	public float rotation {
		get {
			return _rotation.value;
		}
		set {
			_rotation.value = value;
		}
	}

	public float scale {
		get {
			return _scale.value;
		}
		set {
			_scale.value = value;
		}
	}

	public float groupAlpha {
		get {
			return _groupAlpha.value;
		}
		set {
			_groupAlpha.value = value;
		}
	}

	public Color textColor {
		get {
			return _textColour.value;
		}
		set {
			_textColour.value = value;
		}
	}

	public Color imageColor {
		get {
			return _imageColour.value;
		}
		set {
			_imageColour.value = value;
		}
	}

	public float imageAlpha {
		get {
			return _imageColour.value.a;
		}
		set {
			var color = _imageColour.value;
			color.a = value;
			_imageColour.value = color;
		}
	}

	public Rect rect {
		get {
			return new Rect(x, y, width, height);
		}
		set {
			x = value.x;
			y = value.y;
			width = value.width;
			height = value.height;
		}
	}
		
	public RectTransform rectTransform {
		get {
			return transform as RectTransform;
		}
	}

	void OnEnable() 
	{
		// Initialise/reset state
		InitialSetupFromUnityProperties();
	}

	void Awake() 
	{
		// Initialise state
		InitialSetupFromUnityProperties();
	}

	void OnLayoutPropertyChanged()
	{
		LayoutRebuilder.MarkLayoutForRebuild (rectTransform);
	}

	void InitialSetupFromUnityProperties()  
	{
		_x = new SAnimatedFloatProperty();
		_y = new SAnimatedFloatProperty();
		_width = new SAnimatedFloatProperty();
		_height = new SAnimatedFloatProperty();
		_rotation = new SAnimatedFloatProperty();
		_scale = new SAnimatedFloatProperty();

		_x.onChangeCallback        = OnLayoutPropertyChanged;
		_y.onChangeCallback        = OnLayoutPropertyChanged;
		_width.onChangeCallback    = OnLayoutPropertyChanged;
		_height.onChangeCallback   = OnLayoutPropertyChanged;
		_rotation.onChangeCallback = OnLayoutPropertyChanged;
		_scale.onChangeCallback    = OnLayoutPropertyChanged;

		_groupAlpha = new SAnimatedFloatProperty();
		_textColour = new SAnimatedColorProperty();
		_imageColour = new SAnimatedColorProperty();

		_groupAlpha.onChangeCallback = () => _groupAlpha.dirty = true;
		_textColour.onChangeCallback = () => _textColour.dirty = true;
		_imageColour.onChangeCallback = () => _imageColour.dirty = true;

		// Pass calculated rect into element layout, which is the new source of truth
		RecalculatePropertiesFromSource();

		debugRect = properties.rect;

		// Ensure RectTransform is set up according to how we want it, using bottom left as anchor
		SetRectTransformFromProperties(resetAnchoring:true);
	}

	public void RecalculatePropertiesFromSource()
	{
		properties = CalculateExistingProperties(gameObject);
	}

	SLayoutProperties CalculateExistingProperties(GameObject obj)
	{
		var properties = new SLayoutProperties();
		properties.rect = CalculateRectTransformLayoutRect(obj.transform as RectTransform);
		properties.rotation = transform.localEulerAngles.z;
		properties.scale = transform.localScale.x;

		var text = obj.GetComponent<Text>();
		if( text )
			properties.textColor = text.color;

		var image = obj.GetComponent<Image>();
		if( image )
			properties.imageColor = image.color;
		else
			properties.imageColor = Color.white;

		var canvasGroup = obj.GetComponent<CanvasGroup>();
		if( canvasGroup )
			properties.groupAlpha = canvasGroup.alpha;
		else
			properties.groupAlpha = 1.0f;

		return properties;
	}

	Rect CalculateRectTransformLayoutRect(RectTransform rt) 
	{
		var parentRectTransform = rt.parent as RectTransform;
		if( parentRectTransform ) {
			var parentSize = parentRectTransform.rect.size;

			// Calculate rect in absolute terms based on parent's size
			float left = rt.anchorMin.x * parentSize.x + rt.offsetMin.x;
			float right = rt.anchorMax.x * parentSize.x + rt.offsetMax.x;
			float bottom = rt.anchorMin.y * parentSize.y + rt.offsetMin.y;
			float top = rt.anchorMax.y * parentSize.y + rt.offsetMax.y;

			// Above sizes are in Unity's coordinate space, where 0,0 is bottom left,
			// and y axis goes positive as it goes upwards
			// But our rects use top left as 0,0, and goes downwards, so needs to be flipped
			return new Rect(left, parentSize.y-top, right-left, top-bottom);
		} 

		// Without a parent, we can't actually know what the exact size of this
		// RectTransform, unless the anchors are close together, which we'll have to assume.
		else {
			var unityPosition = rt.anchoredPosition;
			var size = rt.sizeDelta;

			// We're really just guessing, since this method could be called on a parent RectTransform
			// which doesn't have a SLayoutComponent at all. But should try to assume that it's intended
			// to have one in future.
			return new Rect(unityPosition.x-0.5f*size.x, -unityPosition.y-0.5f*size.y, size.x, size.y);
		}

	}

	void SetRectTransformFromProperties(bool resetAnchoring=false)
	{
		// Top left anchor
		if( resetAnchoring ) {
			rectTransform.anchorMin = rectTransform.anchorMax = new Vector2(0.0f, 1.0f);
			rectTransform.pivot = new Vector2(0.5f, 0.5f);
		}
			
		var centreLayoutSpace = rect.center;
		var centreUnitySpace = new Vector2(centreLayoutSpace.x, -centreLayoutSpace.y);

		rectTransform.anchoredPosition = centreUnitySpace;
		rectTransform.sizeDelta = properties.rect.size;
		rectTransform.localScale = properties.scale * Vector3.one;
		rectTransform.localEulerAngles = new Vector3(0,0,properties.rotation);
	}
		
	void Update() {

		// When UI is dragged in editor, update the element layout to reflect this
		#if UNITY_EDITOR
		if( !Application.isPlaying ) {
			if( transform.hasChanged ) {
				InitialSetupFromUnityProperties();
				transform.hasChanged = false;
			}
		}
		#endif

		if( _animations != null ) {

			var completeAnims = new List<SLayoutAnimation>();

			// Don't foreach, since a new animation could be added to the list
			// as part of the Update (due to a completion callback)
			for(int i=0; i<_animations.Count; ++i) {
				var anim = _animations[i];

				anim.Update();

				if( anim.isComplete )
					completeAnims.Add(anim);
			}

			foreach(var completeAnim in completeAnims)
				_animations.Remove(completeAnim);
		}
	}

	void LateUpdate()
	{
		if( _textColour.dirty ) {
			var text = GetComponent<Text>();
			if( text )
				text.color = _textColour.value;

			_textColour.dirty = false;
		}

		if( _imageColour.dirty ) {
			var image = GetComponent<Image>();
			if( image )
				image.color = _imageColour.value;

			_imageColour.dirty = false;
		}

		if( _groupAlpha.dirty ) {
			var canvasGroup = GetComponent<CanvasGroup>();
			if( canvasGroup )
				canvasGroup.alpha = _groupAlpha.value;
		}

		#if UNITY_EDITOR
		debugRect = properties.rect;
		#endif
	}

	public void SetLayoutHorizontal ()
	{
		if( layoutEvent != null )
			layoutEvent(this);
		
		SetRectTransformFromProperties();
	}

	public void SetLayoutVertical ()
	{
		// We don't do anything here
	}

	SAnimatedFloatProperty _x;
	SAnimatedFloatProperty _y;
	SAnimatedFloatProperty _width;
	SAnimatedFloatProperty _height;
	SAnimatedFloatProperty _rotation;
	SAnimatedFloatProperty _scale;
									 
	SAnimatedFloatProperty _groupAlpha;
	SAnimatedColorProperty _textColour;
	SAnimatedColorProperty _imageColour;

	List<SLayoutAnimation> _animations;
}
