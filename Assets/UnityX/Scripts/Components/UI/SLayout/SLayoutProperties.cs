﻿using UnityEngine;

public struct SLayoutProperties {
	public Rect rect;
	public float rotation;
	public float scale;
	public float groupAlpha;
	public Color textColor;
	public Color imageColor;
}
