﻿using UnityEngine;
using System.Collections.Generic;

public class SLayoutAnimation {

	public SLayoutAnimation(float duration, float delay, System.Action animAction, System.Action completionAction)
	{
		if( _animationsBeingDefined == null )
			_animationsBeingDefined = new List<SLayoutAnimation>();

		_animationsBeingDefined.Add(this);

		_time = 0.0f;
		_duration = duration;
		_delay = delay;
		_completionAction = completionAction;
		_properties = new List<ISAnimatedProperty>();

		animAction();

		foreach(var property in _properties)
			property.StartAnimation();

		_animationsBeingDefined.RemoveAt(_animationsBeingDefined.Count-1);

		// Zero duration?
		if( timeIsUp )
			Complete();
	}

	public static SLayoutAnimation AnimationUnderDefinition()
	{
		if( _animationsBeingDefined != null && _animationsBeingDefined.Count > 0 )
			return _animationsBeingDefined[_animationsBeingDefined.Count-1];
		else
			return null;
	}

	public bool isComplete {
		get {
			return _completed;
		}
	}

	bool timeIsUp {
		get {
			return _time >= _delay + _duration;
		}
	}

	public void AddProperty(ISAnimatedProperty property)
	{
		_properties.Add(property);
	}

	public void Update() 
	{
		_time += Time.deltaTime;

		if( isComplete )
			return;
		
		float lerpValue = 0.0f;
		if( _time > _delay ) {
			lerpValue =  Mathf.Clamp01((_time-_delay)/_duration);

			// TODO: Allow different curves?
			lerpValue = Mathf.SmoothStep(0.0f, 1.0f, lerpValue);
		}

		foreach(var property in _properties)
			property.Animate(lerpValue);

		if( timeIsUp )
			Complete();
	}

	void Complete() 
	{
		_completed = true;

		if( _completionAction != null )
			_completionAction();
	}

	List<ISAnimatedProperty> _properties;

	float _time;
	float _duration;
	float _delay;
	System.Action _completionAction;
	bool _completed;

	static List<SLayoutAnimation> _animationsBeingDefined;
}