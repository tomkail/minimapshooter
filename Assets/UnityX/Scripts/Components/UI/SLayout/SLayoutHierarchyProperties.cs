﻿using UnityEngine;
using System.Collections.Generic;

public class SLayoutHierarchyProperties {
	public List<SLayoutHierarchyProperties> childrenLayout;

	// Does the layout specify a top level element layout?
	public SLayout targetComponent;
	public SLayoutProperties properties;
}
