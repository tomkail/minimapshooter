﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class InputPoint {
	public string name;
	internal float tapTime = 0.2f;

	public InputPointState state;

	public float startTime;
	public float activeTime;

	public Vector2 position;
	public Vector2 lastPosition;
	public Vector2 deltaPosition;
	
	public Vector2 viewportPosition;
	public Vector2 lastViewportPosition;
	public Vector2 deltaViewportPosition;
	
	private float lerpedMovement;
	
	public bool isOnScreen = true;

	public delegate void OnStartEvent(InputPoint inputPoint);
	public event OnStartEvent OnStart;
	public delegate void OnEndEvent(InputPoint inputPoint);
	public event OnEndEvent OnEnd;

	public delegate void OnTapEvent(InputPoint inputPoint);
	public event OnTapEvent OnTap;

	

	public InputPoint(Vector2 _position){
		name = "Input Point";
		position = lastPosition = _position;
		viewportPosition = lastViewportPosition = new Vector2(_position.x/Screen.width, position.y/Screen.height);
		Start();
	}

	public virtual void Start(){
		startTime = Time.time;
		state = InputPointState.Started;
		if(OnStart != null){
			OnStart(this);
		}
	}

	public virtual void UpdateLifeTime(float _deltaTime){
		activeTime += _deltaTime;
	}

	public virtual void UpdatePosition(Vector2 _screenPosition){
		isOnScreen = new Rect(0,0,Screen.width,Screen.height).Contains(_screenPosition);
		
		SetScreenPositionProperties(_screenPosition);
		SetViewportPositionProperties(_screenPosition);
		UpdateDeltaMovement();
	}
	
	public virtual void UpdateState(){
		if(state != InputPointState.Pinch1 && state != InputPointState.Pinch2){
			if(state == InputPointState.Started && activeTime > tapTime){
				state = InputPointState.Stationary;
			}

			if(lerpedMovement > 1f){
				state = InputPointState.Moving;
			} else {
				lerpedMovement = 0;
				if(state != InputPointState.Started){
					state = InputPointState.Stationary;
				}
			}
		}
	}

	public virtual void ResetInput(){
		
	}

	public virtual void End(){
		if(state == InputPointState.Started && activeTime < tapTime){
			Tap();
		}
		if(OnEnd != null){
			OnEnd(this);
		}
	}

	public virtual void Tap() {
		state = InputPointState.Tap;
		if(OnTap != null){
			OnTap(this);
		}
	}
	
	private void SetWorldPositionProperties (Vector2 _screenPosition) {
		lastPosition = position;
		position = _screenPosition;
		deltaPosition = position-lastPosition;
	}
	
	private void SetViewportPositionProperties (Vector2 _screenPosition) {
		lastViewportPosition = viewportPosition;
		viewportPosition.x = _screenPosition.x/Screen.width;
		viewportPosition.y = _screenPosition.y/Screen.height;
		deltaViewportPosition = viewportPosition-lastViewportPosition;
	}
	
	private void SetScreenPositionProperties (Vector2 _worldPosition) {
		lastPosition = position;
		position = _worldPosition;
		deltaPosition = position-lastPosition;
	}
	
	private void UpdateDeltaMovement () {
		lerpedMovement = Mathf.Lerp(lerpedMovement, deltaPosition.magnitude, Time.deltaTime*40);
		lerpedMovement += deltaPosition.magnitude;
	}

	public override string ToString () {
		return string.Format ("[InputPoint] State {0} Position {1} Delta Position {2} Active Time {3}", state, position, deltaPosition, activeTime);
	}
}