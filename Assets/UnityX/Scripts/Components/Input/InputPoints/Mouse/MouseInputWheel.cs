﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MouseInputWheel {
	public bool scrolling;
	public float totalScroll;
	public float deltaScroll;

	public delegate void OnMouseScrollEvent(float deltaScroll);
	public event OnMouseScrollEvent OnMouseScroll;

	public void UpdatePosition(){
		if(Input.mousePosition.x > 0 && Input.mousePosition.x < Screen.width && Input.mousePosition.y > 0 && Input.mousePosition.y < Screen.height)
		deltaScroll = Input.GetAxis("Mouse ScrollWheel");
		if (deltaScroll != 0){
			scrolling = true;
			totalScroll += deltaScroll;
			if(OnMouseScroll != null){
				OnMouseScroll(deltaScroll);
			}
		} else {
			scrolling = false;
		}
	}

	public void ResetInput(){
		scrolling = false;
		deltaScroll = 0;
	}
}