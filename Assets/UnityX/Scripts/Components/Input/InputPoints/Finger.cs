﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Finger : InputPoint {
	//The ID of the touch, as defined by Unity's Touch class. This never changes.
	public int fingerId;
	//The index of the touch, where index 0 is the active touch and index 1 is the second touch
	public int fingerArrayIndex;
	//The index of the touch, where index 0 is the active touch and index 1 is the second touch
	//public int order;

	public Finger(Touch _touch) : base (_touch.position) {
		fingerId = _touch.fingerId;
		name = "Finger "+fingerId;
	}

	public override void UpdateLifeTime(float _deltaTime){
		base.UpdateLifeTime(_deltaTime);		
	}

	public override void UpdatePosition(Vector2 _position){
		base.UpdatePosition(_position);		
	}

	public override void UpdateState(){
		base.UpdateState();		
	}

	public override void End(){
		base.End();
	}
	
	public override string ToString () {
		return string.Format ("[Finger] Name {0} ID {1} State {2} Position {3}", name, fingerId, state, position);
	}
}
