using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T> {
	private static T _Instance;
	public static T Instance {
		get {
			if(_Instance == null) {
				_Instance = Object.FindObjectOfType<T>();
			}
			return _Instance;
		}
	}

	public static bool IsInitialized {
		get {
			return Instance != null;
		}
	}
}
