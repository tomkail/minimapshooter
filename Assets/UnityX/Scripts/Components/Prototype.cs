﻿using UnityEngine;

/// <summary>
/// Convenience way to create objects in the hierarchy that are a bit like prefabs, except that they
/// aren't. They exist in the scene, immediately deactivate themselves, then provide an easy way to
/// instantiate a copy in the same place in the hierarchy.
/// </summary>
public class Prototype : MonoBehaviour {

	void Start() 
	{
		this.gameObject.SetActive(false);
	}

	public T Instantiate<T>()  where T : Component
	{
		var instance = ObjectX.InstantiateAsChild(this);
		instance.gameObject.SetActive(true);
		Destroy(instance.GetComponent<Prototype>());
		return instance.GetComponent<T>();
	}
}
