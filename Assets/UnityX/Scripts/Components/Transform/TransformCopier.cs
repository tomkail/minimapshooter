﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TransformCopier : MonoBehaviour {
	
//	[SerializeField]
//	private bool inPlayMode = true;
//	[SerializeField]
//	private bool inEditMode = true;

	/// <summary>
	/// The target to mirror.
	/// </summary>
	public Transform target;
	public bool position = true;
	public bool rotation = true;
	public bool useFixedUpdate = false;

	void Update () {
		if(useFixedUpdate || target == null) 
			return;
		Apply();
	}

	void FixedUpdate () {
		if(!useFixedUpdate || target == null) 
			return;
		Apply();
	}

	void Apply () {
		if(position)
			transform.position = target.position;
		if(rotation)
			transform.rotation = target.rotation;
	}
}
