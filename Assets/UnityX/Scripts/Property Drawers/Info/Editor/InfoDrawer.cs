﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(InfoAttribute))]
public class InfoDrawer : BasePropertyDrawer<InfoAttribute> {
	private int helpBoxHeight = 38;
	
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		return base.GetPropertyHeight (property, label) + helpBoxHeight;
	}

	protected override bool IsSupported (SerializedProperty property) {
		return true;
	}

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		EditorGUI.HelpBox(position.CopyWithHeight(helpBoxHeight), attribute.info, MessageType.Info);
		EditorGUI.PropertyField(position.CopyWithY(position.y + helpBoxHeight).CopyWithHeight(position.height-helpBoxHeight), property, label);
    }
}