using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof(FolderPathAttribute))]
class FolderPathDrawer : BasePropertyDrawer<FolderPathAttribute> {
	
	// Draw the property inside the given rect
	public override void OnGUI (Rect position , SerializedProperty property, GUIContent label) {

		if (!IsSupported(property)) {
			DrawNotSupportedGUI(position, property, label);
			return;
		}

		var path = property.stringValue;
		var contentRect = EditorGUI.PrefixLabel( position, label );
		var textRect = contentRect;
		var buttonRect = contentRect;
		
		textRect.width -= 22;
		buttonRect.width = 22;
		buttonRect.x = textRect.xMax;
		
		path = EditorGUI.TextField( textRect, path );
		if(GUI.Button( buttonRect, "..." )) {
			string relativePath = "";
			if(attribute.relativeTo == FolderPathAttribute.RelativeTo.Assets) {
				relativePath = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf("/Assets"));
			} else if(attribute.relativeTo == FolderPathAttribute.RelativeTo.Project) {
				relativePath = Application.dataPath;
			}
			path = relativePath+path;
			path = EditorUtility.OpenFolderPanel( "Select Folder", path, "" );
			
			path = path.Split(new string[]{relativePath}, System.StringSplitOptions.RemoveEmptyEntries)[0];
			if(path.IndexOf('/') == 0) path = path.Remove(0,1);
		}
		property.stringValue = path;
	}

	protected override bool IsSupported(SerializedProperty property) {
		return property.propertyType == SerializedPropertyType.String;
	}
}