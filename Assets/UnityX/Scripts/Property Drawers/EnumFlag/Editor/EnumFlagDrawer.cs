using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(EnumFlagAttribute))]
public class EnumFlagDrawer : BasePropertyDrawer<EnumFlagAttribute> {
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {

		if (!IsSupported(property)) {
			DrawNotSupportedGUI(position, property, label);
			return;
		}

		Enum targetEnum = property.GetBaseProperty<Enum>();

		EditorGUI.BeginProperty(position, label, property);
		Enum enumNew = EditorGUI.EnumMaskField(position, ObjectNames.NicifyVariableName(property.name), targetEnum);
		property.intValue = (int) Convert.ChangeType(enumNew, targetEnum.GetType());
		EditorGUI.EndProperty();
	}

	static T GetBaseProperty<T>(SerializedProperty prop) {
		// Separate the steps it takes to get to this property
		string[] separatedPaths = prop.propertyPath.Split('.');
		
		// Go down to the root of this serialized property
		System.Object reflectionTarget = prop.serializedObject.targetObject as object;
		// Walk down the path to get the target object
		foreach (var path in separatedPaths)
		{
			FieldInfo fieldInfo = reflectionTarget.GetType().GetField(path);
			reflectionTarget = fieldInfo.GetValue(reflectionTarget);
		}
		return (T) reflectionTarget;
	}

	protected override bool IsSupported(SerializedProperty property) {
		Enum targetEnum = property.GetBaseProperty<Enum>();
		return targetEnum != null;
	}
}