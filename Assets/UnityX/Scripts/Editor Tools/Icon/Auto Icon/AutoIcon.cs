﻿using UnityEngine;
using System.Collections;
using UnityX.Editor.Icon;

public class AutoIcon : MonoBehaviour {
	public enum IconType {
		Label,
		Dot,
		Custom
	}

	public bool customForSelected;
	public IconProperties defaultIconProperties;
	public IconProperties selectedIconProperties;

	[System.Serializable]
	public class IconProperties {
		public IconType iconType;
		public Icon icon;
		public LabelIcon labelIcon;
		public Texture2D texture;
	}
}
