﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;
using UnityX.Editor.Icon;

[CustomEditor(typeof(AutoIcon))]
[CanEditMultipleObjects]
public class AutoIconEditor : BaseEditor<AutoIcon> {

	public override void OnEnable() {
		base.OnEnable();
		RefreshEnabled();
    }

	void OnDisable() {
		SetFromProperties(data.defaultIconProperties);
	}

	void RefreshEnabled () {
		foreach (var d in datas) {
			if (d.customForSelected)
				SetFromProperties(d.selectedIconProperties);
			else
				SetFromProperties(d.defaultIconProperties);
		}
		// if(serializedObject.FindProperty("defaultIconProperties").boolValue)
		// 	SetFromProperties(data.selectedIconProperties);
		// else
		// 	SetFromProperties(data.defaultIconProperties);
	}

	public override void OnInspectorGUI () {
		serializedObject.Update();
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField(serializedObject.FindProperty("customForSelected"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("defaultIconProperties"));
		if(data.customForSelected) {
			EditorGUILayout.PropertyField(serializedObject.FindProperty("selectedIconProperties"));
		}
		serializedObject.ApplyModifiedProperties();
		if (EditorGUI.EndChangeCheck()) {
			RefreshEnabled();
		}
	}

	void DrawProperties (AutoIcon.IconProperties iconProperties) {
//		iconProperties
	}

	void SetFromProperties (AutoIcon.IconProperties iconProperties) {
		if(data == null) return;
		if(iconProperties.iconType == AutoIcon.IconType.Dot) {
			IconManager.SetIcon(data.gameObject, iconProperties.icon);
		} else if(iconProperties.iconType == AutoIcon.IconType.Label) {
			IconManager.SetIcon(data.gameObject, iconProperties.labelIcon);
		} else if(iconProperties.iconType == AutoIcon.IconType.Custom) {
			IconManager.SetIcon(data.gameObject, iconProperties.texture);
		}
	}
}


[CustomPropertyDrawer(typeof(AutoIcon.IconProperties))]
public class AutoIconPropertiesDrawer : PropertyDrawer {
	
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		return EditorGUIUtility.singleLineHeight * 3 + EditorGUIUtility.standardVerticalSpacing;
	}
	
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		Rect r = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
		EditorGUI.Foldout(r, true, label);

		EditorGUI.indentLevel++;

		r.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
		EditorGUI.PropertyField(r, property.FindPropertyRelative("iconType"));

		r.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
		AutoIcon.IconType type = (AutoIcon.IconType)property.FindPropertyRelative("iconType").enumValueIndex;
		if(type == AutoIcon.IconType.Dot) {
			EditorGUI.PropertyField(r, property.FindPropertyRelative("icon"));
		} else if(type == AutoIcon.IconType.Label) {
			EditorGUI.PropertyField(r, property.FindPropertyRelative("labelIcon"));
		} else if(type == AutoIcon.IconType.Custom) {
			EditorGUI.PropertyField(r, property.FindPropertyRelative("texture"));
		}
		EditorGUI.indentLevel--;
    }
}
