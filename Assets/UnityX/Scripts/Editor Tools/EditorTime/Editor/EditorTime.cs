﻿using UnityEngine;
using UnityEditor;
using System.Collections;

/// <summary>
/// Keeps track of time when in the editor.
/// </summary>
[InitializeOnLoad]
public static class EditorTime {

	public static float time {
		get {
			return Time.realtimeSinceStartup;
		}
	}
	
	public static float deltaTime {get; private set;}
	public static float frameCount {get; private set;}
	
	private static float lastTime;
	
	static EditorTime() {
		lastTime = time;
		deltaTime = 0;
		EditorApplication.update += Update;
	}
	
	private static void Update () {
		deltaTime = time - lastTime;
		frameCount++;
		lastTime = Time.realtimeSinceStartup;
		Shader.SetGlobalFloat("_EditorTime", EditorTime.time);
	}
}
