﻿Shader "UnityX/Reveal/Radial/Sprite Reveal" {
	Properties {
		[PerRendererData] _MainTex ("Texture", 2D) = "white" {}
		_BackgroundTex ("Background Texture", 2D) = "white" {}
		_Cutoff("Cutoff", Range(0.0,1.0)) = 0
	}

	Category {
		Cull off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off
		Fog { Mode Off }
 
	 	SubShader {
			Tags {"Queue"="Transparent" "RenderType"="Transparent"}
			CGPROGRAM
			#pragma surface surf Unlit alpha
			half4 LightingUnlit (SurfaceOutput s, half3 lightDir, half atten) {
				half4 c;
				c.rgb = s.Albedo;
				c.a = s.Alpha;
				return c;
			}

			sampler2D _MainTex;
			sampler2D _BackgroundTex;
		   	fixed _Cutoff;
			
			float angleBetween(float2 a, float2 b) {
				return atan2(-(b.y - a.y), b.x - a.x) + (0.01745329 * 90);
			}
			
			struct Input {
				float2 uv_MainTex;
				float2 uv2_BackgroundTex;
				float4 color : Color;
			};
			
			void surf (Input IN, inout SurfaceOutput o) {
				float PI = 3.14159265358979323846264;
				
				half4 tex = tex2D (_MainTex, IN.uv_MainTex);
				half4 backgroundTex = tex2D (_BackgroundTex, IN.uv_MainTex);
				

                float angle = degrees(angleBetween(float2(0.5,0.5), IN.uv2_BackgroundTex.xy));
                if(angle < 0) angle +=360;
                if(angle > 360) angle -=360;
                
//                o.Albedo = lerp(float3(0,0,0), float3(1,1,1), (angle/360));
//                o.Alpha = 1;
				angle /= 360;
				o.Albedo = 0.5*tex.rgb;
				o.Albedo = angle > _Cutoff ? 0.5*backgroundTex.rgb : 0.5*tex.rgb;
                o.Alpha = angle > _Cutoff ? IN.color.a*backgroundTex.a : IN.color.a*tex.a;
                //o.Alpha = (angle+3)/6;
			}
			ENDCG
		}
	}
}