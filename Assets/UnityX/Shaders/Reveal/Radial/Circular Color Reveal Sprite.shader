﻿Shader "UnityX/Reveal/Radial/Color Reveal" {
	Properties {
		[PerRendererData] _MainTex ("Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_BackgroundColor ("Background Tint", Color) = (0,0,0,1)
		_Offset("Offset", Range(0.0,1.0)) = 0
		_Cutoff("Cutoff", Range(0.0,1.0)) = 0
	}

	Category {
		ZWrite Off
		Cull off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off
		Fog { Mode Off }
 
	 	SubShader {
			Tags {"Queue"="Transparent" "RenderType"="Transparent"}
			Pass {

				CGPROGRAM
				#pragma vertex vert
	            #pragma fragment frag

				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _MainTex_ST;
				fixed4 _Color;
				fixed4 _BackgroundColor;
			   	fixed _Cutoff;
			   	fixed _Offset;

				float angleBetween(float2 a, float2 b) {
					return atan2(-(b.y - a.y), b.x - a.x) + (0.01745329 * 90);
				}
				
				struct appdata
	            {
	                float4 vertex : POSITION;
	                float2 uv : TEXCOORD0;
	                float4 color : COLOR;
	            };

	            struct v2f
	            {
	                float2 uv : TEXCOORD0;
	                float4 vertex : SV_POSITION;
	                float4 color : COLOR;
	            };
				
				v2f vert (appdata v)
	            {
	                v2f o;
	                o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
	                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
	                o.color = v.color;
	                return o;
	            }
	            
	            fixed4 frag (v2f i) : SV_Target
	            {
	                fixed4 col = fixed4(0,0,0,0);
	                // sample the texture
	                half4 tex = tex2D (_MainTex, i.uv);

	                float angle = degrees(angleBetween(float2(0.5,0.5), i.uv.xy));
	                if(angle < 0) angle +=360;
	                if(angle > 360) angle -=360;
	
					angle /= 360;
					angle += 1-_Offset;
					angle = fmod(angle, 1);
	
					col.rgb = tex.rgb * i.color;
					col.rgb *= angle > _Cutoff ? _BackgroundColor : _Color;
					col.a = 1;
	                col.a *= i.color.a*tex.a;
	                col.a *= angle > _Cutoff ? _BackgroundColor.a : _Color.a;

	                return col;
	            }
				ENDCG
			}
		}
	}
}