﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabDatabase : MonoSingleton<PrefabDatabase> {
	public PlayerAvatar avatar;
	public PlayerUIController playerUI;

	public ParticleSystem bulletHitParticles;
	public ParticleSystem playerHitParticles;
	public BulletTracer bulletTracer;
}
