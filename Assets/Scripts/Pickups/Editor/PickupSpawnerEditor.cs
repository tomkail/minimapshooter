﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PickupSpawner))]
public class PickupSpawnerEditor : BaseEditor<PickupSpawner> {

	public override void OnEnable () {
		base.OnEnable ();
		UpdateName();
	}

	public override void OnInspectorGUI () {
		base.OnInspectorGUI ();
		UpdateName();
	}

	void UpdateName () {
		string name = "Spawner: ";
		if(data.pickupPrefab != null) {
			name += data.pickupPrefab.gameObject.name;
		} else {
			name += "None";
		}

		if(data.gameObject.name != name) {
			data.gameObject.name = name;
		}
	}
}
