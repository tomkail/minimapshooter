using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {
	public AudioClip pickupSFX;
	public bool requiresAction = false;

	public delegate void OnPickupEvent();
	public event OnPickupEvent OnPickup;

	/*
	protected virtual void OnTriggerEnter2D (Collider2D collider) {
		if(collider.CompareTag("Player")) {
			PlayerAvatar player = collider.GetComponent<PlayerAvatar>();
			OnPickup(player);
		}
	}
	*/

	public virtual void DoPickup (PlayerAvatar player) {
		if(OnPickup!= null) OnPickup();
		Destroy(gameObject);
	}
}