using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour {
	[DontAllowSceneObjectsAttribute]
	public Pickup pickupPrefab;
	private Pickup pickup;

	public float intervalTime = 5f;
	private Timer intervalTimer;

	public SpriteRenderer spriteRenderer;
	private Material respawnMaterial;

	void Start () {
		Spawn();
		intervalTimer = new Timer();
		intervalTimer.OnComplete += CompleteTimer;
		respawnMaterial = new Material(spriteRenderer.material);
		spriteRenderer.material = respawnMaterial;
	}

	void OnPickup () {
		spriteRenderer.gameObject.SetActive(true);
		pickup = null;
		intervalTimer.StopAndReset();
		intervalTimer.Set(intervalTime);
		intervalTimer.Start();
	}

	void CompleteTimer () {
		Spawn();
	}

	void Update () {
		if(intervalTimer.state == Timer.State.Playing) {
			respawnMaterial.SetFloat("_Cutoff", intervalTimer.GetNormalizedTime());
			intervalTimer.Update();
		}
	}

	void Spawn () {
		spriteRenderer.gameObject.SetActive(false);
		pickup = Object.Instantiate<Pickup>(pickupPrefab, transform);
		pickup.transform.localPosition = Vector3.zero;
		pickup.OnPickup += OnPickup;
	}
}