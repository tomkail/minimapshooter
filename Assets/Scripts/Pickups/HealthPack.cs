﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : Pickup {
	public float health = 100;
	public override void DoPickup (PlayerAvatar player) {
		player.healthModule.Heal(health);
		base.DoPickup(player);
	}
}
