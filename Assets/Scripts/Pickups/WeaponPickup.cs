using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : Pickup {

	public Gun weapon;

	public override void DoPickup (PlayerAvatar player) {
		base.DoPickup (player);
		player.weaponModule.AddWeapon(Object.Instantiate<Gun>(weapon), true);
	}
}