﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LaserSight : WeaponAttachment {

	public LineRenderer lineRenderer;

	private MaterialPropertyBlock block;
	[Range(0f,1f)]
	public float alpha = 1;

	void OnEnable () {
		lineRenderer.enabled = false;
	}

	void Start () {
		block = new MaterialPropertyBlock();
		lineRenderer.GetPropertyBlock(block);
	}

	void Update () {
		block.SetColor("_TintColor", Color.white.WithAlpha(alpha));

		Vector3 hitPoint = Gun.BulletModule.GetHitPoint(transform.position, transform.up, gun.player.gameObject);
		lineRenderer.numPositions = 2;
		lineRenderer.SetPositions(new Vector3[]{transform.position, hitPoint});

		lineRenderer.SetPropertyBlock(block);

		GradientColorKey[] colors = new GradientColorKey[1];
		GradientAlphaKey[] alphas = new GradientAlphaKey[1];
		for (int i = 0; i < 1; i++) {
//			var key = gun.damageOverDistanceMultiplier.keys [i];
			float normalizedDistance = 0;
//			float
			colors[i] = new GradientColorKey (gun.player.player.team.color, normalizedDistance);
			alphas[i] = new GradientAlphaKey (1, normalizedDistance);
		}
		lineRenderer.colorGradient.colorKeys = colors;
		lineRenderer.colorGradient.alphaKeys = alphas;
//		lineRenderer.colorGradient = new Gradient()
//		Debug.Log(lineRenderer.colorGradient.colorKeys.Length+" "+colors.Length);
	}

	public override void OnDraw () {
		base.OnDraw ();
		lineRenderer.enabled = true;
	}

	public override void OnPutAway () {
		base.OnPutAway ();
		lineRenderer.enabled = false;
	}

//	void OnEnable () {
//		lineRenderer.enabled = true;
//	}
//
//	void OnDisable () {
//		lineRenderer.enabled = false;
//	}
}