﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class GameController : MonoSingleton<GameController> {

	public Game game;
	public List<Team> teams;
	public Canvas canvas;

	public void Start () {
		game = new Game();
		game.Create();
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.Return)) {
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);
		}
	}
}

[System.Serializable]
public class Game {
	public float timer;
	public Level level;
	public List<Player> players = new List<Player>();

	public Game () {}

	public void Create () {
		level = Object.FindObjectOfType<Level>();
//		for (int i = 0; i < 8; i++) {
//			var device = InputManager.Devices [0];
//			players.Add (new Player ("Player "+(i+1), InputManager.Devices [0], GameController.Instance.teams[i]));
//		}
		for (int i = 0; i < InputManager.Devices.Count; i++) {
			players.Add (new Player ("Player "+(i+1), i, InputManager.Devices [i], GameController.Instance.teams[i]));
		}
		foreach(var player in players) {
			player.CreateNewAvatar();
		}
	}

	void Update () {
		timer += Time.deltaTime;
		foreach(var player in players) {
			player.Update();
		}
	}
}

[System.Serializable]
public class Team {
	public Color color;
}

[System.Serializable]
public class Player {
	public string name;
	public int index;
	public InputDevice device;
	public Team team;
	public PlayerAvatar avatar;
	public PlayerUIController diegeticUI;

	public Player (string name, int index, InputDevice device, Team team) {
		this.name = name;
		this.index = index;
		this.device = device;
		this.team = team;
	}

	public void Update () {
		if(avatar == null) {
			CreateNewAvatar();
		}
	}

	public void CreateNewAvatar () {
		Transform spawnPoint = GameController.Instance.game.level.GetBestSpawnPointForPlayer(this);
		avatar = Object.Instantiate<PlayerAvatar>(PrefabDatabase.Instance.avatar, spawnPoint.position, spawnPoint.rotation);
		avatar.player = this;
		avatar.OnDie += OnAvatarDie;


		avatar.weaponModule.AddWeapon(Object.Instantiate<Gun>(WeaponDatabase.Instance.pistol), true);
		avatar.weaponModule.AddWeapon(Object.Instantiate<Gun>(WeaponDatabase.Instance.bounceBulletGun), true);
//		avatar.weaponModule.AddWeapon(Object.Instantiate<Gun>(WeaponDatabase.Instance.grenadeLauncher), true);

		/*
		List<Gun> primaryGuns = new List<Gun>() {
			WeaponDatabase.Instance.semiAuto,
			WeaponDatabase.Instance.machineGun,
			WeaponDatabase.Instance.shotgun,
			WeaponDatabase.Instance.sniperRifle,
		};
		*/
//		Object.Instantiate<Gun>(WeaponDatabase.Instance.pistol);
//		Object.Instantiate<Gun>(primaryGuns.Random(), avatar.transform, false);
//		Object.Instantiate<Gun>(WeaponDatabase.Instance.semiAuto, avatar.transform, false);
//		Object.Instantiate<Gun>(WeaponDatabase.Instance.lmg, avatar.transform, false);
//		Object.Instantiate<Gun>(WeaponDatabase.Instance.shotgun, avatar.transform, false);
//		Object.Instantiate<Gun>(WeaponDatabase.Instance.sniperRifle, avatar.transform, false);
//		Object.Instantiate<Gun>(WeaponDatabase.Instance.pistol, avatar.transform, false);

		diegeticUI = Object.Instantiate<PlayerUIController>(PrefabDatabase.Instance.playerUI, GameController.Instance.canvas.transform, false);
		diegeticUI.player = this;
	}

	void OnAvatarDie (PlayerAvatar avatar) {
		Object.Destroy(avatar.gameObject);
		Object.Destroy(diegeticUI.gameObject);
		CreateNewAvatar();
	}
}