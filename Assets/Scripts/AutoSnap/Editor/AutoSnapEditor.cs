﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AutoSnap))]
public class AutoSnapEditor : BaseEditor<AutoSnap> {

	public override void OnEnable () {
		base.OnEnable ();
		Tools.hidden = true;
	}


	public void OnDisable () {
		Tools.hidden = false;
	}

	public override void OnInspectorGUI () {
		base.OnInspectorGUI ();
	}

	protected virtual void OnSceneGUI() {
//		var rectExample = (RectExample)target;
		Rect rect = RectX.Create(data.transform.position, data.transform.localScale, new Vector2(0,0));
		rect = RectUtils.ResizeRect(
			rect,
            Handles.CubeCap,
            Color.green,
            Color.yellow,
            HandleUtility.GetHandleSize(Vector3.zero) * .1f,
            1);

		data.transform.position = rect.TopLeft();
		data.transform.localScale = new Vector3(rect.size.x, rect.size.y, 1);
//        rectExample.MyRect = rect;

//		Vector3 targetScale = data.transform.localScale;
//		targetScale.x = MathX.RoundToInt(data.transform.localScale.x);
//		targetScale.y = MathX.RoundToInt(data.transform.localScale.y);
//		targetScale.z = MathX.RoundToInt(data.transform.localScale.z);
//		if(data.transform.localScale != targetScale) {
//			data.transform.localScale = targetScale;
//		}

//		Vector3 targetRotation = data.transform.localRotation;
//		targetScale.x = MathX.RoundToNearest(data.transform.localScale.x, data.scaleSnap);
//		targetScale.y = MathX.RoundToNearest(data.transform.localScale.y, data.scaleSnap);
//		targetScale.z = MathX.RoundToNearest(data.transform.localScale.z, data.scaleSnap);
//		if(data.transform.localScale != targetScale) {
//			data.transform.localScale = targetScale;
//		}


//		Vector3 targetPosition = data.transform.localPosition;
//		targetPosition.x = MathX.RoundToNearest(data.transform.localPosition.x, 0.5f)  0.5f;
//		targetPosition.y = MathX.RoundToInt(data.transform.localPosition.y);
//		targetPosition.z = MathX.RoundToInt(data.transform.localPosition.z);

//		Debug.Log((targetScale.x % 2) * 0.5f);
//		targetPosition.x -= (data.transform.localScale.x % 2) * 0.5f;
//		targetPosition.y -= (targetScale.y % 2) * 0.5f;

//		if(data.transform.localPosition != targetPosition) {
//			data.transform.localPosition = targetPosition;
//		}
	}
}

public class RectUtils 
{
    public static Rect ResizeRect(Rect rect, Handles.DrawCapFunction capFunc, Color capCol, Color fillCol, float capSize, float snap)
    {
        Vector2 halfRectSize = new Vector2(rect.size.x * 0.5f, rect.size.y * 0.5f);

        Vector3[] rectangleCorners =
            { 
                new Vector3(rect.position.x - halfRectSize.x, rect.position.y - halfRectSize.y, 0),   // Bottom Left
                new Vector3(rect.position.x + halfRectSize.x, rect.position.y - halfRectSize.y, 0),   // Bottom Right
                new Vector3(rect.position.x + halfRectSize.x, rect.position.y + halfRectSize.y, 0),   // Top Right
                new Vector3(rect.position.x - halfRectSize.x, rect.position.y + halfRectSize.y, 0)    // Top Left
            }; 

        Handles.color = fillCol;
        Handles.DrawSolidRectangleWithOutline(rectangleCorners, new Color(fillCol.r, fillCol.g, fillCol.b, 0.25f), capCol);

		rect.center = Handles.Slider2D(rect.center, -Vector3.right, -Vector3.right, Vector3.up, capSize, capFunc, snap);
		rect.center = new Vector2(Mathf.Round(rect.center.x), Mathf.Round(rect.center.y));

        Vector3[] handlePoints =
            { 
                new Vector3(rect.position.x - halfRectSize.x, rect.position.y, 0),   // Left
                new Vector3(rect.position.x + halfRectSize.x, rect.position.y, 0),   // Right
                new Vector3(rect.position.x, rect.position.y + halfRectSize.y, 0),   // Top
                new Vector3(rect.position.x, rect.position.y - halfRectSize.y, 0)    // Bottom 
            }; 

        Handles.color = capCol;

        var newSize = rect.size;
        var newPosition = rect.position;
		

        var leftHandle =    Handles.Slider(handlePoints[0], -Vector3.right, capSize, capFunc, snap).x - handlePoints[0].x;
        var rightHandle =   Handles.Slider(handlePoints[1], Vector3.right, capSize, capFunc, snap).x - handlePoints[1].x;
        var topHandle =     Handles.Slider(handlePoints[2], Vector3.up, capSize, capFunc, snap).y - handlePoints[2].y;
        var bottomHandle =  Handles.Slider(handlePoints[3], -Vector3.up, capSize, capFunc, snap).y - handlePoints[3].y;
		leftHandle = Mathf.Round(leftHandle);
		rightHandle = Mathf.Round(rightHandle);
		topHandle = Mathf.Round(topHandle);
		bottomHandle = Mathf.Round(bottomHandle);

        newSize = new Vector2(
            Mathf.Max(.1f, newSize.x - leftHandle + rightHandle), 
            Mathf.Max(.1f, newSize.y + topHandle - bottomHandle));

        newPosition = new Vector2(
            newPosition.x + leftHandle * .5f + rightHandle * .5f, 
            newPosition.y + topHandle * .5f + bottomHandle * .5f);

//		newPosition.x = Mathf.Round(newPosition.x);
//		newPosition.y = Mathf.Round(newPosition.y);
        rect = new Rect(newPosition.x, newPosition.y, newSize.x, newSize.y);
//		rect.xMin = Mathf.Round(rect.xMin);
//		rect.yMin = Mathf.Round(rect.yMin);
//		rect.yMax = Mathf.Round(rect.yMax);
		return rect;
    }
}