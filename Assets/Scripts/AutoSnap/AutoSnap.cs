﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSnap : MonoBehaviour {
	public bool useDefaultSnapSettings;
	public float positionSnap = 1;
	public float rotationSnap = 15f;
	public float scaleSnap = 1f;
}
