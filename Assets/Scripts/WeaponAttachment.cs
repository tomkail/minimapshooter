using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponAttachment : MonoBehaviour {
	public Gun gun;

	public virtual void Init (Gun gun) {
		this.gun = gun;
	}

	public virtual void OnDraw () {}

	public virtual void OnPutAway () {}
}
