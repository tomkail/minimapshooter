using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerActions : PlayerActionSet {

    public PlayerAction MoveLeft;
    public PlayerAction MoveRight;
	public PlayerAction MoveUp;
    public PlayerAction MoveDown;
	public PlayerTwoAxisAction Move;

	public PlayerAction RotateLeft;
	public PlayerAction RotateRight;
	public PlayerAction RotateUp;
	public PlayerAction RotateDown;
	public PlayerTwoAxisAction Rotate;

	public PlayerAction Shoot;
    public PlayerAction Sights;
	public PlayerAction Run;
	public PlayerAction Reload;
	public PlayerAction Pickup;
	public PlayerAction SwitchWeapon;

	public PlayerActions() {
        MoveLeft = CreatePlayerAction( "Move Left" );
        MoveRight = CreatePlayerAction( "Move Right" );
		MoveUp = CreatePlayerAction( "Move Up" );
        MoveDown = CreatePlayerAction( "Move Down" );
		Move = CreateTwoAxisPlayerAction( MoveLeft, MoveRight, MoveDown, MoveUp );

		RotateLeft = CreatePlayerAction( "Rotate Left" );
		RotateRight = CreatePlayerAction( "Rotate Right" );
		RotateUp = CreatePlayerAction("Rotate Up");
		RotateDown = CreatePlayerAction("Rotate Down");
		Rotate = CreateTwoAxisPlayerAction( RotateLeft, RotateRight, RotateDown, RotateUp );

		Shoot = CreatePlayerAction( "Shoot" );
		Sights = CreatePlayerAction( "Sights" );
		Run = CreatePlayerAction( "Run" );
		Reload = CreatePlayerAction( "Reload" );
		Pickup = CreatePlayerAction( "Pickup" );
		SwitchWeapon = CreatePlayerAction("SwitchWeapon");
    }
}