﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class PlayerAvatar : MonoBehaviour {

	public new Rigidbody2D rigidbody {
		get {
			return GetComponent<Rigidbody2D>();
		}
	}

	public new Collider2D collider {
		get {
			return GetComponent<Collider2D>();
		}
	}

	public SpriteRenderer spriteRenderer {
		get {
			return GetComponent<SpriteRenderer>();
		}
	}

	public Player player;

	public FloatSmoothDamper runningDamper;

	public MovementStats defaultMovement;
	public MovementStats runningMovement;
	public MovementStats aimingMovement;

	public AnimationCurve defaultRotationResponseCurve;


	public delegate void PlayerEvent (PlayerAvatar avatar);
	public event PlayerEvent OnDie;


	[System.Serializable]
	public class MovementStats {
		public float baseSpeed = 75;
		public AnimationCurve responseCurve;
		public AnimationCurve moveLookDotMultiplier;

		public float rotationSpeed = 10;

		public Vector2 EvaluateMovement (Vector2 inputVector, float moveLookDot) {
			Vector2 calculatedSpeed = inputVector;
			calculatedSpeed *= baseSpeed;
			calculatedSpeed *= responseCurve.Evaluate(inputVector.magnitude);
			calculatedSpeed *= moveLookDotMultiplier.Evaluate(moveLookDot);
			return calculatedSpeed;
		}
	}

	public bool running;

	[System.Serializable]
	public abstract class PlayerModule {
		public PlayerAvatar player;

		public virtual void Init (PlayerAvatar player) {
			this.player = player;
		}

		public virtual void Update () {}

		public virtual void OnTriggerEnter2D (Collider2D collider) {}
		public virtual void OnTriggerStay2D (Collider2D collider) {}
		public virtual void OnTriggerExit2D (Collider2D collider) {}
	}

	public PlayerHealthModule healthModule;
	[System.Serializable]
	public class PlayerHealthModule : PlayerModule {
		public float maxHealth = 100;
		[SerializeField]
		private float _currentHealth;
		public float currentHealth {
			get {
				return _currentHealth;
			} private set {
				_currentHealth = value;
				if(OnChangeHealth != null) OnChangeHealth(currentHealth);
				if(currentHealth <= 0) {
					player.Die();
				}
			}
		}

		public delegate void HealthEvent(float health);
		public event HealthEvent OnTakeDamage;
		public event HealthEvent OnHeal;
		public event HealthEvent OnChangeHealth;

		public AutoRecoveryHealth autoHeal;
		[System.Serializable]
		public class AutoRecoveryHealth {
			[System.NonSerialized]
			public PlayerHealthModule healthModule;
			public float timeUntilHeal = 5;
			public float healSpeed = 20;
			public Timer healWaitTimer;
			public bool healing;

			public void Init (PlayerHealthModule healthModule) {
				this.healthModule = healthModule;
				this.healthModule.OnTakeDamage += OnTakeDamage;
				healWaitTimer = new Timer();
				healWaitTimer.OnComplete += delegate() {
					healing = true;
				};
			}

			void OnTakeDamage (float health) {
				healing = false;
				healWaitTimer.StopAndReset();
				healWaitTimer.Set(timeUntilHeal);
				healWaitTimer.Start();
			}

			public void Update () {
				healWaitTimer.Update();
				if(healing) {
					healthModule.Heal(healSpeed * Time.deltaTime);
				}
			}
		}

		public override void Init (PlayerAvatar player) {
			base.Init (player);
			autoHeal.Init(this);
			currentHealth = maxHealth;
		}

		public override void Update () {
			autoHeal.Update();
			base.Update ();
		}
		public void SetHealth (float newHealth) {
			currentHealth = newHealth;
		}

		public void TakeDamage (float damage) {
			currentHealth = Mathf.Clamp(currentHealth-damage, 0, maxHealth);
			if(OnTakeDamage != null) OnTakeDamage(damage);
		}

		public void Heal (float health) {
			currentHealth = Mathf.Clamp(currentHealth+health, 0, maxHealth);
			if(OnHeal != null) OnHeal(health);
		}
	}

	public PlayerWeaponModule weaponModule;
	[System.Serializable]
	public class PlayerWeaponModule : PlayerModule {
		public bool hasWeapon {
			get {
				return currentWeapon != null;
			}
		}
		public List<Gun> weapons;
		public Gun currentWeapon;

		public bool shooting;


		// When looking down sights
		public bool aiming;
		public FloatSmoothDamper inSightsDamper;

		public int maxWeapons = 2;

		public override void Init (PlayerAvatar player) {
			base.Init (player);
			List<Gun> foundWeapons = player.GetComponentsInChildren<Gun>().ToList();
			foreach(var weapon in foundWeapons) {
				AddWeapon(weapon);
			}
			if(!weapons.IsEmpty()) {
				currentWeapon = weapons.First();
				currentWeapon.Draw();
			}
		}

		public override void Update () {
			base.Update ();
			inSightsDamper.Update();
		}

		public void AddWeapon (Gun weapon, bool switchTo = false) {
			if(weapon == null) return;
			if(weapons.Count >= maxWeapons) {
				weapons.Remove(currentWeapon);
			}
			weapons.Add(weapon);
			weapon.player = player;
			weapon.transform.SetParent(player.transform);
			weapon.transform.ResetTransform();
			if(switchTo) SwitchToWeapon(weapon);
		}

		public void SwitchWeapon () {
			if(currentWeapon == null) {
				if(!weapons.IsEmpty())
					SwitchToWeapon(weapons[0]);
			} else {
				int currentWeaponIndex = weapons.IndexOf(currentWeapon);
				SwitchToWeapon(weapons.GetRepeating(currentWeaponIndex+1));
			}
		}

		public void SwitchToWeapon (Gun weapon) {
			if(!currentWeapon == null) {
				currentWeapon.PutAway();
			}
			currentWeapon = weapon;
			if(currentWeapon != null) {
				currentWeapon.Draw();
				inSightsDamper.smoothTime = currentWeapon.scopeTime;
			}
		}

		public void StartShooting () {
			if(!hasWeapon) return;
			shooting = true;
			currentWeapon.HoldTrigger();
		}

		public void EndShooting () {
			if(!hasWeapon) return;
			shooting = false;
			currentWeapon.ReleaseTrigger();
		}

		public void EnterSights () {
			aiming = true;
			inSightsDamper.target = 1;
		}

		public void ExitSights () {
			aiming = false;
			inSightsDamper.target = 0;
		}

	}

	public PlayerPickupModule pickupModule;
	[System.Serializable]
	public class PlayerPickupModule : PlayerModule {
		
		public override void OnTriggerEnter2D (Collider2D collider) {
			base.OnTriggerEnter2D(collider);
			Pickup pickup = collider.GetComponent<Pickup>();
			if(pickup != null && !pickup.requiresAction) {
				pickup.DoPickup(player);
			}
		}

		public override void OnTriggerStay2D (Collider2D collider) {
			base.OnTriggerStay2D(collider);
			Pickup pickup = collider.GetComponent<Pickup>();
			if(pickup != null && pickup.requiresAction && player.actions.Reload.IsPressed) {
				pickup.DoPickup(player);
			}
		}
	}

	private List<PlayerModule> modules;

	void Awake () {
		modules = new List<PlayerModule>() {
			healthModule,
			weaponModule,
			pickupModule
		};
		foreach(var module in modules) {
			module.Init(this);
		}
	}

	void Start () {
		SetUpActions();
	}

	void Update () {
		if(actions.Sights.WasPressed) {
			weaponModule.EnterSights ();
		} else if(actions.Sights.WasReleased) {
			weaponModule.ExitSights ();
		}
		if(!weaponModule.currentWeapon.burstModule.betweenBursts && actions.Run.WasPressed) {
			EnterRunning ();
		} else if(running && actions.Run.WasReleased) {
			ExitRunning ();
		}

		if(actions.Shoot.WasPressed && !running) {
			weaponModule.StartShooting ();
		} else if(weaponModule.shooting && actions.Shoot.WasReleased) {
			weaponModule.EndShooting ();
		}

		if(actions.Reload.WasPressed) {
			weaponModule.currentWeapon.Reload();
		}
		if(actions.SwitchWeapon.WasPressed) {
			if(!weaponModule.hasWeapon || !weaponModule.currentWeapon.burstModule.betweenBursts)
				weaponModule.SwitchWeapon();
		}

		foreach(var module in modules) {
			module.Update();
		}

		runningDamper.Update();

		spriteRenderer.color = Color.Lerp(Color.white, Color.cyan, weaponModule.inSightsDamper.current);

		if(Input.GetKeyDown(KeyCode.Q)) {
			healthModule.TakeDamage(20);
		}
	}

	void EnterRunning () {
		running = true;
		runningDamper.target = 1;
	}

	void ExitRunning () {
		running = false;
		runningDamper.target = 0;
	}

	void FixedUpdate () {
		if(actions.Move.IsPressed) {
			Vector2 clampedRawInput = actions.Move.Vector.normalized;
			Vector2.ClampMagnitude(clampedRawInput, 1);
			float moveLookDot = Vector2.Dot(clampedRawInput, transform.up);
			Vector2 movementForce = clampedRawInput;

			if(weaponModule.aiming) {
				movementForce = aimingMovement.EvaluateMovement(clampedRawInput, moveLookDot);
			} else if(running) {
				float degrees = -Vector2X.Degrees(actions.Move.Vector);
				float response = defaultRotationResponseCurve.Evaluate(actions.Move.Vector.magnitude);
				float rotateSpeed = runningMovement.rotationSpeed * response;
				rigidbody.MoveRotation(Mathf.LerpAngle(rigidbody.rotation, degrees, rotateSpeed * Time.fixedDeltaTime));
				movementForce = runningMovement.EvaluateMovement(transform.up * clampedRawInput.magnitude, moveLookDot);
			} else {
				movementForce = defaultMovement.EvaluateMovement(clampedRawInput, moveLookDot);
			}

			if(weaponModule.hasWeapon) {
				movementForce *= weaponModule.currentWeapon.movementSpeedMultiplier;
			}

			rigidbody.AddForce(movementForce);
		}
		if(!running && actions.Rotate.IsPressed) {
			float degrees = -Vector2X.Degrees(actions.Rotate.Vector);
			float response = defaultRotationResponseCurve.Evaluate(actions.Rotate.Vector.magnitude);

			float rotateSpeed = Mathf.Lerp(weaponModule.currentWeapon.defaultRotationSpeed, runningMovement.rotationSpeed, runningDamper.current);
			rotateSpeed = Mathf.Lerp(rotateSpeed, weaponModule.currentWeapon.scopedRotationSpeed, weaponModule.inSightsDamper.current);
			rotateSpeed *= response;

			if(weaponModule.aiming) {
				rigidbody.MoveRotation(Mathf.LerpAngle(rigidbody.rotation, degrees, rotateSpeed * Time.fixedDeltaTime));
			} else {
				rigidbody.MoveRotation(Mathf.LerpAngle(rigidbody.rotation, degrees, rotateSpeed * Time.fixedDeltaTime));
			}
		}
	}

	public void Die () {
		if(OnDie != null)
			OnDie(this);
	}

	public PlayerActions actions;
	void SetUpActions () {
		actions = new PlayerActions();
		actions.Device = player.device;

		actions.MoveLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
		actions.MoveLeft.AddDefaultBinding(Key.LeftArrow);
		actions.MoveRight.AddDefaultBinding(InputControlType.LeftStickRight);
		actions.MoveRight.AddDefaultBinding(Key.RightArrow);
		actions.MoveUp.AddDefaultBinding(InputControlType.LeftStickUp);
		actions.MoveUp.AddDefaultBinding(Key.UpArrow);
		actions.MoveDown.AddDefaultBinding(InputControlType.LeftStickDown);
		actions.MoveDown.AddDefaultBinding(Key.DownArrow);

		actions.RotateLeft.AddDefaultBinding(InputControlType.RightStickLeft);
//		actions.RotateLeft.AddDefaultBinding(Key.A);
		actions.RotateRight.AddDefaultBinding(InputControlType.RightStickRight);
//		actions.RotateRight.AddDefaultBinding(Key.D);
		actions.RotateUp.AddDefaultBinding(InputControlType.RightStickUp);
//		actions.RotateLeft.AddDefaultBinding(Key.A);
		actions.RotateDown.AddDefaultBinding(InputControlType.RightStickDown);
//		actions.RotateRight.AddDefaultBinding(Key.DownArrow);

		actions.Shoot.AddDefaultBinding(InputControlType.RightTrigger);
		actions.Shoot.AddDefaultBinding(Mouse.LeftButton);

		actions.Sights.AddDefaultBinding(InputControlType.LeftTrigger);
		actions.Sights.AddDefaultBinding(Mouse.RightButton);
		actions.Sights.AddDefaultBinding(Key.RightShift);

//		actions.Run.AddDefaultBinding(InputControlType.LeftStickButton);
		actions.Run.AddDefaultBinding(InputControlType.RightBumper);
		actions.Run.AddDefaultBinding(InputControlType.LeftBumper);
		actions.Run.AddDefaultBinding(Key.LeftShift);

		actions.Reload.AddDefaultBinding(InputControlType.Action3);
		actions.Reload.AddDefaultBinding(Key.R);

		actions.SwitchWeapon.AddDefaultBinding(InputControlType.Action4);
		actions.SwitchWeapon.AddDefaultBinding(Key.S);

		actions.Pickup.AddDefaultBinding(InputControlType.Action2);
		actions.Pickup.AddDefaultBinding(Key.P);
	}

	private void OnTriggerEnter2D (Collider2D collider) {
		foreach(var module in modules) {
			module.OnTriggerEnter2D(collider);
		}
	}

	private void OnTriggerStay2D (Collider2D collider) {
		foreach(var module in modules) {
			module.OnTriggerStay2D(collider);
		}
	}

	private void OnTriggerExit2D (Collider2D collider) {
		foreach(var module in modules) {
			module.OnTriggerExit2D(collider);
		}
	}
}