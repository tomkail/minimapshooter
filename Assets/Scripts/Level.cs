﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Level : MonoBehaviour {

	public List<SpawnArea> spawnAreas;

	void Awake () {
		spawnAreas = GetComponentsInChildren<SpawnArea>().ToList();
	}

	public Transform GetBestSpawnPointForPlayer (Player player) {
		List<Vector2> otherTeamPlayerPositions = GameController.Instance.game.players.Where(x => x.team != player.team && x.avatar != null).Select(x => (Vector2)x.avatar.transform.position).ToList();
		SpawnArea bestArea = null;
		if(otherTeamPlayerPositions.IsEmpty()) {
			bestArea = spawnAreas.Random();
		} else {
			bestArea = spawnAreas.OrderBy(x => Vector2X.ClosestDistance(x.transform.position, otherTeamPlayerPositions)).Last();
		}
		return bestArea.GetBestSpawnPoint();
	}
}
