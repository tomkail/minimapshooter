using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleScale : MonoBehaviour {

	public AnimationCurve scaleCurve;
	public float timer;

	void Update () {
		timer += Time.deltaTime;
		transform.localScale = Vector3.one * scaleCurve.Evaluate(timer);
	}
}