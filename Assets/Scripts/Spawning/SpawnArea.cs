﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnArea : MonoBehaviour {

	public List<SpawnPoint> spawnPoints = new List<SpawnPoint>();

	void Awake () {
		spawnPoints = GetComponentsInChildren<SpawnPoint>().ToList();
	}

	public Transform GetBestSpawnPoint () {
		List<Vector2> playerPositions = GameController.Instance.game.players.Where(x => x.avatar != null).Select(x => (Vector2)x.avatar.transform.position).ToList();
		if(playerPositions.IsEmpty()) {
			return spawnPoints.Random().transform;
		} else {
			return spawnPoints.OrderBy(x => Vector2X.ClosestDistance(x.transform.position, playerPositions)).Last().transform;
		}
	}
}