﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class GunUI : UIMonoBehaviour {

	public Player player;
	public UILineRenderer arcRenderer;
	public UIPolygon segmentPolygon;

	public UILineRenderer lineRenderer;
	public Image pointRenderer;

	public AnimationCurve inaccuracyPointLineCurve = new AnimationCurve(new Keyframe(0,1), new Keyframe(2,0));

	void Update () {
		if(player.avatar == null || !player.avatar.weaponModule.hasWeapon) return;

		float pointLineStrength = inaccuracyPointLineCurve.Evaluate(player.avatar.weaponModule.currentWeapon.accuracyModule.currentInaccuracy);

		float wideAlpha = (1f-pointLineStrength) * (1f-player.avatar.runningDamper.current);
		arcRenderer.color = arcRenderer.color.WithAlpha(wideAlpha * 0.5f);
		segmentPolygon.color  = arcRenderer.color.WithAlpha(wideAlpha * 0.2f);

		float tightAlpha = pointLineStrength * (1f-player.avatar.runningDamper.current);
		lineRenderer.color = lineRenderer.color.WithAlpha(tightAlpha * 0.5f);
		pointRenderer.color = pointRenderer.color.WithAlpha(tightAlpha * 0.5f);

		pointRenderer.GetComponent<WorldSpaceUIElement>().worldPosition = player.avatar.transform.position + player.avatar.transform.up * player.avatar.weaponModule.currentWeapon.currentRange;

		UpdateArcPoints();
		UpdatePolygonPoints();

		UpdateLinePoints();
	}

	void UpdateArcPoints () {
		float playerAngle = Vector2X.Degrees(player.avatar.transform.up);
		float inaccuracy = player.avatar.weaponModule.currentWeapon.accuracyModule.currentInaccuracy;
		float distance = player.avatar.weaponModule.currentWeapon.currentRange;
		float startAngle = playerAngle - inaccuracy * 0.5f;
		float endAngle = playerAngle + inaccuracy * 0.5f;
		int numPoints = Mathf.Max(2, Mathf.RoundToInt(inaccuracy));
		Vector2[] points = new Vector2[numPoints];
		for(int i = 0; i < points.Length; i++) {
			float normalizedAngle = (float)i/(points.Length-1);
			float angle = Mathf.Lerp(startAngle, endAngle, normalizedAngle);
			Vector3 offset = MathX.DegreesToVector2(angle) * distance;
			points[i] = WorldToCanvasSpace(player.avatar.transform.position + offset);
		}
		arcRenderer.Points = points;
	}

	void UpdatePolygonPoints () {
		List<Vector2> points = new List<Vector2>();
		points.Add(WorldToCanvasSpace(player.avatar.transform.position));
		float playerAngle = Vector2X.Degrees(player.avatar.transform.up);
		float inaccuracy = player.avatar.weaponModule.currentWeapon.accuracyModule.currentInaccuracy;
		float distance = player.avatar.weaponModule.currentWeapon.currentRange;
		float startAngle = playerAngle - inaccuracy * 0.5f;
		float endAngle = playerAngle + inaccuracy * 0.5f;
		int numPoints = Mathf.Max(2, Mathf.RoundToInt(inaccuracy));
		for(int i = 0; i < numPoints; i++) {
			float normalizedAngle = (float)i/(numPoints-1);
			float angle = Mathf.Lerp(startAngle, endAngle, normalizedAngle);
			Vector3 offset = MathX.DegreesToVector2(angle) * distance;
			points.Add(WorldToCanvasSpace(player.avatar.transform.position + offset));
		}
		segmentPolygon.points = points.ToArray();
	}

	void UpdateLinePoints () {
		Vector2[] points = new Vector2[2];
		points[0] = WorldToCanvasSpace(player.avatar.transform.position);
		float playerAngle = Vector2X.Degrees(player.avatar.transform.up);
		float inaccuracy = player.avatar.weaponModule.currentWeapon.accuracyModule.currentInaccuracy;
		float distance = player.avatar.weaponModule.currentWeapon.currentRange;
		Vector3 offset = MathX.DegreesToVector2(playerAngle) * distance;
		points[1] = WorldToCanvasSpace(player.avatar.transform.position + offset);

		lineRenderer.Points = points;
	}

	Vector2 WorldToCanvasSpace (Vector3 targetPosition) {
		Vector3? targetPositionNullable = canvas.WorldPointToLocalPointInRectangle(Camera.main, targetPosition);
		var canvasPosition = (Vector3) targetPositionNullable;

		if(canvas.renderMode == RenderMode.WorldSpace) {
			canvasPosition = transform.parent.InverseTransformPoint(targetPosition);
			canvasPosition.z = 0;
		}
		return canvasPosition;
	}
}
