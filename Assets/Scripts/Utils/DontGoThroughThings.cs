using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using InControl;
using UnityX.Geometry;
using UnityX.StateMachine;

public class DontGoThroughThings : MonoBehaviour {

	private Rigidbody2D _rigidbody2D;
	public new Rigidbody2D rigidbody2D {
		get {
			if(_rigidbody2D == null)
				_rigidbody2D = GetComponent<Rigidbody2D>();
			return _rigidbody2D;
		}
	}

	private Collider2D _collider;
	public new Collider2D collider {
		get {
			if(_collider == null)
				_collider = GetComponent<Collider2D>();
			return _collider;
		}
	}

	public List<Collider2D> triggers = new List<Collider2D>();
	private Vector2 previousPosition;

	private void Awake () {
		previousPosition = rigidbody2D.position;
	}

	private void FixedUpdate () {
		InterframeTriggerCheck();
	}

	private void InterframeTriggerCheck () {
//		Debug.Log (rigidbody2D.position.y+" "+previousPosition.y+" "+rigidbody2D.velocity.y);
		//have we moved more than our minimum extent? 

//		Vector2 movementThisStep = rigidbody2D.position - previousPosition;
//		float movementSqrMagnitude = movementThisStep.sqrMagnitude;
//		Vector2 movementThisStep = rigidbody2D.position, rigidbody2D.velocity * Time.fixedDeltaTime;
		float movementSqrMagnitude = (rigidbody2D.velocity * Time.fixedDeltaTime).sqrMagnitude;

		float sqrMinimumExtent = 0;

		if (movementSqrMagnitude > sqrMinimumExtent)  { 
			float movementMagnitude = Mathf.Sqrt(movementSqrMagnitude);
			Debug.DrawRay (rigidbody2D.position, rigidbody2D.velocity * Time.fixedDeltaTime, Color.blue);
			RaycastHit2D[] hitInfos = Physics2D.RaycastAll (rigidbody2D.position, rigidbody2D.velocity * Time.fixedDeltaTime, movementMagnitude);

//			Debug.DrawRay (previousPosition, movementThisStep, Color.red);
//			RaycastHit2D[] hitInfos = Physics2D.RaycastAll (previousPosition, movementThisStep.normalized, movementMagnitude, Physics2D.GetLayerCollisionMask(Skater.layer));

			List<Collider2D> hitColliders = new List<Collider2D>();
			foreach(RaycastHit2D hitInfo in hitInfos) {
				if (hitInfo.collider != null) {
					if (hitInfo.collider.isTrigger) {
						EnterTrigger(hitInfo.collider);
					} else {
//						SendMessage("OnTriggerEnter2D", hitInfo.collider);
					}
					hitColliders.Add(hitInfo.collider);
				}
			}

			List<Collider2D> triggersToRemove = triggers.Except(hitColliders).ToList();
			foreach(var trigger in triggersToRemove) {
				ExitTrigger(trigger);
			}
			//check for obstructions we might have missed 
		}

		previousPosition = rigidbody2D.position;
	}

	private void EnterTrigger (Collider2D otherCollider) {
		triggers.Add(otherCollider);
		otherCollider.SendMessage("OnTriggerEnter2D", collider, SendMessageOptions.DontRequireReceiver);
		SendMessage("OnTriggerEnter2D", otherCollider, SendMessageOptions.DontRequireReceiver);
	}

	private void ExitTrigger (Collider2D otherCollider) {
		bool removed = triggers.Remove(otherCollider);
		if(!removed) return;
//		otherCollider.SendMessage("OnTriggerExit2D", collider, SendMessageOptions.DontRequireReceiver);
//		SendMessage("OnTriggerExit2D", otherCollider, SendMessageOptions.DontRequireReceiver);
	}
}