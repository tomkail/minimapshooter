
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public abstract class SmoothDamper<T> {

	private const float defaultSmoothTime = 0.1f;
	private const float defaultMaxSpeed = Mathf.Infinity;

	[SerializeField]
	private T _target;
	public T target {
		get {
			return _target;
		} set {
//			if(_target.Equals(value)) return;
			_target = value;
			if(OnChangeTarget != null) OnChangeTarget(target);
		}
	}
	[SerializeField]
	private T _current;
	public T current {
		get {
			return _current;
		} set {
//			if(_current.Equals(value)) return;
			_current = value;
			if(OnChangeCurrent != null) OnChangeCurrent(current);
		}
	}
	public float smoothTime = defaultSmoothTime;
	[SerializeField, DisableAttribute]
	protected T currentVelocity;
	[SerializeField]
	protected float maxSpeed = defaultMaxSpeed;

	public event System.Action<T> OnChangeTarget;
	public event System.Action<T> OnChangeCurrent;

	// Used for correct Unity editor serializer initialization
	protected SmoothDamper () {
		smoothTime = defaultSmoothTime;
		maxSpeed = defaultMaxSpeed;
	}

	public SmoothDamper (T target, T current) {
		this.target = target;
		this.current = current;
	}

	public SmoothDamper (T target, T current, float smoothTime) {
		this.target = target;
		this.current = current;
		this.smoothTime = smoothTime;
	}

	public virtual T Update () {
		return Update(Time.deltaTime);
	}

	public virtual T Update (float deltaTime) {
		current = SmoothDamp(deltaTime);
		return current;
	}

	protected abstract T SmoothDamp (float deltaTime);
}

[System.Serializable]
public class FloatSmoothDamper : SmoothDamper<float> {
	protected FloatSmoothDamper () : base () {}
	public FloatSmoothDamper (float target, float current) : base(target, current) {}
	public FloatSmoothDamper (float target, float current, float smoothTime) : base(target, current, smoothTime) {}

	protected override float SmoothDamp (float deltaTime) {
		return Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
	}
}

[System.Serializable]
public class Vector2SmoothDamper : SmoothDamper<Vector2> {
	protected Vector2SmoothDamper () : base () {}
	public Vector2SmoothDamper (Vector2 target, Vector2 current) : base(target, current) {}
	public Vector2SmoothDamper (Vector2 target, Vector2 current, float smoothTime) : base(target, current, smoothTime) {}

	protected override Vector2 SmoothDamp (float deltaTime) {
		return SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
	}

	private static Vector2 SmoothDamp (Vector2 current, Vector2 target, ref Vector2 currentVelocity, float smoothTime, float maxSpeed, float deltaTime) {
		return Vector2.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
	}
}

[System.Serializable]
public class ColorSmoothDamper : SmoothDamper<Color> {
	protected ColorSmoothDamper () : base () {}
	public ColorSmoothDamper (Color target, Color current) : base(target, current) {}
	public ColorSmoothDamper (Color target, Color current, float smoothTime) : base(target, current, smoothTime) {}

	protected override Color SmoothDamp (float deltaTime) {
		return SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
	}

	private static Color SmoothDamp (Color current, Color target, ref Color currentVelocity, float smoothTime, float maxSpeed, float deltaTime) {
		return new Color(
			Mathf.SmoothDamp(current.r, target.r, ref currentVelocity.r, smoothTime, maxSpeed, deltaTime),
			Mathf.SmoothDamp(current.g, target.g, ref currentVelocity.g, smoothTime, maxSpeed, deltaTime),
			Mathf.SmoothDamp(current.b, target.b, ref currentVelocity.b, smoothTime, maxSpeed, deltaTime),
			Mathf.SmoothDamp(current.a, target.a, ref currentVelocity.a, smoothTime, maxSpeed, deltaTime)
		);
	}
}

[System.Serializable]
public class RectSmoothDamper : SmoothDamper<Rect> {
	protected RectSmoothDamper () : base () {}
	public RectSmoothDamper (Rect target, Rect current) : base(target, current) {}
	public RectSmoothDamper (Rect target, Rect current, float smoothTime) : base(target, current, smoothTime) {}

	protected override Rect SmoothDamp (float deltaTime) {
		return SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
	}

	private static Rect SmoothDamp (Rect current, Rect target, ref Rect currentVelocity, float smoothTime, float maxSpeed, float deltaTime) {
		Vector2 posVel = currentVelocity.position;
		Vector2 sizeVel = currentVelocity.size;
		Rect output = new Rect(
			Vector2.SmoothDamp(current.position, target.position, ref posVel, smoothTime, maxSpeed, deltaTime),
			Vector2.SmoothDamp(current.size, target.size, ref sizeVel, smoothTime, maxSpeed, deltaTime)
		);
		currentVelocity.position = posVel;
		currentVelocity.size = sizeVel;
		return output;
	}
}