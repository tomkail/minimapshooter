﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {
	public bool startAutomatically = true;
	public float time = 1f;
	public Timer timer;
	
	public void StartTimer (float time) {
		this.time = time;
		timer = new Timer(this.time);
		timer.OnComplete += Timer_OnComplete;
		timer.Start();
	}

	void Timer_OnComplete () {
		Destroy(gameObject);
	}
	
	void Start () {
		if(startAutomatically)
			StartTimer(time);
	}
	
	void Update () {
		timer.Update ();
	}
}
