﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TremorManager {
	public Vector2 tremorPosition = Vector2.zero;
	
	public List<Tremor> tremors;
	public bool tremoring = false;
	
	public Vector2 magnitude = Vector2.one;
	
	public TremorManager () {
		tremors = new List<Tremor>();
	}
	
	public void AddNewTremor(Tremor myTremor){
		tremors.Add(myTremor);
		ChangeNumTremors();
	}
	
	public void Update(){
		Update(Time.deltaTime);
	}
	
	public void Update(float myDeltaTime){
		ResetTremor();
		if(!tremoring)return;
		
		for(int i = 0; i < tremors.Count; i++){
			tremors[i].Loop(myDeltaTime);
			
			tremorPosition += tremors[i].tremorPosition;
			
			if(!tremors[i].alive){
				tremors.RemoveAt(i);
				ChangeNumTremors();
				i--;
			}
		}
		
		tremorPosition = Vector2.Scale(magnitude, tremorPosition);
	}
	
	public void ClearTremors (){
		tremors.Clear();
		ChangeNumTremors();
	}
	
	private void ResetTremor () {
		tremorPosition = Vector3.zero;
	}
	
	private void ChangeNumTremors (){
		if(tremors.Count == 0){
			tremoring = false;
		} else {
			tremoring = true;
		}
	}
}