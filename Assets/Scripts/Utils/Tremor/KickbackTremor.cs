﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class KickbackTremor : Tremor {
	
//	public float shakeTime = 0.2f;
	
//	public KickbackTremor(float myLifetime, float myMagnitude, float myShakeTime) : base (myLifetime) {
//		magnitude = Vector2.one * myMagnitude;
//		shakeTime = myShakeTime;
//	}
	
//	public KickbackTremor(float myLifetime, Vector2 myMagnitude, float myShakeTime) : base (myLifetime) {
//		magnitude = myMagnitude;
//		shakeTime = myShakeTime;
//	}

	public Vector2 direction;

	public KickbackTremor(float _lifetime, float _shakeTime, float _magnitude, Vector2 _direction) : base (_lifetime, _shakeTime, _magnitude) {
		direction = _direction;
	}
	
	
	public override void Init() {
		base.Init ();
	}
	
	public override void UpdateTremor() {
		float shake = OscellateTriangleRepeating((lifetime - life) * tremorSpeed);
//		magnitude = MathX.Degrees2Vector2(Mathf.FloorToInt((lifetime - life) * tremorSpeed) * 1400).normalized * magnitude;
		tremorPosition = Vector2.Scale(Vector2.one * shake, direction * magnitude * (normalizedLife * normalizedLife));
	}
}
