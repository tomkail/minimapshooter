﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnforceDecendentGameObjectProperties : MonoBehaviour {
	public bool enforceTag = true;
	public bool enforceLayer = true;
	public bool enforceIsStatic = true;

	public static void EnforcePropertiesAll () {
		EnforceDecendentGameObjectProperties[] all = Object.FindObjectsOfType<EnforceDecendentGameObjectProperties>();
		foreach(var enforce in all) enforce.EnforceProperties();
	}

	public void EnforceProperties () {
		Transform[] allChildren = GetComponentsInChildren<Transform>();
		foreach(var child in allChildren) {
			if(enforceTag && child.gameObject.tag != gameObject.tag) child.gameObject.tag = gameObject.tag;
			if(enforceLayer && child.gameObject.layer != gameObject.layer) child.gameObject.layer = gameObject.layer;
			if(enforceIsStatic && child.gameObject.isStatic != gameObject.isStatic) child.gameObject.isStatic = gameObject.isStatic;
		}
	}

	void OnTransformChildrenChanged () {
		EnforceProperties();
	}
	/*
	#if UNITY_EDITOR
	[UnityEditor.Callbacks.DidReloadScripts]
 	private static void OnScriptsReloaded() {
		EnforcePropertiesAll();
 	}
 	#endif
 	*/
}