using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

	[HideInInspector]
	public PlayerAvatar player;

	[System.Serializable]
	public abstract class GunModule {
		[HideInInspector]
		public Gun gun;

		public virtual void Init (Gun gun) {
			this.gun = gun;
		}

		public virtual void Update () {}

		public virtual void OnDraw () {}

		public virtual void OnPutAway () {}

		public virtual void OnShoot () {}
	}

	public bool triggerHeld;

	public float movementSpeedMultiplier = 1;

	public float defaultRotationSpeed = 10;
	public float scopedRotationSpeed = 1;

	public float scopeTime = 0.2f;

	public float shotBaseDamage = 20;

	public float range = 10;
	public float scopedRange = 15;
//	public AnimationCurve damageOverDistanceMultiplier = new AnimationCurve(new Keyframe(0,1), new Keyframe(20,1), new Keyframe(30,0));

	public float recoil = 20;
	public float scopedRecoil = 5;
	public float currentRecoil {
		get {
			return Mathf.Lerp(recoil, scopedRecoil, player.weaponModule.inSightsDamper.current);
		}
	}

	public float currentRange {
		get {
			return Mathf.Lerp(range, scopedRange, player.weaponModule.inSightsDamper.current);
		}
	}


	public BulletModule bulletModule;
	[System.Serializable]
	public class BulletModule : GunModule {

		public bool raycast = true;

		public Projectile bulletPrefab;
		public float bulletForce = 1000;

		// Used for shotguns
		public int bulletsPerShot = 1;

		public override void Init (Gun gun) {
			base.Init (gun);
		}

		public override void OnShoot () {
			float angle = Vector2X.Degrees(gun.transform.up);
			for(int i = 0; i < bulletsPerShot; i++) {
				float angleOffset = Random.Range(-gun.accuracyModule.currentInaccuracy * 0.5f, gun.accuracyModule.currentInaccuracy * 0.5f);
				Vector2 direction = MathX.DegreesToVector2(angle + angleOffset);

				gun.player.rigidbody.AddForce(-direction * gun.currentRecoil);

				if(raycast) {
					RaycastBullet(direction);
				} else {
					ShootBullet(direction);
				}
			}
		}

		public void ShootBullet (Vector2 direction) {
			Quaternion rotation = Quaternion.Euler(0,0,90-Vector2X.Degrees(direction));
			Projectile bullet = Object.Instantiate(bulletPrefab, gun.player.rigidbody.position, rotation);
			bullet.Shoot(gun);
			bullet.rigidbody.AddForce(direction * bulletForce);
		}

		public void RaycastBullet (Vector2 direction) {
			RaycastHit2D[] hits = Physics2D.RaycastAll(gun.transform.position, direction, gun.currentRange);
			Vector2 hitPoint = (Vector2)gun.transform.position + direction * gun.currentRange;
			foreach(RaycastHit2D hit in hits) {
				if(hit.collider == null) continue;
				if(hit.collider.gameObject == gun.player.gameObject) continue;
				
				Vector2 particleDirection = Vector2.Reflect(direction, hit.normal);
				Quaternion rotation = Quaternion.Euler(0,0,90-Vector2X.Degrees(particleDirection));
				hitPoint = hit.point;
				float damage = gun.shotBaseDamage;
	//			damageOverDistanceMultiplier.Evaluate(hit.distance);
				if(hit.collider.gameObject.CompareTag("Player")) {
					PlayerAvatar player = hit.collider.GetComponent<PlayerAvatar>();
					Object.Instantiate(PrefabDatabase.Instance.playerHitParticles, hit.point, rotation);
					player.rigidbody.AddForce(direction * damage * 4);
					player.healthModule.TakeDamage(damage);
					break;
				} else if(hit.collider.gameObject.CompareTag("Wall")) {
					Object.Instantiate(PrefabDatabase.Instance.bulletHitParticles, hit.point, rotation);
					break;
				}
			}
			
			BulletTracer tracer = Object.Instantiate<BulletTracer>(PrefabDatabase.Instance.bulletTracer);
			tracer.Init(gun.transform.position, hitPoint);
		}

		public static Vector2 GetHitPoint (Vector2 position, Vector2 direction, params GameObject[] ignoreGOs) {
			Vector2 hitPoint = position + direction * 100;
			RaycastHit2D[] hits = Physics2D.RaycastAll(position, direction);
			foreach(RaycastHit2D hit in hits) {
				if(hit.collider == null) continue;
				if(ignoreGOs.Contains(hit.collider.gameObject)) continue;
				hitPoint = hit.point;
				break;
	 		}
			return hitPoint;
		}
	}

	public AmmoModule ammoModule;
	[System.Serializable]
	public class AmmoModule : GunModule {
		public int clipSize = 8;
		public int bulletsInClip;
		public bool clipEmpty {
			get {
				return useAmmo && bulletsInClip <= 0;
			}
		}

		public bool useAmmo {
			get {
				return clipSize > 0;
			}
		}

		public override void Init (Gun gun) {
			base.Init (gun);
			bulletsInClip = clipSize;
		}

		public override void OnShoot () {
			base.OnShoot ();
			ShootBullet();
		}

		public void ShootBullet () {
			bulletsInClip--;
			if(clipEmpty) {
				EndClip();
			}
		}

		void EndClip () {
			gun.automaticModule.shootRepeatTimer.StopAndReset();
		}
	}
	
	public ReloadModule reloadModule;
	[System.Serializable]
	public class ReloadModule : GunModule {
		public bool reloading {
			get {
				return reloadTimer.state == Timer.State.Playing;
			}
		}
		public float reloadTime = 0.5f;
		public Timer reloadTimer;
		
		public override void Init (Gun gun) {
			base.Init(gun);
			reloadTimer = new Timer(reloadTime);
			reloadTimer.OnComplete += ReloadTimer_OnComplete;
		}
		
		public override void Update () {
			base.Update();
			reloadTimer.Update();
		}

		public override void OnPutAway () {
			base.OnPutAway ();
			reloadTimer.StopAndReset();
		}
		
		void ReloadTimer_OnComplete () {
			CompleteReload();
		}
		
		void CompleteReload () {
			reloadTimer.StopAndReset();
			
			gun.ammoModule.bulletsInClip = gun.ammoModule.clipSize;
			
			if(gun.burstModule.useBurst) {
				gun.burstModule.ResetBurst();
			}
			
			if(gun.triggerHeld && gun.automaticModule.isAutomatic && !gun.ammoModule.clipEmpty) {
				gun.automaticModule.shootRepeatTimer.Start();
			}
		}
	}

	public AutomaticModule automaticModule;
	[System.Serializable]
	public class AutomaticModule : GunModule {
		public bool isAutomatic {
			get {
				return autoShootTime > 0;
			}
		}
		public float autoShootTime = 0.05f;
		public Timer shootRepeatTimer;

		public override void Init (Gun gun) {
			base.Init(gun);
			shootRepeatTimer = new Timer(autoShootTime, true);
			shootRepeatTimer.OnRepeat += ShootRepeatTimer_OnRepeat;
		}

		public override void Update () {
			base.Update ();
			shootRepeatTimer.Update();
		}

		void ShootRepeatTimer_OnRepeat () {
			if(!gun.ammoModule.clipEmpty)
				gun.Shoot();
		}
	}

	public BurstModule burstModule;
	[System.Serializable]
	public class BurstModule : GunModule {

		public bool useBurst {
			get {
				return burstLength > 0;
			}
		}

		public int burstLength = 0;
		[DisableAttribute]
		public int bulletsShotThisBurst = 0;
		public bool betweenBursts = false;
		public float burstIntervalTime = 0.1f;
		[DisableAttribute]
		public Timer burstIntervalTimer;

		public override void Init (Gun gun) {
			base.Init (gun);
			burstIntervalTimer = new Timer(burstIntervalTime);
			burstIntervalTimer.OnComplete += BurstIntervalTimer_OnComplete;
		}

		public override void Update () {
			base.Update();
			burstIntervalTimer.Update();
		}

		public override void OnShoot () {
			base.OnShoot ();
			if(useBurst) {
				bulletsShotThisBurst++;
				if(bulletsShotThisBurst >= burstLength) {
					EndBurst();
				}
			}
		}

		void BurstIntervalTimer_OnComplete () {
			ResetBurst();
			burstIntervalTimer.StopAndReset();
			betweenBursts = false;
		}

		public void StartBurst () {
			gun.Shoot();
		}

		void EndBurst () {
			betweenBursts = true;
			burstIntervalTimer.Start();
			gun.automaticModule.shootRepeatTimer.StopAndReset();
		}

		public void ResetBurst () {
			bulletsShotThisBurst = 0;
		}
	}

	public AccuracyModule accuracyModule;
	[System.Serializable]
	public class AccuracyModule : GunModule {
		public float currentInaccuracy {
			get {
				return baseInaccuracy + drawAccuracy + shootInccuracyOffset.current;
			}
		}
		private float baseInaccuracy {
			get {
				float _currentAccuracy = Mathf.Lerp(hipAccuracy.inaccuracy, scopedAccuracy.inaccuracy, gun.player.weaponModule.inSightsDamper.current);
				_currentAccuracy *= Mathf.Lerp(1, 2, gun.player.runningDamper.current);
				
				return _currentAccuracy;
			}
		}

		[SerializeField]
		private FloatSmoothDamper shootInccuracyOffset = new FloatSmoothDamper(0,0,0.5f);

		[SerializeField]
		private AccuracyStats hipAccuracy = new AccuracyStats(5, new AnimationCurve(new Keyframe(0,5), new Keyframe(15,0)));
		[SerializeField]
		private AccuracyStats scopedAccuracy = new AccuracyStats(1, new AnimationCurve(new Keyframe(0,1), new Keyframe(5,0)));

		[SerializeField]
		private AnimationCurve drawAccuracyCurve = new AnimationCurve(new Keyframe(0, 20), new Keyframe(1, 0));
		public float drawAccuracy {
			get {
				return drawAccuracyCurve.Evaluate(timeSinceDrawn);
			}
		}
		public float timeSinceDrawn = 0;

		[System.Serializable]
		public struct AccuracyStats {
			public float inaccuracy;
			// The effect shooting has your accuracy, evaluated based on current accuracy
			public AnimationCurve shootInaccuracyDelta;

			public AccuracyStats (float accuracy, AnimationCurve shootAccuracyEffect) {
				this.inaccuracy = accuracy;
				this.shootInaccuracyDelta = shootAccuracyEffect;
			}
		}

		public override void OnDraw () {
			base.OnDraw ();
			timeSinceDrawn = 0;
		}

		public override void Update () {
			base.Update ();
			timeSinceDrawn += Time.deltaTime;
			shootInccuracyOffset.Update();
		}

		public override void OnShoot () {
			base.OnShoot();
			float inaccuracyDelta = Mathf.Lerp(hipAccuracy.shootInaccuracyDelta.Evaluate(currentInaccuracy), scopedAccuracy.shootInaccuracyDelta.Evaluate(currentInaccuracy), gun.player.weaponModule.inSightsDamper.current);
			shootInccuracyOffset.current += inaccuracyDelta;
		}
	}

	public AttachmentModule attachmentModule;
	[System.Serializable]
	public class AttachmentModule : GunModule {
		public List<WeaponAttachment> attachments;

		public override void Init (Gun gun) {
			base.Init (gun);
			attachments = gun.GetComponentsInChildren<WeaponAttachment>().ToList();
			foreach(var attachment in attachments) {
				attachment.Init(gun);
			}
		}

		public override void OnDraw () {
			base.OnDraw ();
			foreach(var attachment in attachments) {
				attachment.OnDraw();
			}
		}

		public override void OnPutAway () {
			base.OnPutAway ();
			foreach(var attachment in attachments) {
				attachment.OnPutAway();
			}
		}
	}

	public List<GunModule> modules;

	void Awake () {
		modules = new List<GunModule>() {
			bulletModule,
			reloadModule,
			ammoModule,
			burstModule,
			automaticModule,
			accuracyModule,
			attachmentModule
		};

		foreach(var module in modules) {
			module.Init(this);
		}
	}

	public void PutAway () {
		foreach(var module in modules) {
			module.OnPutAway();
		}
		ReleaseTrigger();
	}

	public void Draw () {
		foreach(var module in modules) {
			module.OnDraw();
		}
	}

	void Update () {
		foreach(var module in modules) {
			module.Update();
		}
	}

	public void HoldTrigger () {
		triggerHeld = true;
		if(reloadModule.reloading) {
				
		} else if(ammoModule.clipEmpty) {
			Reload();
		} else {
			if(burstModule.useBurst) {
				if(burstModule.betweenBursts) {
					return;
				}
				burstModule.StartBurst();
			} else {
				Shoot();
			}
			if(automaticModule.isAutomatic) {
				automaticModule.shootRepeatTimer.Start();
			}
		}
	}

	public void Shoot () {
		foreach(var module in modules) {
			module.OnShoot();
		}
	}

	public void ReleaseTrigger () {
		triggerHeld = false;
		if(!burstModule.useBurst)
			automaticModule.shootRepeatTimer.StopAndReset();
	}

	public void Reload () {
		if(reloadModule.reloading) return;
		automaticModule.shootRepeatTimer.StopAndReset();
		reloadModule.reloadTimer.Start();
	}
}