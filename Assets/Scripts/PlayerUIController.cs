using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class PlayerUIController : UIMonoBehaviour {
	public Player player;
	public PlayerUIPanel panel;
	public GunUI gun;

	void Start () {
		panel.player = player;
		gun.player = player;
	}
}
