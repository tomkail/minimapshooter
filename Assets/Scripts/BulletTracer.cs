using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class BulletTracer : MonoBehaviour {

	public LineRenderer lineRenderer;

	public Vector2 startPoint;
	public Vector2 endPoint;

	public void Init (Vector2 startPoint, Vector2 endPoint) {
		lineRenderer.numPositions = 2;
		lineRenderer.SetPositions(new Vector3[]{startPoint, endPoint});
	}
}