﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Projectile {

	public float fuseTime = 1;
//	public Timer fuseTimer;
	public AnimationCurve damageOverRange;

	void Start () {
//		fuseTimer = new Timer(fuseTime);
//		fuseTimer.Start();
//		fuseTimer.OnComplete += OnCompleteTimer;
	}

	protected override void Update () {
		base.Update();
//		fuseTimer.Update();
		if(lifeTimer > fuseTime) {
			Explode();
		}
	}

	void OnCompleteTimer ()	{
		Explode();
	}

	public void Explode () {
		Collider2D[] colliders = Physics2D.OverlapCircleAll(rigidbody.position, damageOverRange.keys.Last().time);
		foreach(Collider2D otherCollider in colliders) {
			if(otherCollider.gameObject.CompareTag("Player")) {
				PlayerAvatar player = otherCollider.gameObject.GetComponent<PlayerAvatar>();

				RaycastHit2D[] linecastHits = Physics2D.LinecastAll(rigidbody.position, otherCollider.gameObject.transform.position);
				bool hitObstacle = false;
				foreach(var hit in linecastHits) {
					if(hit.collider == collider) continue;
					if(hit.collider == otherCollider) {
						break;
					} else {
						hitObstacle = true;
						break;
					}
				}
				if(hitObstacle) continue;

				float distance = Vector2.Distance(rigidbody.position, player.rigidbody.position);
				float damage = damageOverRange.Evaluate(distance);
				player.healthModule.TakeDamage(damage);
			}
		}
		Destroy(gameObject);
	}
}