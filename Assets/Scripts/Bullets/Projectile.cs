﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
	public new Rigidbody2D rigidbody {
		get {
			return GetComponent<Rigidbody2D>();
		}
	}

	public new Collider2D collider {
		get {
			return GetComponent<Collider2D>();
		}
	}

	public SpriteRenderer spriteRenderer {
		get {
			return GetComponent<SpriteRenderer>();
		}
	}

	public bool destroyOnCollision = true;
	public float lifeTimer;

	public virtual void Shoot (Gun gun) {
		Physics2D.IgnoreCollision(gun.player.collider, collider);
	}

	protected virtual void Update () {
		lifeTimer += Time.deltaTime;
	}
}

