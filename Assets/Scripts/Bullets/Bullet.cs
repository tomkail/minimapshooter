using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Projectile {
	public float deathTime;
//	public float distanceTravelled;
	public float damage;

	protected override void Update () {
		base.Update();
		if(lifeTimer > deathTime) {
			Destroy(gameObject);
		}
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if(destroyOnCollision) {
			Destroy(gameObject);
		}
	}
}