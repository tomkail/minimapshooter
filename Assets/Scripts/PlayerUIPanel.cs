﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIPanel : UIMonoBehaviour {

	public Player player;

	public WorldSpaceUIElement worldSpaceUI;

	public Text playerName;

	public Text weaponName;
	public Text ammo;

	public Image healthImage;
	private Material healthMaterial;

	void Start () {
		healthMaterial = new Material(healthImage.material);
		healthImage.material = healthMaterial;
	}

	void OnDestroy () {
		Destroy(healthMaterial);
	}

	void Update () {
		if(player.avatar == null) {
			return;
		}

		worldSpaceUI.target = player.avatar.transform;
		worldSpaceUI.camera = Camera.main;

		playerName.text = player.name;
		playerName.color = player.team.color;
		if(player.avatar.weaponModule.hasWeapon) {
			weaponName.text = player.avatar.weaponModule.currentWeapon.name;
			ammo.text = player.avatar.weaponModule.currentWeapon.ammoModule.bulletsInClip+"/"+player.avatar.weaponModule.currentWeapon.ammoModule.clipSize;
		} else {
			weaponName.text = "Unarmed";
			ammo.text = "";
		}
		healthMaterial.SetFloat("_Cutoff", player.avatar.healthModule.currentHealth/player.avatar.healthModule.maxHealth);
//		healthImage.material
		
//		_renderer.GetPropertyBlock(_propBlock);
        // Assign our new value.
//        _propBlock.SetColor("_Color", Color.Lerp(Color1, Color2, (Mathf.Sin(Time.time * Speed + Offset) + 1) / 2f));
        // Apply the edited values to the renderer.
//        _renderer.SetPropertyBlock(_propBlock);
//		healthMaterial.renderer.GetPropertyBlock(materialProperty);
	}
}
