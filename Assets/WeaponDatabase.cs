﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDatabase : MonoSingleton<WeaponDatabase> {
	public Gun pistol;
	public Gun semiAuto;
	public Gun machineGun;
	public Gun shotgun;
	public Gun sniperRifle;
	public Gun lmg;
	public Gun grenadeLauncher;
	public Gun bounceBulletGun;
}
